import std.stdio;
import std.string;
import std.conv;
import std.array;
import std.algorithm;


int main(string[] args) {
  auto intr_file = args[1];
  auto out_file = args[2];

  writeln("start");
  auto intrc = clustering(input(intr_file));
  writeln("output");
  output(intrc, out_file);
  writeln("end");
  
  return 0;
}


Interaction_cluster[] clustering(Interaction[] intr) {
  writeln("clustering interaction");
  writeln("swap tags of interacrtions");
  foreach(elem; intr)
    elem.swap_tagc_id;

  writeln("sort tag of interactions");
  intr = array(sort!("a.tagc_id1 < b.tagc_id1")(intr));
  
  writeln("create interaction clusters");
  Interaction_cluster[] intrc;
  auto app = appender(intrc);
  bool[] check_flag;
  check_flag.length = intr.length;
  fill(check_flag, false);

  for(uint i=0; i<intr.length; i++) {
    if(check_flag[i]) continue;

    uint counter = 1;
    
    for(uint j=i+1; j<intr.length; j++) {
      if(intr[j].tagc_id1 == intr[i].tagc_id1 && intr[j].tagc_id2 == intr[i].tagc_id2) {
	  counter++;
	  check_flag[j] = true;
      }
      else break;
    }
    
    app.put(new Interaction_cluster(intr[i].intr, counter));
  }
  
  return app.data;
}


Interaction[] input(string filename) {
  writeln("input");
  auto fin = File(filename);
  Interaction[] intr;
  auto app = appender(intr);
  
  foreach(line; fin.byLine) {
    auto entry = to!string(line);
    app.put(new Interaction(entry, to!uint(entry.split("\t")[0]),
			    to!uint(entry.split("\t")[5]))) ;
  }

  return app.data;
}


void output(Interaction_cluster[] intrc, string filename) {
  auto fout = File(filename, "w");
  foreach(elem; intrc)
    fout.writeln(elem.intr, "\t", elem.count);
}


class Interaction {
  string intr;
  uint tagc_id1, tagc_id2;

  this(string a, uint b, uint c) {
    intr=a; tagc_id1=b; tagc_id2=c;
  }

  void swap_tagc_id() {
    if(tagc_id1 > tagc_id2) {
      auto tmp = tagc_id1;
      tagc_id1 = tagc_id2;
      tagc_id2 = tmp;
    }
  }
}


class Interaction_cluster {
  string intr;
  uint count;
  
  this(string a,  uint b) {
    intr=a; count=b;
  }
}
