import std.stdio;
import std.string;
import std.conv;
import std.array;
import std.algorithm;


int main(string[] args) {
  auto pre_intr_file = args[1];
  auto out_file = args[2];

  writeln("start");
  auto intr = pre_interaction_to_interaction(input(pre_intr_file));
  writeln("output");
  output(intr, out_file);
  writeln("end");
  
  return 0;
}


class Pre_interaction {
  string tagc;
  uint tagc_id, tag_id, pair_id;

  this(string a, uint b, uint c, uint d) {
    tagc=a; tagc_id=b; tag_id=c; pair_id=d;
  }

  void swap_id() {
    if(tag_id > pair_id) {
      auto tmp = tag_id;
      tag_id = pair_id;
      pair_id = tmp;
    }
  }
}


class Interaction {
  string tagc1, tagc2;
  this(string a, string b) {tagc1=a; tagc2=b;}
}


Interaction[] pre_interaction_to_interaction(Pre_interaction[] pre_intr) {
  writeln("convert pre_interactions to interaction between tag_clusters");
  Interaction[] intr;
  auto app = appender(intr);
  
  writeln("swap ID of pre_interacrtions");
  foreach(elem; pre_intr)
    elem.swap_id;

  writeln("sort ID of pre interactions");
  pre_intr = array(sort!("a.tag_id < b.tag_id")(pre_intr));
  
  writeln("create interaction");
  for(uint i=0; i<pre_intr.length; i++) 
    if(i % 2 == 1) 
      app.put(new Interaction(pre_intr[i-1].tagc, pre_intr[i].tagc));
  
  return app.data;
}


Pre_interaction[] input(string filename) {
  writeln("input");
  auto fin = File(filename);
  Pre_interaction[] pre_intr;
  auto app = appender(pre_intr);
  
  foreach(line; fin.byLine) {
    auto entry = to!string(line).split("\t");
    app.put(new Pre_interaction(entry[0] ~ "\t" ~ entry[1] ~ "\t" ~
				entry[2] ~ "\t" ~  entry[3] ~ "\t" ~ entry[4],
				to!uint(entry[0]), to!uint(entry[6]),
				to!uint(entry[7])));
  }

  return app.data;
}


void output(Interaction[] intr, string filename) {
  auto fout = File(filename, "w");
  foreach(elem; intr)
    fout.writeln(elem.tagc1, "\t", elem.tagc2);
}
