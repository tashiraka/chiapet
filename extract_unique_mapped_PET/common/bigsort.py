'''
Sorts huge file.
It can be adjusted to sort any format by modifying the class "Entry" and function "to_entry".

NOTE:
- This script opens ALL the temporary files during merging, so it's important that the number
  of temporary files doesn't exceed the OS's limit. 
- The file content is converted into Entry object twice--during sorting and merging. 
  This is not very efficient.
'''

from __future__ import with_statement
from optparse import OptionParser
from time import time
from heapq import heappush, heappop
from common.util import get_logger

import sys, os, tempfile

def log(msg):
  pass

'''
Sorts entries and returns temp file handler
containing sorted entries.
'''
def dump(entries):
  log('Sorting entries')
  entries = sorted(entries)

  log('Writing partial results to temporary file')
  out = tempfile.TemporaryFile()
  for ent in entries:
    out.write('%s\n' %(ent))
      
  return out

'''
Returns list of temp file handlers
@param instream:
  Iterable input stream, e.g. file object
@param to_entry:
  Function to convert line into sortable object
'''
def sort(instream, maxload, to_entry):
  dumped = list()
  entries = list()
  count = 0
  
  for line in instream:
    ent = to_entry(line)
    entries.append(ent)
    count += 1
    
    if count == maxload:
      d = dump(entries)
      dumped.append(d)
      count = 0
      entries = list()

  if entries:
    d = dump(entries)
    dumped.append(d)
    entries = list()

  log('Partial results written to {0} files'.format(len(dumped)))
  return dumped

'''
Merges _open_ and sorted temporary files and 
write the result into stream 'out'. 

@param files:
  Handlers to open sorted temp files
@param out:
  Output stream
@param to_entry:
  Function to convert line into sortable object
'''
def merge(out, files, to_entry):
  log('Merging {0} files'.format(len(files)))
  heap = list()
  
  # kick-start
  for i, f in enumerate(files):
    f.seek(0)
    ent = to_entry(f.next())
    heappush(heap, (ent, i))

  while heap:
    ent, i = heappop(heap)
    out.write('%s\n' %(ent))
    
    try:
      line = files[i].next()
      heappush(heap, (to_entry(line), i))
    except StopIteration:
      log('Closing temp file')
      files[i].close()
      
  log('Merging complete')

if __name__ == '__main__':
  pass
