import logging, subprocess, sys, tempfile, threading, os, re, time, string

'''
Opens normal file or compressed file.
'''
class ouvre:
  def __init__(self, filename):
    if filename[-3:] == '.gz':
      self.handler = tempfile.NamedTemporaryFile()
      p = subprocess.call('gunzip -c {0} > {1}'.format(filename, self.handler.name), shell=True)
    else:
      self.handler = __builtins__.open(filename)
  
  def __enter__(self):
    return self.handler
  
  def __exit__(self, type, value, traceback):
    self.handler.close()

'''
Creates a named temporary file that gets deleted
upon leaving 'with' block.
'''
class opentemp:
  def __init__(self, dir=None, delete=True):
    self.temp = tempfile.NamedTemporaryFile(dir=dir, delete=delete)
    
  def __enter__(self):
    return self.temp
  
  def __exit__(self, type, value, traceback):
    self.temp.close()

def get_logger(logname, file=None, level=logging.INFO):
  #create logger
  logger = logging.getLogger(logname)
  logger.setLevel(logging.DEBUG)

  #create formatter
  formatter = logging.Formatter('%(asctime)s %(levelname)5s [%(name)s] %(message)s')

  #console handler
  sh = logging.StreamHandler()
  sh.setLevel(level)
  sh.setFormatter(formatter)
  logger.addHandler(sh)

  if file:
    #create file handler
    fh = logging.FileHandler(file)
    fh.setLevel(logging.DEBUG) #file is always DEBUG
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    logger.logfile = file #used by 'shell' to send email to admin

  return logger

'''
Converts something like "1,3-4,7-8" to [1,3,4,7,8]
'''
def parse_range(r):
  result = list()
  a = r.split(',')
  for x in a:
    if '-' in x:
      p, q = x.split('-')
      result += list(xrange(int(p), int(q)+1))
    else:
      result.append(int(x))
  return result

if __name__ == '__main__':
  log = get_logger(sys.argv[0])
  log.debug('Howdy!')

'''
Converts time in seconds to a more readable format.
'''
def format_benchtime(t):
  s = ''
  x = int(t/86400)
  if x > 0: 
    s += '%.2fd ' %(x)
    t %= 86400

  x = int(t/3600)
  if x > 0: 
    s += '%.2fh ' %(x)
    t %= 3600

  x = int(t/60)
  if x > 0: 
    s += '%.2fm ' %(x)
    t %= 60

  x = t
  s += '%.2fs' %(x)
  return s

'''
Return the name of a temporary file to
which the supplied file is decompressed.
Caller has to delete the temp file manually.
'''
def decompress_file(file, workdir):
  t = tempfile.NamedTemporaryFile(delete=False, dir=workdir)
  t.close()
  shell('gunzip -c {0} > {1}'.format(file, t.name))
  return t.name

# NOTE: This method doesn't keep the original order of lines!
def split_file(filename, nfile, nline, workdir):
  temps = [tempfile.NamedTemporaryFile(
             dir=workdir, delete=False, prefix='%s.part%04d.' %(os.path.basename(filename), i+1)) 
           for i in xrange(nfile)]
  
  with open(filename) as f:
    i = 0
    while True:
      out = temps[i]
      try:
        for j in xrange(nline):
          out.write(f.next())
      except StopIteration:
        break
      
      i += 1
      if i == nfile: i = 0
      
  return [t.name for t in temps]

class shell:
  dry_run = False
  log = None

  def __init__(self, cmd, input=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE):
    log = shell.log
    cmd = re.sub('\s{2,}', ' ', cmd)
    
    if log: log.debug("START [{0}]".format(cmd)) 
    start_time = time.time()
    
    if not shell.dry_run:
      if input:
        if log: log.debug("< {0}".format(input))
        p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
      else:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
      self.out, self.err = p.communicate(input)
      if log and self.out: log.debug('> ' + self.out)
      
      if p.returncode != 0:
        log.error("Execution failed: '{0}'".format(cmd))
        if log and self.err: log.error('> ' + self.err)
        
        raise Exception("Execution failed: '{0}'".format(cmd))
      
    elapsed = format_benchtime(time.time()-start_time)
    if log: log.debug('END   [Time elapsed = {0}]\n'.format(elapsed))

# Reverse complement
d = string.maketrans('ACGTNacgtn', 'TGCANtgcan')
def rev_comp(str):
  return str[::-1].translate(d)

# Threaded shell
class tshell(threading.Thread):
  def __init__(self, cmd):
    threading.Thread.__init__(self)
    self.cmd = cmd
    
  def run(self):
    shell(self.cmd)

# To make reading/writing to Config file slightly simpler
class ConfigHelper:
  def __init__(self, config):
    self.__dict__['config'] = config
    
  def __setattr__(self, name, value):
    if name == '__section__':
      self.__dict__[name] = value
    else:
      self.config.set(self.__section__, name, str(value))

  def __getattr__(self, name):
    return self.config.get(self.__section__, name)

# Adds thousands separator to integer
def intcomma(num):
  c = 0
  out = list()
  while True:
    p = num % 10
    num /= 10
    c += 1
    out.append(str(p))
    if num == 0:
      break 
    elif c == 3:
      out.append(',')
      c = 0
  return ''.join(out[::-1])
