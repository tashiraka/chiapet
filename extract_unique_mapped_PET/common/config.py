from os import path, environ
BaseDir = path.abspath(environ['CHIAPETPATH'])

class Location:
  BinDir    = path.join(BaseDir, 'bin')
  PrepDir   = path.join(BaseDir, 'prep')
  WorkDir   = path.join(BaseDir, 'work')
  SourceDir = path.join(BaseDir, 'src')
  LogDir    = path.join(BaseDir, 'log')
  LibDir    = path.join(BaseDir, 'lib')
  TemplDir  = path.join(BaseDir, 'data/template')
  
  # Location of ChIA-PET browser
  BrowserDir= '/var/www/html/chiapet'

class Interpreter:  
  Python = 'python2.6'
  Perl   = 'perl'
  
  # The first two paths are required
  classpath = ':'.join((Location.BinDir,
                        path.join(Location.LibDir, 'java/commons-cli-1.2.jar'),
                        ))
  Java   = 'java -Xmx2048m -cp %s' %(classpath)

class Mapping:
  # Parameters here are used for mapping with Batman.
  MaxHits = 2 # max hits in each direction; this makes a total of 2x2=10 hits.
  MaxMismatches = 2

class Assembly:
  # Assembly that can be used in ChIA-PET processing (option --asm/-a)
  # Adding a new assembly involves adding 3 new files, for chromosome size, gap, and gene respectively  
  assemblies = ('hg18', 'mm8', 'pombe2004')
  
  # Directories containing files required by Batman
  BatmanDir = dict([(x, path.join(BaseDir, 'data/batman', x)) for x in assemblies])
  
  # Directories containing base tracks corresponding to each assembly
  # Note that corresponding GBrowse conf template must be updated to reflect the content of the directories
  BaseTrackDir = dict([(x, path.join(BaseDir, 'data/tracks', x)) for x in assemblies])
  
  SizeInfo = dict([(x, path.join(BaseDir, 'data/chrdata/size', x + '.txt')) for x in assemblies]) 
  GapInfo  = dict([(x, path.join(BaseDir, 'data/chrdata/gap', x + '.txt')) for x in assemblies])
  GeneInfo = dict([(x, path.join(BaseDir, 'data/chrdata/gene', x + '.txt')) for x in assemblies])

class Database:
  # Credentials used to create and update tables in the specified database (--database/-db)
  Username = 'root'
  Password = ''

class Parameter:
  # max number of entries loaded and sorted in-memory before finding unique tags
  # (with the default value the program takes around ~2GB of memory)
  UniqueFindingMaxSort = 1000000
  
  # max number of entries loaded and sorted in-memory before creating GFF density track
  # (with the default value the program takes around ~2GB of memory)
  GFFDensityMaxSort = 4000000 
  
  # These two values used to plot the histogram shown on the ChIA-PET browser
  HistogramSpan = 10000
  HistogramBinCount = 100
  
  # These two values used to determine input to program that calculates cutoff value
  AutoCutoffSpan = 100000
  AutoCutoffBinCount = 1000
  
  # Used to draw density tracks
  DensityExtTagLength = 250
  
  # This value is used when generating SVG file, such that
  # clicking on any interaction will take user to URL [base_url][project]
  # [project] is specified during run
  BrowserBaseUrl = 'http://localhost/cgi-bin/gbrowse/'

  # This string will be inserted as library description
  VersionString = 'ChIA-PET Pipeline v3.5'
  
  # All linker sets.
  # There are two default linkers here: linker_a and linker_b 
  # Note that the name must follow this pattern: (linker name).[1/2]
  Linker = {
    'linker_a.1': 'GTTGGATCCGATATCGCGG',
    'linker_a.2': 'GTTGGATCATATATCGCGG',
  
    'linker_b.1': 'GTTGGATAAGATATCGCGG',
    'linker_b.2': 'GTTGGAATGTATATCGCGG',
  }

# The values here will be modified during runtime.
# They just act as global variables.
class Flag:
  DryRun = False   # whether real call is made to the OS
  KeepTemp = False # whether intermediate files are kept
