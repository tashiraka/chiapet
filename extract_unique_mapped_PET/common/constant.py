class InteractionType:
  InterChrom = 1
  IntraChrom = 2
  DiffOrient = 3
  SelfLigate = 4
