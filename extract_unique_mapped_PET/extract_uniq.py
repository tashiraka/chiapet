'''
Expects input from stdin.
'''

from __future__ import with_statement
from optparse import OptionParser
import sys

class Tag:
  def __init__(self, count, chr_h, start_h, end_h, chr_t, start_t, end_t, arr):
    self.count = count
    self.chr_h = chr_h
    self.start_h = start_h
    self.end_h = end_h
    self.chr_t = chr_t
    self.start_t = start_t
    self.end_t = end_t
    self.arr = arr # Array

  def __eq__(self, other):
    return (self.chr_h == other.chr_h and self.start_h == other.start_h and self.end_h == other.end_h and 
            self.chr_t == other.chr_t and self.start_t == other.start_t and self.end_t == other.end_t)

'''
Format:
  1: count
  Head
    6: chrom
    8: start
    9: end
  Tail
    12: chrom
    14: start
    15: end
'''
def arr_to_tag(line):
  a = line.split('\t')
  return Tag(int(a[1]), a[6], int(a[8]), int(a[9]), a[12], int(a[14]), int(a[15]), a)

def parse(f):
  prev_tag = None
  extra = False
  
  for line in f:
    cur_tag = arr_to_tag(line)
    extra = False

    if(cur_tag.count != 1): sys.stderr.write("error: count is not 1.\n")
    
    if prev_tag and (cur_tag == prev_tag):
      prev_tag.count += cur_tag.count
    else:
      if not prev_tag:
        prev_tag = cur_tag 
        continue
        
      prev_tag.arr[1] = str(prev_tag.count)
      print '\t'.join(prev_tag.arr),
      extra = True
      prev_tag = cur_tag
        
  if extra:
    print '\t'.join(prev_tag.arr),
        
def main():
  parser = OptionParser(usage="usage: %prog < <input stream>")
  (options, args) = parser.parse_args()
  parse(sys.stdin)
  
  return 0

if __name__ == '__main__':
  sys.exit(main())
