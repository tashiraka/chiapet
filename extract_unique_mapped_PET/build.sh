#!/bin/sh

OUTDIR=bin
LIBDIR=lib/java
CLASSPATH=.:$LIBDIR/commons-cli-1.2.jar
JC=javac
TMP=`date +%Y%m%d%H%M%S.tmp`

find . -name '*.java' > $TMP

$JC -XDignore.symbol.file -d $OUTDIR -cp $CLASSPATH @$TMP

rm -f $TMP

