'''
Reads map file and generates "list" file, which may include rescued PETs, plus statistics.
It takes only map entry with unique hits.
Currently, it doesn't implement "rescue", but it can be added. 

Note that this "unique" does _not_ mean that there's exactly only one PET with distinct sequences.
That "unique" will be done by separate script.

There are two kinds of statistics generated:
1) "unique" statistics
2) redundant statistics, which includes the sequencing count

For the unique statistics to be correct, the input map file must
already be "compacted", i.e. each pair sequences is distinct from the rest
'''

import re, sys, ConfigParser
from optparse import OptionParser
from common.config import *

def helper(chrom, strand, pos, mismatch, seqlen):
  pos = int(pos)
  seqlen = int(seqlen)
  
  if strand == '+':
    return [chrom, strand, pos, pos+seqlen-1, mismatch, seqlen]
  else:
    return [chrom, strand, pos-seqlen+1, pos, mismatch, seqlen]

def make_one_entry(id, count, hseq, tseq, hline, tline):
  DUMMY = 'dummy'
  
  ent = [id, count, hseq, tseq, DUMMY, DUMMY]
  ent += helper(*hline)
  ent += helper(*tline)
  
  return '\t'.join(str(x) for x in ent)

def check_mismatches(hits):
  m = [[], [], []]
  for hit in hits:
    m[int(hit[3])].append(hit)
  
  if len(m[0]) == 1: # 1:m1:m2
    return m[0]
  elif len(m[0]) == 0 and len(m[1]) == 1: # 0:1:m2
    return m[1]
  # elif len(m[0]) == 0 and len(m[1]) == 0 and len(m[2]) == 1: # 0:0:1
  #   return m[2]
  else:
    return None

def process(stats, red_stats, id, count, hseq, tseq, heads, tails):
  h = len(heads)
  t = len(tails)
  
  key = None
  if h >= 2:
    key = 'M'
  else:
    key = 'NU'[h]

  if t >= 2:
    key += 'M'
  else:
    key += 'NU'[t]
  
  stats[key] += 1
  red_stats[key] += count
  
  # Write out PETs with pattern 1:m1:m2, 0:1:m2, (not) or 0:0:1 
  head = check_mismatches(heads)
  tail = check_mismatches(tails)
  
  if head and tail:
    stats['CHERRY'] += 1
    red_stats['CHERRY'] += count
    print make_one_entry(id, 1, hseq, tseq, head[0], tail[0])
#   print make_one_entry(id, count, hseq, tseq, head[0], tail[0])

def main():
  parser = OptionParser(usage='usage: %prog [options]')
  parser.add_option('-S', '--stats', metavar='FILE', 
                    help='File into which the map file statistics will be written')
  # parser.add_option('-C', '--chrom', metavar='FILE', 
  #                  help='File containing accepted chromosome names')
  (opts, args) = parser.parse_args()

  if not (opts.stats): #or opts.chrom):
    parser.print_usage()
    return 1
  
  #valid_chromes = set()
  #with open(opts.chrom) as f:
  #  for line in f:
  #    valid_chromes.add(line.split('\t')[0])
  
  # NOTE: 
  # U=Unique, M=Multiple, N=Non-mappable --- (head-tail)
  # CHERRY=Cherry-picked
  
  # distinct sequence pairs
  stats = dict(ALL=0, CHERRY=0, UU=0, NN=0, MM=0, NU=0, UN=0, UM=0, MU=0, NM=0, MN=0)

  # redundant statistics: with sequencing counts
  red_stats = dict(ALL=0, CHERRY=0, UU=0, NN=0, MM=0, NU=0, UN=0, UM=0, MU=0, NM=0, MN=0)
  
  # number of PETs with sequencing count from 1..20 or more
  count_stats = [0] * 21
  
  id = None
  hseq = None
  tseq = None
  
  heads = list()
  tails = list()
  count = 0
  
  for line in sys.stdin:
    line = line.strip()
    if line[0] == '>':
      if id: process(stats, red_stats, id, count, hseq, tseq, heads, tails)
      
      id, hseq, tseq = line.split('\t')
      regex = re.search(r'>(.+)_COUNT:(\d+)$', id)
      
      if not regex:
        raise Exception('PET ID must end with COUNT, e.g. ">PET1_COUNT:4".')
      
      id = regex.group(1)
      count = int(regex.group(2))
      
      if count <= 20:
        count_stats[count-1] += 1
      else:
        count_stats[-1] += 1
        
      red_stats['ALL'] += count
      stats['ALL'] += 1
      
      heads = list()
      tails = list()
    else:
      a = line.split('\t')
      if line[0] == 'H': #and a[1] in valid_chromes:
        heads.append(a[1:])
      elif line[0] == 'T': #and a[1] in valid_chromes:
        tails.append(a[1:])

  # process last entry
  process(stats, red_stats, id, count, hseq, tseq, heads, tails)
  
  # dump the statistics
  config = ConfigParser.SafeConfigParser()
  config.add_section('Unique PETs')
  config.add_section('Total PETs')
  config.add_section('Sequencing Count')
  
  for k, v in stats.items():
    config.set('Unique PETs', k, str(v))

  for k, v in red_stats.items():
    config.set('Total PETs', k, str(v))

  config.set('Sequencing Count', '>20', str(count_stats[-1]))
  for i in xrange(20): 
    config.set('Sequencing Count', str(i+1), str(count_stats[i]))
  
  with open(opts.stats, 'w') as out:
    config.write(out)

  return 0

if __name__ == '__main__':
  sys.exit(main())
  