package sg.edu.astar.gis.chiapet;

/**
 * Assume there are three linkers. The first linker is for ATAT, 
 * the second linker is for CGAT, and the third linker is for CGCG.
 * 
 * For linkers ATAT and CGCG, only need one sequence alignment.
 * 
 * For linker CGAT, need two sequence alignments: 
 * one is aligning the sequence with the linker, 
 * and the other one is aligning the sequence with the reverse complements of the linker
 *
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class LinkerFilter {

	final static int TAG_LENGTH = 20;
	final static int MIN_SCORE = 20;
	final static int SCORE_DIFF = 4;
	final static int CONFIDENT_TAG_LENGTH = 18;// assume that the first 18
	// nucleotides are sequenced with
	// high quality, and there is no
	// linker information in this
	// range

	final static int debugLevel = 0;

	private static void log(Object msg) {
		// dummy method to prevent the program
		// from printing too much debugging output
		System.out.println(msg.toString());
	}

	public static void main(String[] argv) throws IOException, ParseException {
		Options opts = new Options();
		opts.addOption(null, "flip-tail", false,
				"print reverse-complemented tail sequences");
		opts.addOption("h", "help", false, "display this help and exit");

		CommandLineParser parser = new GnuParser();
		CommandLine cmd = parser.parse(opts, argv);

		if (cmd.hasOption('h') || cmd.getArgs().length < 5) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(LinkerFilter.class.getName(), opts);
			return;
		}

		// Overwrite the original args
		String[] args = cmd.getArgs();

		String fFile = args[0], rFile = args[1];
		String outPrefix = args[2];

		String linker_x1 = args[3].toUpperCase();
		String linker_x2 = revComplement(linker_x1);
		String linker_y1 = args[4].toUpperCase();
		String linker_y2 = revComplement(linker_y1);

		int nLinkers = 3;
		String[] linkers = new String[nLinkers];
		linkers[0] = linker_x1 + linker_x2;
		linkers[1] = linker_y1 + linker_y2;
		linkers[2] = linker_y1 + linker_x2;
		int[] linkerCount = new int[nLinkers];
		Arrays.fill(linkerCount, 0);
		PrintWriter fileOut[] = new PrintWriter[nLinkers];
		fileOut[0] = new PrintWriter(new FileOutputStream(outPrefix + "."
				+ linker_x1 + linker_x2));
		fileOut[1] = new PrintWriter(new FileOutputStream(outPrefix + "."
				+ linker_y1 + linker_y2));
		fileOut[2] = new PrintWriter(new FileOutputStream(outPrefix + "."
				+ linker_y1 + linker_x2));

		PrintWriter fileOutNoLinkers = new PrintWriter(new FileOutputStream(
				outPrefix + ".NNN"));

		BufferedReader fileInF = new BufferedReader(new InputStreamReader(
				new FileInputStream(fFile)));
		BufferedReader fileInR = new BufferedReader(new InputStreamReader(
				new FileInputStream(rFile)));

		String lineF = null, lineR = null;
		int total = 0, noLinker = 0;
		int scores[] = new int[nLinkers];

		PrintWriter fileOutScore = null;
		if (debugLevel > 3) {
			fileOutScore = new PrintWriter(new FileOutputStream(outPrefix
					+ ".scores.txt"));
		}

		while ((lineF = fileInF.readLine()) != null) {
			lineF = fileInF.readLine();
			lineR = fileInR.readLine();
			lineR = fileInR.readLine();

			total++;
			if (debugLevel > 4) {
				if (total % 1000 == 0) {
					log("Total: " + total + " , No linker: " + noLinker);
					for (int c1 = 0; c1 < nLinkers; c1++) {
						log("\t" + linkers[c1] + "\t" + linkerCount[c1]);
					}
				}
			}
			Arrays.fill(scores, 0);

			/*
			 * String read = lineF.substring(CONFIDENT_TAG_LENGTH, lineF.length())
			 * .toUpperCase() + revComplement(
			 * lineR.substring(CONFIDENT_TAG_LENGTH, lineR.length())) .toUpperCase();
			 */
			String read1 = lineF.substring(CONFIDENT_TAG_LENGTH, lineF.length())
					.toUpperCase().replace('.', 'N');
			String read2 = revComplement(
					lineR.substring(CONFIDENT_TAG_LENGTH, lineR.length())).toUpperCase()
					.replace('.', 'N');

			Aligner alignerX1 = new Aligner(read1, linker_x1);
			Aligner alignerX2 = new Aligner(read2, linker_x2);
			Aligner alignerY1 = new Aligner(read1, linker_y1);
			Aligner alignerY2 = new Aligner(read2, linker_y2);

			int scoreX1 = alignerX1.getScore();
			int scoreX2 = alignerX2.getScore();
			int scoreY1 = alignerY1.getScore();
			int scoreY2 = alignerY2.getScore();
			int indexX1 = 8 - alignerX1.aligner.start1;
			int indexX2 = 9 - alignerX2.aligner.start1;
			int indexY1 = 8 - alignerY1.aligner.start1;
			int indexY2 = 9 - alignerY2.aligner.start1;
			// check whether the specific positions are "AT" or "CG"
			// If yes, keep the original alignment score
			// otherwise, replace the alignment score with 0
			if ((alignerX1.getResults()[0].length() >= indexX1 + 2) && (indexX1 >= 0)) {
				if (alignerX1.getResults()[0].substring(indexX1, indexX1 + 2)
						.compareTo(
								alignerX1.getResults()[1].substring(indexX1, indexX1 + 2)) != 0) {
					scoreX1 = 0;
				}
			} else {
				scoreY1 = 0;
			}
			if ((alignerX2.getResults()[0].length() >= indexX2 + 2) && (indexX2 >= 0)) {
				if (alignerX2.getResults()[0].substring(indexX2, indexX2 + 2)
						.compareTo(
								alignerX2.getResults()[1].substring(indexX2, indexX2 + 2)) != 0) {
					scoreX2 = 0;
				}
			} else {
				scoreX2 = 0;
			}
			if ((alignerY1.getResults()[0].length() >= indexY1 + 2) && (indexY1 >= 0)) {
				if (alignerY1.getResults()[0].substring(indexY1, indexY1 + 2)
						.compareTo(
								alignerY1.getResults()[1].substring(indexY1, indexY1 + 2)) != 0) {
					scoreY1 = 0;
				}
			} else {
				scoreY1 = 0;
			}
			if ((alignerY2.getResults()[0].length() >= indexY2 + 2) && (indexY2 >= 0)) {
				if (alignerY2.getResults()[0].substring(indexY2, indexY2 + 2)
						.compareTo(
								alignerY2.getResults()[1].substring(indexY2, indexY2 + 2)) != 0) {
					scoreY2 = 0;
				}
			} else {
				scoreY2 = 0;
			}

			/*
			 * scores[0] = alignerX1.getScore() + alignerX2.getScore(); scores[1] =
			 * alignerY1.getScore() + alignerY2.getScore();
			 * 
			 * int chimeras = 0; scores[2] = alignerX1.getScore() +
			 * alignerY2.getScore(); if(scores[2] < alignerY1.getScore() +
			 * alignerX2.getScore()) { scores[2] = alignerY1.getScore() +
			 * alignerX2.getScore(); chimeras = 1; }
			 */

			scores[0] = scoreX1 + scoreX2;
			scores[1] = scoreY1 + scoreY2;

			int chimeras = 0;
			scores[2] = scoreX1 + scoreY2;
			if (scores[2] < scoreY1 + scoreX2) {
				scores[2] = scoreY1 + scoreX2;
				chimeras = 1;
			}

			int best = 0, secondbest = 0, bestindex = -1;
			for (int c1 = 0; c1 < nLinkers; c1++) {
				if (scores[c1] >= best) {
					secondbest = best;
					bestindex = c1;
					best = scores[c1];
				} else if (scores[c1] > secondbest) {
					secondbest = scores[c1];
				}
			}

			if (debugLevel > 3) {
				fileOutScore.println(best + "\t" + secondbest + "\t"
						+ (best - secondbest));
			}

			final boolean flipTailSeq = cmd.hasOption("flip-tail");

			if (best >= MIN_SCORE && best - secondbest == SCORE_DIFF) {
				linkerCount[bestindex]++;
				fileOut[bestindex].println(">PET_" + linkerCount[bestindex]);

				if (flipTailSeq) {
					fileOut[bestindex].println(lineF.substring(0, TAG_LENGTH) + "\t"
							+ revComplement(lineR.substring(0, TAG_LENGTH)));
				} else {
					fileOut[bestindex].println(lineF.substring(0, TAG_LENGTH) + "\t"
							+ lineR.substring(0, TAG_LENGTH));
				}

				if (debugLevel > 3) {
					fileOut[bestindex].println(lineF + "\t"
							+ revComplement(lineR));
					fileOut[bestindex].println(read1 + "\t" + read2);
					fileOut[bestindex].println("Score: " + scores[bestindex]);
					if (bestindex == 0) {
						fileOut[bestindex].println(alignerX1.getResults()[0] + "\t"
								+ alignerX2.getResults()[0]);
						fileOut[bestindex].println(alignerX1.getResults()[1] + "\t"
								+ alignerX2.getResults()[1]);
						fileOut[bestindex].println(alignerX1.getMatchBar() + "\t"
								+ alignerX2.getMatchBar());
						fileOut[bestindex].println(alignerX1.aligner.start1 + "\t"
								+ alignerX1.aligner.end1);
						fileOut[bestindex].println(alignerX1.aligner.start2 + "\t"
								+ alignerX1.aligner.end2);
						fileOut[bestindex].println(alignerX2.aligner.start1 + "\t"
								+ alignerX2.aligner.end1);
						fileOut[bestindex].println(alignerX2.aligner.start2 + "\t"
								+ alignerX2.aligner.end2);
					} else if (bestindex == 1) {
						fileOut[bestindex].println(alignerY1.getResults()[0] + "\t"
								+ alignerY2.getResults()[0]);
						fileOut[bestindex].println(alignerY1.getResults()[1] + "\t"
								+ alignerY2.getResults()[1]);
						fileOut[bestindex].println(alignerY1.getMatchBar() + "\t"
								+ alignerY2.getMatchBar());
						fileOut[bestindex].println(alignerY1.aligner.start1 + "\t"
								+ alignerY1.aligner.end1);
						fileOut[bestindex].println(alignerY1.aligner.start2 + "\t"
								+ alignerY1.aligner.end2);
						fileOut[bestindex].println(alignerY2.aligner.start1 + "\t"
								+ alignerY2.aligner.end1);
						fileOut[bestindex].println(alignerY2.aligner.start2 + "\t"
								+ alignerY2.aligner.end2);
					} else {
						if (chimeras == 0) {
							fileOut[bestindex].println(alignerX1.getResults()[0] + "\t"
									+ alignerY2.getResults()[0]);
							fileOut[bestindex].println(alignerX1.getResults()[1] + "\t"
									+ alignerY2.getResults()[1]);
							fileOut[bestindex].println(alignerX1.getMatchBar() + "\t"
									+ alignerY2.getMatchBar());
							fileOut[bestindex].println(alignerX1.aligner.start1 + "\t"
									+ alignerX1.aligner.end1);
							fileOut[bestindex].println(alignerX1.aligner.start2 + "\t"
									+ alignerX1.aligner.end2);
							fileOut[bestindex].println(alignerY2.aligner.start1 + "\t"
									+ alignerY2.aligner.end1);
							fileOut[bestindex].println(alignerY2.aligner.start2 + "\t"
									+ alignerY2.aligner.end2);
						} else {
							fileOut[bestindex].println(alignerY1.getResults()[0] + "\t"
									+ alignerX2.getResults()[0]);
							fileOut[bestindex].println(alignerY1.getResults()[1] + "\t"
									+ alignerX2.getResults()[1]);
							fileOut[bestindex].println(alignerY1.getMatchBar() + "\t"
									+ alignerX2.getMatchBar());
							fileOut[bestindex].println(alignerY1.aligner.start1 + "\t"
									+ alignerY1.aligner.end1);
							fileOut[bestindex].println(alignerY1.aligner.start2 + "\t"
									+ alignerY1.aligner.end2);
							fileOut[bestindex].println(alignerX2.aligner.start1 + "\t"
									+ alignerX2.aligner.end1);
							fileOut[bestindex].println(alignerX2.aligner.start2 + "\t"
									+ alignerX2.aligner.end2);
						}
					}
				}
			} else {
				if (debugLevel > 4) {
					log(lineF.substring(0, TAG_LENGTH) + "\t"
							+ lineR.substring(0, TAG_LENGTH) + "\tBest Score: " + best
							+ "\tScore Diff: " + (best - secondbest));
				}
				noLinker++;
				if (flipTailSeq) {
					fileOutNoLinkers.println(best + "\t" + secondbest + "\t"
							+ (best - secondbest) + "\t" + lineF + "\t"
							+ revComplement(lineR));
				} else {
					fileOutNoLinkers.println(best + "\t" + secondbest + "\t"
							+ (best - secondbest) + "\t" + lineF + "\t" + lineR);
				}
				// fileOutNoLinkers.println(bestAligner[bestindex].getResults()[0]);
				// fileOutNoLinkers.println(bestAligner[bestindex].getResults()[1]);
				// fileOutNoLinkers.println(bestAligner[bestindex].getMatchBar());
			}

			lineF = fileInF.readLine();
			lineF = fileInF.readLine();
			lineR = fileInR.readLine();
			lineR = fileInR.readLine();
		}

		fileInF.close();
		fileInR.close();
		for (int c1 = 0; c1 < fileOut.length; c1++) {
			fileOut[c1].close();
		}
		fileOutNoLinkers.close();
		if (debugLevel > 3) {
			fileOutScore.close();
		}

		log("Total\t:\t" + total);
		log("No linker: " + noLinker);
		for (int c1 = 0; c1 < nLinkers; c1++) {
			log(linkers[c1] + " :\t" + linkerCount[c1]);
		}
	}

	private static char[] complTable = new char[255];
	static {
		complTable['A'] = 'T';
		complTable['C'] = 'G';
		complTable['G'] = 'C';
		complTable['T'] = 'A';
		complTable['N'] = 'N';

		complTable['a'] = 't';
		complTable['c'] = 'g';
		complTable['g'] = 'c';
		complTable['t'] = 'a';
		complTable['n'] = 'n';
	}

	private static String revComplement(String seq) {
		StringBuilder result = new StringBuilder(seq);
		result.reverse();
		for (int i = seq.length() - 1; i >= 0; i--) {
			switch (result.charAt(i)) {
			case 'A':
			case 'C':
			case 'G':
			case 'T':
			case 'N':
			case 'a':
			case 'c':
			case 'g':
			case 't':
			case 'n':
				result.setCharAt(i, complTable[result.charAt(i)]);
				break;
			default:
				break;
			}
		}
		return result.toString();
	}

}
