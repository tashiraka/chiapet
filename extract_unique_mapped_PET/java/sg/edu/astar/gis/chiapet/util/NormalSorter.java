package sg.edu.astar.gis.chiapet.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NormalSorter<E extends Comparable<E>> implements ISorter<E> {
	private File sortAndDumpHelper(List<E> entries)
			throws IOException {
		File tmpFile = File.createTempFile("tmp", null);
		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));
		Collections.sort(entries);
		for (Object o : entries) {
			out.write(o.toString());
			out.write('\n');
		}
		out.close();
		return tmpFile;
	}

	@Override
	public List<File> sortAndDump(BufferedReader input, int maxLoad,
			sg.edu.astar.gis.chiapet.util.Converter<E> conv) throws IOException {
		List<E> entries = new ArrayList<E>(maxLoad);
		List<File> dumped = new ArrayList<File>();

		int count = 0;
		String line;
		while ((line = input.readLine()) != null) {
			E ent = conv.valueOf(line);
			entries.add(ent);
			count++;

			if (count == maxLoad) {
				File tmpFile = sortAndDumpHelper(entries);
				dumped.add(tmpFile);
				count = 0;
				entries.clear();
			}
		}

		if (!entries.isEmpty()) {
			File tmpFile = sortAndDumpHelper(entries);
			dumped.add(tmpFile);
		}

		return dumped;
	}
}