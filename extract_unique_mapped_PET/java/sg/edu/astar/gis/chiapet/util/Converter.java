package sg.edu.astar.gis.chiapet.util;

public interface Converter<E> {
	public E valueOf(String line);
}

