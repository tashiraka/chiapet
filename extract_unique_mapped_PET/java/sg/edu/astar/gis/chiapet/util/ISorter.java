package sg.edu.astar.gis.chiapet.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ISorter<E extends Comparable<E>> {

	public abstract List<File> sortAndDump(BufferedReader input, int maxLoad,
			Converter<E> conv) throws IOException;
}