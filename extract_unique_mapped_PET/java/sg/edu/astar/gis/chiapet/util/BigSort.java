package sg.edu.astar.gis.chiapet.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class BigSort<E extends Comparable<E>> {
	private ISorter<E> sorter;
	private int maxLoad;
	private Converter<E> conv;
	private BufferedReader input;

	public BigSort(BufferedReader input, int maxLoad, Converter<E> conv, ISorter<E> sorter) {
		this.input = input;
		this.maxLoad = maxLoad;
		this.conv = conv;
		this.sorter = sorter;
	}
	
	public List<File> sortAndDump() {
		try {
			return sorter.sortAndDump(input, maxLoad, conv);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void mergeSortedFiles(Writer out,
			List<File> dumped, Converter<E> conv) throws IOException {
		Queue<Pair<E,Integer>> queue = new PriorityQueue<Pair<E,Integer>>();
		
		int tmpCount = dumped.size();
		List<BufferedReader> readers = new ArrayList<BufferedReader>(tmpCount);
		for (int i = 0; i < tmpCount; i++) {
			File tmpFile = dumped.get(i);
			BufferedReader r = new BufferedReader(new InputStreamReader(
					new FileInputStream(tmpFile)));
			readers.add(r);
			E ent = conv.valueOf(r.readLine());
			queue.add(new Pair<E, Integer>(ent, i));
		}
		
		while (!queue.isEmpty()) {
			Pair<E, Integer> p = queue.poll();
			E ent = p.getEins();
			int i = p.getZwei();
			
			out.write(ent.toString());
			out.write('\n');
			
			String line = readers.get(i).readLine();
			if (line != null) {
				queue.add(new Pair<E, Integer>(conv.valueOf(line), i));
			}
		}
		
		for (Reader r : readers) {
			r.close();
		}
		out.flush();
		cleanUp(dumped);
	}
	
	private void cleanUp(List<File> tmpFiles) {
		for (File f : tmpFiles) {
			f.delete();
		}
	}
}
