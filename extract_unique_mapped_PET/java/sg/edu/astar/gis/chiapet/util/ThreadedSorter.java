package sg.edu.astar.gis.chiapet.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreadedSorter<E extends Comparable<E>> implements ISorter<E> {
	/**
	 * 
	 * Depth of recursion/division.
	 * For example, if the value is 3, it means there'll be 2x2x2 = 8 threads
	 * created.
	 */
	private static final int RECURSE_DEPTH = (int) (Math.log(Runtime.getRuntime()
			.availableProcessors()) / Math.log(2));
	/**
	 * 
	 * If the number of elements in array is less or equal than this,
	 * do not "divide" again even though the recursion depth > 0.
	 */
	private static final int DIVIDE_THRESHOLD = 1000;

	private class Sort implements Runnable {
		private E[] entries;
		private int from;
		private int to;
		private int depth;

		public Sort(E[] entries, int from, int to, int depth) {
			this.entries = entries;
			this.from = from;
			this.to = to;
			this.depth = depth;
		}

		private void merge(E[] entries, int from, int middle, int to) {
			Comparable[] tmp = new Comparable[to - from];
			int p = from;
			int q = middle;
			int i = 0;
			while (p < middle || q < to) {
				if (p >= middle) {
					tmp[i] = entries[q++];
				} else if (q >= to) {
					tmp[i] = entries[p++];
				} else {
					if (entries[p].compareTo(entries[q]) <= 0) {
						tmp[i] = entries[p++];
					} else {
						tmp[i] = entries[q++];
					}
				}
				i++;
			}
			for (int x = 0, y = from; x < tmp.length; x++, y++) {
				entries[y] = (E) tmp[x];
			}
		}

		@Override
		public void run() {
			if (depth > 0 && (to - from) > DIVIDE_THRESHOLD) {
				int half = (to - from) / 2;
				Thread p = new Thread(new Sort(entries, from, from + half, depth - 1));
				Thread q = new Thread(new Sort(entries, from + half, to, depth - 1));
				p.start();
				q.start();
				try {
					p.join();
					q.join();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
				merge(entries, from, from + half, to);
			} else {
				Arrays.sort(entries, from, to);
			}
		}
	}

	private void threadedSort(E[] entries, int length) {
		try {
			Thread t = new Thread(new Sort(entries, 0, length, RECURSE_DEPTH));
			t.start();
			t.join();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	private File sortAndDumpHelper(E[] entries, int count)
			throws IOException {
		File tmpFile = File.createTempFile("tmp", null);
		BufferedWriter out = new BufferedWriter(new FileWriter(tmpFile));
		threadedSort(entries, count);
		for (int i = 0; i < count; i++) {
			out.write(entries[i].toString());
			out.write('\n');
		}
		out.close();
		return tmpFile;
	}

	@Override
	public List<File> sortAndDump(BufferedReader input, int maxLoad,
			Converter<E> conv) throws IOException {
		Comparable[] entries = new Comparable[maxLoad];
		List<File> dumped = new ArrayList<File>();
		int count = 0;
		String line;
		while ((line = input.readLine()) != null) {
			E ent = conv.valueOf(line);
			entries[count] = ent;
			count++;
			if (count == maxLoad) {
				File tmpFile = sortAndDumpHelper((E[]) entries, count);
				dumped.add(tmpFile);
				count = 0;
			}
		}
		if (count > 0) {
			File tmpFile = sortAndDumpHelper((E[]) entries, count);
			dumped.add(tmpFile);
		}
		return dumped;
	}
}
