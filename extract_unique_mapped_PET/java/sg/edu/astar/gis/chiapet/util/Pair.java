package sg.edu.astar.gis.chiapet.util;

public class Pair<P extends Comparable<P>, Q extends Comparable<Q>> implements
		Comparable<Pair<P, Q>> {
	public Pair(P eins, Q zwei) {
		this.setEins(eins);
		this.setZwei(zwei);
	}

	@SuppressWarnings("unchecked")
	@Override
	public int compareTo(Pair o) {
		int i = this.getEins().compareTo((P) o.getEins());
		if (i != 0)
			return i;
		return this.getZwei().compareTo((Q) o.getZwei());
	}

	public void setZwei(Q zwei) {
		this.zwei = zwei;
	}

	public Q getZwei() {
		return zwei;
	}

	public void setEins(P eins) {
		this.eins = eins;
	}

	public P getEins() {
		return eins;
	}

	private P eins;
	private Q zwei;
}
