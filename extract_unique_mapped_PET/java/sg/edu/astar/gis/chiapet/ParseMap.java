package sg.edu.astar.gis.chiapet;

/**
 * The input is the map file from ChIA-PET pipeline
 * The output is the filtered map file with UU tags only
 *
 * Usage: java ParseMap <Map_file_before_parse> <Map_file_after_parse>
 *
 * author: Guoliang
 * date:   2009-07-17
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class ParseMap {
	int debugLevel = 1;
	long[] distributions = new long[9];
	char[] labels = { 'N', 'U', 'M' };
	long nPairedTags = 0;

	Pattern validChrom = Pattern.compile("^chr(\\d+|[XYM])$");
	Pattern validScafd = Pattern.compile("^scaffold_?\\d+$");
	
	public ParseMap(String inFile, String outFile) throws IOException {
		Arrays.fill(distributions, 0);
		parse(inFile, outFile);

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out
						.println(labels[i]
								+ "<=>"
								+ labels[j]
								+ " "
								+ ((double) distributions[i * 3 + j] / (double) nPairedTags * 100.0)
								+ "% " + distributions[i * 3 + j]);
			}
		}
		System.out.println("Total_paired_tags: 100% " + nPairedTags);
		System.out.println("");

		System.out.println("NN= "
				+ ((double) distributions[0] / (double) nPairedTags * 100.0) + "% "
				+ (distributions[0]));
		System.out.println("UU= "
				+ ((double) distributions[4] / (double) nPairedTags * 100.0) + "% "
				+ (distributions[4]));
		System.out.println("MM= "
				+ ((double) distributions[8] / (double) nPairedTags * 100.0) + "% "
				+ (distributions[8]));
		System.out.println("NU'(=NU+UN)= "
				+ ((double) (distributions[1] + distributions[3])
						/ (double) nPairedTags * 100.0) + "% "
				+ (distributions[1] + distributions[3]));
		System.out.println("NM'(=NM+MN)= "
				+ ((double) (distributions[2] + distributions[6])
						/ (double) nPairedTags * 100.0) + "% "
				+ (distributions[2] + distributions[6]));
		System.out.println("UM'(=UM+MU)= "
				+ ((double) (distributions[5] + distributions[7])
						/ (double) nPairedTags * 100.0) + "% "
				+ (distributions[5] + distributions[7]));
		System.out.println("");

		System.out.println("NU= "
				+ ((double) (distributions[1]) / (double) nPairedTags * 100.0) + "% "
				+ (distributions[1]));
		System.out.println("UN= "
				+ ((double) (distributions[3]) / (double) nPairedTags * 100.0) + "% "
				+ (distributions[3]));
		System.out.println("");

		System.out.println("NM= "
				+ ((double) (distributions[2]) / (double) nPairedTags * 100.0) + "% "
				+ (distributions[2]));
		System.out.println("MN= "
				+ ((double) (distributions[6]) / (double) nPairedTags * 100.0) + "% "
				+ (distributions[6]));
		System.out.println("");

		System.out.println("UM= "
				+ ((double) (distributions[5]) / (double) nPairedTags * 100.0) + "% "
				+ (distributions[5]));
		System.out.println("MU= "
				+ ((double) (distributions[7]) / (double) nPairedTags * 100.0) + "% "
				+ (distributions[7]));
		System.out.println("");

		System.out.println("head-only(=UN+MN)= "
				+ ((double) (distributions[3] + distributions[6])
						/ (double) nPairedTags * 100.0) + "% "
				+ (distributions[3] + distributions[6]));
		System.out.println("tail-only(=NU+NM)= "
				+ ((double) (distributions[1] + distributions[2])
						/ (double) nPairedTags * 100.0) + "% "
				+ (distributions[1] + distributions[2]));
	}

	public void parse(String inFileName, String outFileName) throws IOException {
		BufferedReader fileIn = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File(inFileName))));
		PrintWriter fileOut = new PrintWriter(new BufferedWriter(new FileWriter(
				outFileName)));
		PairedTagMaps pairedTagMaps = new PairedTagMaps();
		String line;
		int firstTag = 1;
		nPairedTags = 0;
		while ((line = fileIn.readLine()) != null) {
			if (line.length() <= 1) {
				continue;
			}
			if (line.charAt(0) == '>') {
				nPairedTags++;
				if (firstTag == 1) {
					firstTag = 0; // no previous tag
				} else {
					processOnePairedTag(pairedTagMaps, fileOut);
					if (nPairedTags % 10000 == 0) {
//						System.out.println(nPairedTags + " paired tags are processed");
						fileOut.flush();
					}
				}

				pairedTagMaps.setPairedTags(line);
				pairedTagMaps.getHeadMaps().clear();
				pairedTagMaps.getTailMaps().clear();
			} else if (line.charAt(0) == 'H') {
				pairedTagMaps.getHeadMaps().add(line);
			} else if (line.charAt(0) == 'T') {
				pairedTagMaps.getTailMaps().add(line);
			} else {
				System.out.println("Error in this line: " + line);
			}
		}
		// process the last tag ???
		processOnePairedTag(pairedTagMaps, fileOut);

		fileIn.close();
		fileOut.close();
	}

	private int countNonRandomHits(List<String> lines) {
		int count = 0;
		for (String line : lines) {
			String[] a = line.split("\\t");
			if (validChrom.matcher(a[1]).matches() ||
					validScafd.matcher(a[1]).matches()) {
				count++;
			}
		}
		return count;
	}
	
	private void processOnePairedTag(PairedTagMaps petMaps,
			PrintWriter fileOut) {

		int h = petMaps.getHeadMaps().size();
		int t = petMaps.getTailMaps().size();
		
		// count only non-random chromosomes (format: chrXXX)
		int nHeads = countNonRandomHits(petMaps.getHeadMaps());
		int nTails = countNonRandomHits(petMaps.getTailMaps());
		
		int headLabel; // 0: for N, 1: for U, and 2: for M
		int tailLabel; // 0: for N, 1: for U, and 2: for M

		if (h == 1 && t == 1 && nHeads == 1 && nTails == 1) // UU
		{
			fileOut.println(petMaps.getPairedTags());
			fileOut.println(petMaps.getHeadMaps().get(0));
			fileOut.println(petMaps.getTailMaps().get(0));
		}

		headLabel = OccurrencesToLabel(nHeads);
		if (headLabel < 0) {
			System.out.println("Error in head numbers for paired tags: "
					+ petMaps.getPairedTags());
		}

		tailLabel = OccurrencesToLabel(nTails);
		if (tailLabel < 0) {
			System.out.println("Error in tail numbers for paired tags: "
					+ petMaps.getPairedTags());
		}

		if ((headLabel >= 0) && (headLabel <= 2) && (tailLabel >= 0)
				&& (tailLabel <= 2)) {
			distributions[headLabel * 3 + tailLabel]++;
		}
	}

	private int OccurrencesToLabel(int nOccurrences) {
		int label = -1;
		if (nOccurrences == 0) {
			label = 0;
		} else if (nOccurrences == 1) {
			label = 1;
		} else if (nOccurrences > 1) {
			label = 2;
		}

		return label;
	}

	public static void main(String[] args) throws IOException {
		/*
		 * File dir1 = new File ("."); File dir2 = new File (".."); try {
		 * System.out.println ("Current dir : " + dir1.getCanonicalPath());
		 * System.out.println ("Parent  dir : " + dir2.getCanonicalPath()); }
		 * catch(Exception e) { e.printStackTrace(); }
		 */
		if (args.length == 2) {
			new ParseMap(args[0], args[1]);
		} else {
			System.out
					.println("Usage: java ParseMap <Map_file_before_parse> <Map_file_after_parse>");
		}
	}
}

class PairedTagMaps {
	private String pairedTags;
	private List<String> headMaps = new ArrayList<String>();
	private List<String> tailMaps = new ArrayList<String>();

	public PairedTagMaps() {}

	public String getPairedTags() {
		return pairedTags;
	}

	public void setPairedTags(String pairedTags) {
		this.pairedTags = pairedTags;
	}

	public List<String> getHeadMaps() {
		return headMaps;
	}

	public void setHeadMaps(ArrayList<String> headMaps) {
		this.headMaps = headMaps;
	}

	public List<String> getTailMaps() {
		return tailMaps;
	}

	public void setTailMaps(List<String> tailMaps) {
		this.tailMaps = tailMaps;
	}
}
