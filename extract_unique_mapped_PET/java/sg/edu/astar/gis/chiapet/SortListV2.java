package sg.edu.astar.gis.chiapet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import sg.edu.astar.gis.chiapet.util.BigSort;
import sg.edu.astar.gis.chiapet.util.Converter;
import sg.edu.astar.gis.chiapet.util.NormalSorter;

public class SortListV2 {
	private static class ListV2Entry implements Comparable<ListV2Entry> {
		private String hchr;
		private int hstart;
		private int hend;
		private String tchr;
		private int tstart;
		private int tend;
		private String raw; // the whole line

		public ListV2Entry(String hchr, int hstart, int hend, String tchr,
				int tstart, int tend, String raw) {
			this.hchr = hchr;
			this.hstart = hstart;
			this.hend = hend;
			this.tchr = tchr;
			this.tstart = tstart;
			this.tend = tend;
			this.raw = raw;
		}

		@Override
		public int compareTo(ListV2Entry o) {
			int i;
			i = this.hchr.compareTo(o.hchr);
			if (i != 0) return i;
			i = this.tchr.compareTo(o.tchr);
			if (i != 0) return i;
			i = (this.hstart - o.hstart);
			if (i != 0) return i;
			i = (this.tstart - o.tstart);
			if (i != 0) return i;
			i = (this.hend - o.hend);
			if (i != 0) return i;
			return (this.tend - o.tend);
		}
		
		@Override
		public String toString() {
			return raw;
		}
	}

	private static class LineToListV2 implements Converter<ListV2Entry> {
		@Override
		public ListV2Entry valueOf(String line) {
			try {
				String[] a = line.split("\t");
				return new ListV2Entry(a[6], Integer.parseInt(a[8]), Integer
						.parseInt(a[9]), a[12], Integer.parseInt(a[14]), Integer
						.parseInt(a[15]), line);
			} catch (Exception e) {
				System.out.println(line);
				return null;
			} 
		}
	}

	public static void main(String[] args) throws Exception {
		
		Options opts = new Options();
		opts.addOption(OptionBuilder.withArgName("max").hasArg()
				.withDescription("maximum number of entries loaded into memory")
				.withLongOpt("maxload")
				.withType(Integer.class)
				.create("m"));
		
		CommandLineParser parser = new GnuParser();
		CommandLine cmd = parser.parse(opts, args);
		
		if (cmd.getArgs().length < 1) {
			new HelpFormatter().printHelp("java SortListV2 "
					+ "[options] <list file>", opts);
			return;
		}
		
		String fileName = cmd.getArgs()[0];
		int maxLoad = 1000000;
		if (cmd.hasOption('m')) {
			maxLoad = Integer.parseInt(cmd.getOptionValue('m'));
		}
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(fileName)));

		LineToListV2 converter = new LineToListV2();
		BigSort<ListV2Entry> bigSort = new BigSort<ListV2Entry>(reader, maxLoad,
				converter, new NormalSorter<ListV2Entry>());
		
		List<File> tmpFiles = bigSort.sortAndDump();

		PrintWriter writer = new PrintWriter(System.out);
		bigSort.mergeSortedFiles(writer, tmpFiles, converter);

		writer.close();
		reader.close();
	}
}
