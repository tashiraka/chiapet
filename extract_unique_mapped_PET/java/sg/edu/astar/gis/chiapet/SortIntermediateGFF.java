package sg.edu.astar.gis.chiapet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import sg.edu.astar.gis.chiapet.util.BigSort;
import sg.edu.astar.gis.chiapet.util.Converter;
import sg.edu.astar.gis.chiapet.util.NormalSorter;

public class SortIntermediateGFF {
	private static class IntermediateGFFEntry implements Comparable<IntermediateGFFEntry> {
		private String chr;
		private int pos;
		private char sign;
		private int index;

		public IntermediateGFFEntry(String chr, int pos, char sign, int index) {
			this.chr = chr;
			this.pos = pos;
			this.sign = sign;
			this.index = index;
		}

		@Override
		public int compareTo(IntermediateGFFEntry o) {
			int i;
			i = this.chr.compareTo(o.chr);
			if (i != 0) return i;
			return this.pos - o.pos;
		}
		
		@Override
		public String toString() {
			return new StringBuilder()
				.append(chr).append('\t')
				.append(pos).append('\t')
				.append(sign).append('\t')
				.append(index).toString();
		}
	}

	private static class LineToIntermediateGFF implements Converter<IntermediateGFFEntry> {
		@Override
		public IntermediateGFFEntry valueOf(String line) {
			try {
				String[] a = line.split("\t");
				return new IntermediateGFFEntry(a[0], Integer.parseInt(a[1]), a[2]
						.charAt(0), Integer.parseInt(a[3]));
			} catch (Exception e) {
				System.out.println(line);
				return null;
			} 
		}
	}

	/**
	 * If parameter is not supplied, read from stdin.
	 */
	public static void main(String[] args) throws Exception {
		
		Options opts = new Options();
		opts.addOption(OptionBuilder.withArgName("max").hasArg()
				.withDescription("maximum number of entries loaded into memory")
				.withLongOpt("maxload")
				.withType(Integer.class)
				.create("m"));
		
		CommandLineParser parser = new GnuParser();
		CommandLine cmd = parser.parse(opts, args);
		
		int maxLoad = 5000000;
		if (cmd.hasOption('m')) {
			maxLoad = Integer.parseInt(cmd.getOptionValue('m'));
		}
		
		BufferedReader reader;
		if (cmd.getArgs().length == 0) {
			// read from stdin
			reader = new BufferedReader(new InputStreamReader(System.in));
		} else {
			String fileName = cmd.getArgs()[0];
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName)));
		}

		LineToIntermediateGFF converter = new LineToIntermediateGFF();
		BigSort<IntermediateGFFEntry> bigSort = new BigSort<IntermediateGFFEntry>(
				reader, maxLoad, converter, new NormalSorter<IntermediateGFFEntry>());
		
		List<File> tmpFiles = bigSort.sortAndDump();

		PrintWriter writer = new PrintWriter(System.out);
		bigSort.mergeSortedFiles(writer, tmpFiles, converter);

		writer.close();
		reader.close();
	}
}
