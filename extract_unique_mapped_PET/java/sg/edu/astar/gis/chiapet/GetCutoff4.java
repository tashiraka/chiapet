package sg.edu.astar.gis.chiapet;

/* revise the cutoff program on 2009-06-22
 * The input is the output from getUUDensityDistribution.pl with MAX_SPAN 100,000
 *
 * Usage: java GetCutoff4 <intensityDistributionUU.dat>
 *
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class GetCutoff4 {
	final static int MAX_SPAN = 100000;
	final static int intervalSize = 100;
	final static int LOWER_BOUND = 1000;
	final static int DefaultCutoff = 3000;
	final static int debugLevel = 1;
	// final static int UPPER_BOUND = 20000;
	int nBins = MAX_SPAN / intervalSize - 1;
	NumberFormat nf = NumberFormat.getInstance();

	public GetCutoff4(String inFile) throws IOException {
		Point[] points = getDistribution(inFile);
		// printDistribution(points);
		Point[] logLogPoints = getLogLogDistribution(points);
		int lowerBound = findMode(points);
		if (lowerBound < LOWER_BOUND) {
			lowerBound = LOWER_BOUND;
		}
		// System.out.println("lowerBound = " + lowerBound);
		double maxY = getMaxY(points);
		double minY = getMinY(points);
		if (Math.abs(maxY - minY) < 10) {
			System.out.println("The data seems as constant!!! Please check ...");
		}

		int cutoff = findBestLR(lowerBound, logLogPoints);
		if (debugLevel >= 2)
			System.out.println(new File(inFile).getName() + "\t: " + cutoff
					+ " (initial cutoff)");

		printResult3(cutoff, lowerBound, logLogPoints, inFile);
	}

	private void printDistribution(Point[] points) throws IOException {
		PrintWriter fileOut = new PrintWriter(new FileOutputStream("tempout.txt"));
		for (int c1 = 0; c1 < points.length; c1++) {
			fileOut.print(points[c1].x + "\t" + points[c1].y);
			fileOut.println();
		}
		fileOut.close();
	}

	private void printResult3(int cutoff1, int lowerBound, Point[] points,
			String inFile) throws IOException {
		int cutoff;
		if ((cutoff1 >= 2000) && (cutoff1 <= 10000)) {
			cutoff = cutoff1;
		} else {
			cutoff = DefaultCutoff;
		}

		int leftUpperBound = (int) (cutoff * 0.9);
		int leftLowerBound = 500;
		// if(leftUpperBound > 8000) leftUpperBound = 8000;
		int rightLowerBound = (int) (cutoff * 1.1);
		// if(rightLowerBound < 10000) rightLowerBound = 10000;
		double leftB0, leftB1, rightB0, rightB1;
		{
			double mX = getAverageX(leftLowerBound, leftUpperBound, points);
			double mY = getAverageY(leftLowerBound, leftUpperBound, points);
			leftB1 = getB1(leftLowerBound, leftUpperBound, points, mX, mY);
			leftB0 = mY - (leftB1 * mX);
			if (debugLevel >= 2)
				System.out.println("leftB1 = " + leftB1 + ", leftB0 = " + leftB0);
		}
		{
			double mX = getAverageX(rightLowerBound, Integer.MAX_VALUE, points);
			double mY = getAverageY(rightLowerBound, Integer.MAX_VALUE, points);
			rightB1 = getB1(rightLowerBound, Integer.MAX_VALUE, points, mX, mY);
			rightB0 = mY - (rightB1 * mX);
			if (debugLevel >= 2)
				System.out.println("rightB1 = " + rightB1 + ", rightB0 = " + rightB0);
		}
		Point[] selectedPoints = selectPoints(rightLowerBound, Integer.MAX_VALUE,
				points, rightB0, rightB1);
		{
			double mX = getAverageX(rightLowerBound, Integer.MAX_VALUE,
					selectedPoints);
			double mY = getAverageY(rightLowerBound, Integer.MAX_VALUE,
					selectedPoints);
			rightB1 = getB1(rightLowerBound, Integer.MAX_VALUE, selectedPoints, mX,
					mY);
			rightB0 = mY - (rightB1 * mX);
		}

		double cutoff2;
		if (Math.abs(rightB1 - leftB1) > 1e-6) {
			cutoff2 = Math.pow(10.0, (leftB0 - rightB0) / (rightB1 - leftB1));
		} else {
			cutoff2 = cutoff;
		}

		if (cutoff > cutoff2) {
			cutoff = (int) cutoff2;
		}
		if ((cutoff <= 2000) || (cutoff >= 10000)) {
			cutoff = DefaultCutoff;
		}

		System.out.println(cutoff);
		if (debugLevel >= 2)
			System.out.println(new File(inFile).getName() + "\t: " + cutoff
					+ " (final cutoff)");

		if (debugLevel >= 2) {
			String outFile = (new File(inFile)).getAbsolutePath() + ".log";
			PrintWriter fileOut = new PrintWriter(new FileOutputStream(outFile));
			fileOut.println("Cutoff\t" + cutoff);
			fileOut.println("Cutoff2\t" + cutoff2);

			double cutoffLog = Math.log10(cutoff), minPosLog = Math.log10(lowerBound);
			for (int c1 = 0; c1 < points.length; c1++) {
				fileOut.print(points[c1].x + "\t" + points[c1].y);
				if (points[c1].x > minPosLog) {
					if (points[c1].x < cutoffLog) {
						double y = leftB0 + (leftB1 * points[c1].x);
						fileOut.print("\t" + y + "\t");
					} else if (points[c1].x > cutoffLog) {
						double y = rightB0 + (rightB1 * points[c1].x);
						fileOut.print("\t" + "\t" + y);
					}
				}
				fileOut.println();
			}
			fileOut.close();
		}
	}

	private int findMode(Point[] points) {
		if (points.length == 0) {
			return -1;
		}
		double max = points[0].y, maxIndex = points[0].x;
		for (int c1 = 1; c1 < points.length; c1++) {
			if (points[c1].y > max) {
				max = points[c1].y;
				maxIndex = points[c1].x;
			}
		}
		return (int) maxIndex;
	}

	Point[] getDistribution(String inFile) throws IOException {
		BufferedReader fileIn = new BufferedReader(new InputStreamReader(
				new FileInputStream(inFile)));
		String line;
		int[] hist = new int[nBins];
		Arrays.fill(hist, 0);
		int iBin = 0;
		while ((line = fileIn.readLine()) != null) {
			String fields[] = line.split(" ");
			for (int i = 0; i < fields.length; i++) {
				Number number = null;
				try {
					number = nf.parse(fields[i]);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (number != null) {
					hist[iBin] = number.intValue();
					iBin++;
					if (iBin >= nBins) {
						break;
					}
				}
			}
		}
		Vector tempPoints = new Vector();
		for (iBin = 0; iBin < nBins; iBin++) {
			tempPoints.addElement(new Point(iBin * intervalSize, hist[iBin]));
		}
		return Point.toArray(tempPoints);
	}

	Point[] getLogLogDistribution(Point[] inPoints) {
		Vector tempPoints = new Vector();
		for (int iBin = 0; iBin < nBins; iBin++) {
			tempPoints.addElement(new Point(Math.log10(inPoints[iBin].x + 1), Math
					.log10(inPoints[iBin].y + 1))); // plus 1 to avoid 0 in some cases
		}
		return Point.toArray(tempPoints);
	}

	/**
	 * select the points with positive residuals from the regression model
	 * 
	 * @param lowerBound
	 * @param upperBound
	 * @param points
	 * @param b0
	 * @param b1
	 * @return
	 */
	private Point[] selectPoints(int lowerBound, int upperBound, Point[] points,
			double b0, double b1) {
		Vector temp = new Vector();
		Vector positiveErrorVector = new Vector();
		double lowerBoundLog = Math.log10(lowerBound), upperBoundLog = Math
				.log10(upperBound);
		// get all the positive residuals from the regression
		for (int c1 = 0; c1 < points.length; c1++) {
			if (points[c1].x > lowerBoundLog && points[c1].x < upperBoundLog) {
				double error = points[c1].y - b0 - (b1 * points[c1].x);
				if (error > 0) {
					positiveErrorVector.addElement(new Double(error));
				}
			}
		}
		// sort the residuals
		Collections.sort(positiveErrorVector);
		// set the lower bound and upper bound for the residuals
		int nPositiveErrors = positiveErrorVector.size();
		// System.out.println("nPositiveErrors = " + nPositiveErrors);
		double lowerBoundError = 0, upperBoundError = Integer.MAX_VALUE;
		if (nPositiveErrors > 10) {
			lowerBoundError = ((Double) positiveErrorVector.get((int) (Math
					.floor(nPositiveErrors * 0.05)))).doubleValue();
			upperBoundError = ((Double) positiveErrorVector.get((int) (Math
					.floor(nPositiveErrors * 0.95)))).doubleValue();
		} else if (nPositiveErrors > 0) {
			lowerBoundError = ((Double) positiveErrorVector.firstElement())
					.doubleValue();
			upperBoundError = ((Double) positiveErrorVector.lastElement())
					.doubleValue();
		}
		for (int c1 = 0; c1 < points.length; c1++) {
			if (points[c1].x > lowerBoundLog && points[c1].x < upperBoundLog) {
				double error = points[c1].y - b0 - (b1 * points[c1].x);
				if ((error > lowerBoundError) && (error < upperBoundError)) {
					temp.addElement(new Point(points[c1].x, points[c1].y));
				}
			}
		}
		return Point.toArray(temp);
	}

	/**
	 * the best model has the smallest SSE(sum of squared errors) from two linear
	 * regression models
	 * 
	 * @param minPos
	 * @param points
	 * @return
	 */
	private int findBestLR(int minPos, Point[] points) {
		double bestSSE = Double.MAX_VALUE;
		int bestPos = -1;
		for (int curPos = minPos; curPos < MAX_SPAN; curPos = curPos + intervalSize) {
			double sse = getSSE(minPos, curPos, points);
			if (sse < bestSSE) {
				bestSSE = sse;
				bestPos = curPos;
			}
		}
		return bestPos;
	}

	/**
	 * get SSE from two linear regression models
	 * 
	 * @param minPos
	 * @param curPos
	 * @param points
	 * @return
	 */
	private double getSSE(int minPos, int curPos, Point[] points) {
		double sseLeft = 0, sseRight = 0;
		{
			double mX = getAverageX(minPos, curPos, points);
			double mY = getAverageY(minPos, curPos, points);
			double b1 = getB1(minPos, curPos, points, mX, mY);
			double b0 = mY - (b1 * mX);
			sseLeft = calcSSE(minPos, curPos, points, b0, b1);
		}
		{
			double mX = getAverageX(curPos, Integer.MAX_VALUE, points);
			double mY = getAverageY(curPos, Integer.MAX_VALUE, points);
			double b1 = getB1(curPos, Integer.MAX_VALUE, points, mX, mY);
			double b0 = mY - (b1 * mX);
			sseRight = calcSSE(curPos, Integer.MAX_VALUE, points, b0, b1);
		}
		return sseLeft + sseRight;
	}

	private double calcSSE(int lowerBound, int upperBound, Point[] points,
			double b0, double b1) {
		double sse = 0;
		double lowerBoundLog = Math.log10(lowerBound), upperBoundLog = Math
				.log10(upperBound);
		for (int c1 = 0; c1 < points.length; c1++) {
			if (points[c1].x > lowerBoundLog && points[c1].x < upperBoundLog) {
				double error = points[c1].y - b0 - (b1 * points[c1].x);
				sse = sse + (error * error);
			}
		}
		return sse;
	}

	private double getB1(int lowerBound, int upperBound, Point[] points,
			double mX, double mY) {
		double lowerBoundLog = Math.log10(lowerBound), upperBoundLog = Math
				.log10(upperBound);
		double num = 0, den = 0;
		for (int c1 = 0; c1 < points.length; c1++) {
			if (points[c1].x > lowerBoundLog && points[c1].x < upperBoundLog) {
				num = num + ((points[c1].x - mX) * (points[c1].y - mY));
				den = den + ((points[c1].x - mX) * (points[c1].x - mX));
			}
		}
		if (den == 0)
			return 0;
		else
			return num / den;
	}

	private double getAverageX(int lowerBound, int upperBound, Point[] points) {
		double lowerBoundLog = Math.log10(lowerBound), upperBoundLog = Math
				.log10(upperBound);
		int count = 0;
		double total = 0;
		for (int c1 = 0; c1 < points.length; c1++) {
			if (points[c1].x > lowerBoundLog && points[c1].x < upperBoundLog) {
				count++;
				total = total + points[c1].x;
			}
		}
		if (count == 0)
			return 0;
		else
			return total / (double) count;
	}

	// get the maximum value of Y
	private double getMaxY(Point[] points) {
		double maxY = Double.NEGATIVE_INFINITY;
		for (int c1 = 0; c1 < points.length; c1++) {
			if (maxY < points[c1].y) {
				maxY = points[c1].y;
			}
		}
		return maxY;
	}

	// get the minimum value of Y
	private double getMinY(Point[] points) {
		double minY = Double.POSITIVE_INFINITY;
		for (int c1 = 0; c1 < points.length; c1++) {
			if (minY > points[c1].y) {
				minY = points[c1].y;
			}
		}
		return minY;
	}

	private double getAverageY(int lowerBound, int upperBound, Point[] points) {
		double lowerBoundLog = Math.log10(lowerBound), upperBoundLog = Math
				.log10(upperBound);
		int count = 0;
		double total = 0;
		for (int c1 = 0; c1 < points.length; c1++) {
			if (points[c1].x > lowerBoundLog && points[c1].x < upperBoundLog) {
				count++;
				total = total + points[c1].y;
			}
		}
		if (count == 0)
			return 0;
		else
			return total / (double) count;
	}

	public static void runOneFile(String fileNameString) throws IOException {
		if (debugLevel == 1) {
			new GetCutoff4(fileNameString);
		} else {
			if (fileNameString.endsWith("_long_intensityDistributionUU.dat")) {
				System.out.println(fileNameString);
				new GetCutoff4(fileNameString);
			}
		}
	}

	public static void runAll(String dirStr) throws IOException {
		File dir = new File(dirStr);
		String[] fileList = dir.list();
		for (int c1 = 0; c1 < fileList.length; c1++) {
			String fileNameString = dir.getAbsolutePath() + File.separator
					+ fileList[c1];
			File fileTemp = new File(fileNameString);
			if (fileTemp.isDirectory()) {
				runAll(fileNameString); // recursively check for all sub-directories
			} else {
				runOneFile(fileNameString);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		if (args.length == 1) {
			if (new File(args[0]).isDirectory()) {
				runAll(args[0]);
			} else {
				runOneFile(args[0]);
			}
		} else {
			System.out.println("Usage: java GetCutoff4 <distribution_data_file>");
		}
	}
}

class Point {
	double x = 0, y = 0;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	static Point[] toArray(Vector temp) {
		Point[] result = new Point[temp.size()];
		for (int c1 = 0; c1 < result.length; c1++)
			result[c1] = (Point) (temp.elementAt(c1));
		return result;
	}
}