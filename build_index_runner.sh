#!bin/bash
mkdir tmphg19
cd tmphg19
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/bigZips/chromFa.tar.gz
tar -zxf chromFa.tar.gz
rm chr*_*.fa
cat *.fa > hg19
rm *.fa
cp ../batman/scripts/* .
cp ../batman/src/*.ini .
cp ../batman/bin/bwtformatdb .
cp ../batman/bin/reverse .
sh build_index hg19
rm hg19
mv hg19.location *.bwt *.fmv *.sa /data/hg19
cd ..
rm -r tmphg19
