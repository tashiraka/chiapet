import std.stdio;
import std.string;
import std.conv;
import std.array;
import std.algorithm;


int main(string[] args) {
  auto in_basename = args[1];
  auto tag_cluster_file = args[2];
  
  tag_clustering(in_basename, tag_cluster_file);
  
  return 0;
}

void tag_clustering(string in_basename, string tag_cluster_file) {
  auto tagc_fout = File(tag_cluster_file, "w");
  uint tagc_id = 0;
  
  for(uint chrom_index=0; chrom_index<24; chrom_index++) {

    writeln("inputing the PETs in chromoseme ", chrom_index+1, "...");
    auto tagb = input(in_basename~"."~to!string(chrom_index+1)~".for_tag_clustering",
		      chrom_index+1);

    writeln("sorting the tag boundaries by these positions...");
    tagb = array(sort!("a.position < b.position")(tagb));
    
    writeln("clustering tags in chromosome ", chrom_index+1, "...");
    auto tagc_app = appender!(Tag_cluster[]);
    Tag_cluster tmp_tagc;
    
    // (the number of encounting start of a tag boundary)
    //    - (the number of encounting end of a tag boundary)
    uint boundary_counter = 0; 
    uint tag_counter = 0;
    uint max_start_position = 0;
    
    foreach(t; tagb) {
      // encount the start of tag boundary
      if(t.boundary) {	
	// create a tag cluster
	if(boundary_counter == 0) {
	  tmp_tagc = new Tag_cluster(tagc_id, chrom_index+1, t.position);
	  tagc_id++;
	}

	boundary_counter++;
	max_start_position = t.position;

      }// encount the end of tag boundary
      else {
	boundary_counter--;
	tag_counter++;

	tmp_tagc.put_entry(t.entry);
	if(tag_counter == 1)
	  tmp_tagc.put_min_end(t.position);

	// close the end of tag cluster
	if(boundary_counter == 0) {
	  tmp_tagc.put_max_start(max_start_position);
	  tmp_tagc.put_max_end(t.position);
	  tmp_tagc.put_tag_count(tag_counter);
	  tagc_app.put(tmp_tagc);	  
	  tag_counter = 0;
	}
      }
    }

    writeln("outputing the tag clusters in chromosome ", chrom_index, "...");
    output(tagc_app.data, tagc_fout);
  }
}


void output(Tag_cluster[] tagc, File fout) {
  foreach(elem; tagc) {
    fout.writeln(">", elem.id, "\t", elem.chrom, "\t", elem.min_start, "\t", elem.max_start,
		 "\t", elem.min_end, "\t", elem.max_end, "\t", elem.tag_count);
    foreach(entry; elem.entries)
      fout.writeln(entry);
  }
}


Tag_boundary[] input(string filename, uint chrom_index) {
  auto fin = File(filename);
  auto app = appender!(Tag_boundary[]);
  
  foreach(entry; fin.byLine) {
    auto str_entry = to!string(entry);
    auto field = str_entry.split("\t");
    
    if(chrom_to_index(field[6]) == chrom_index) {
      app.put(new Tag_boundary(to!uint(field[8]), true, str_entry));
      app.put(new Tag_boundary(to!uint(field[9]), false, str_entry));
    }
    else if(chrom_to_index(field[12]) == chrom_index){
      app.put(new Tag_boundary(to!uint(field[14]), true, str_entry));
      app.put(new Tag_boundary(to!uint(field[15]), false, str_entry));
    }
    else
      writeln("error: there is PET in wrong chromosome");
  }

  return app.data;
}


uint chrom_to_index(string chrom)
{
  if(chrom.length > 3 && chrom[0..3] == "chr") {
    if(chrom[3..$] == "X") return 23;
    if(chrom[3..$] == "Y") return 24;
    else return to!int(chrom[3..$]);
  }
  else {
    writeln("error: invalid chromosome");
    return 9999;
  }
}


class Tag_boundary {
  uint position;
  bool boundary;
  string entry;
  this(uint a, bool b, string c) {position=a; boundary=b; entry=c;}
}


class Tag_cluster {
  uint id, chrom, min_start, max_start, min_end, max_end, tag_count;
  string[] entries;
  
  this(uint a, uint b, uint c) {
    id=a; chrom=b; min_start=c; max_start=0; min_end=0; max_end=0; tag_count=0;
  }

  void put_max_start(uint a) { max_start=a; }
  void put_min_end(uint a) { min_end=a; }
  void put_max_end(uint a) { max_end=a; }
  void put_tag_count(uint a) { tag_count=a; }
  void put_entry(string a) { entries ~= a; }
}
