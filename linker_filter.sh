#!bin/bash
####
# usage
###
# sh linker_filter.sh
#
cd linker_filter

echo "linker filtering..."
java -cp .:commons-cli-1.2-src/src/java LGL.chiapet.LinkerFilter \
../data/CHM040/CHM040_1.fastq ../data/CHM040/CHM040_2.fastq ../data/CHM040/CHM040 \
GTTGGATAAGATATCGCGG GTTGGAATGTATATCGCGG \
--bar-length_1 4 --bar-length_2 4 --bar-start_1 7 --bar-start_2 7

echo "convert the 1-1 file to the fasta file"
./to_fasta/bin/to_fasta \
../data/CHM040/CHM040.GTTGGATAAGATATCGCGGCCGCGATATCTTATCCAAC \
../data/CHM040/CHM040.linker_1-1.fasta

echo "convert the 2-2 file to the fasta file"
./to_fasta/bin/to_fasta \
../data/CHM040/CHM040.GTTGGAATGTATATCGCGGCCGCGATATACATTCCAAC \
../data/CHM040/CHM040.linker_2-2.fasta

echo "convert the 1-2 file to the fasta file"
./to_fasta/bin/to_fasta \
../data/CHM040/CHM040.GTTGGAATGTATATCGCGGCCGCGATATCTTATCCAAC \
../data/CHM040/CHM040.linker_1-2.fasta

echo "concatenate the linker 1-1 and 2-2 files"
cat ../data/CHM040/CHM040.linker_1-1.fasta ../data/CHM040/CHM040.linker_2-2.fasta \
> ../data/CHM040/CHM040.linker_1-1_and_2-2.fasta

cd ..
