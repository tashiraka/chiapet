import std.stdio;
import std.string;
import std.conv;
import std.array;
import std.algorithm;


int main(string[] args)
{
  auto mapped_unique_file = args[1];
  auto out_basename = args[2];

  auto fin = File(mapped_unique_file);
  
  File[24] fout;
  for(int i=0; i<24; i++) {
    fout[i] = File(out_basename~"."~to!string(i+1)~".for_tag_clustering", "w");
  }
  
  foreach(entry; fin.byLine) {
    auto field = to!string(entry).split("\t");

    // entry[6] is the chromosome in a head tag
    // entry[12] is the chromosome in a tail tag
    fout[chrom_to_index(field[6]) - 1].writeln(entry);
    fout[chrom_to_index(field[12]) - 1].writeln(entry);
  }
  
  return 0;
}


int chrom_to_index(string chrom)
{
  if(chrom.length > 3 && chrom[0..3] == "chr") {
    if(chrom[3..$] == "X") return 23;
    if(chrom[3..$] == "Y") return 24;
    else return to!int(chrom[3..$]);
  }
  else {
    writeln("error: invalid chromosome");
    return -1;
  }
}
