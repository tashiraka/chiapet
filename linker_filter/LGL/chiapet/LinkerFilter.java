package LGL.chiapet;


/**
 * Assumptions
 * 1) There are two half linkers with the SAME length
 * 2) The reads length is 36 nucleotides
 * 3) The first 20 nucleotides are from the tag
 * 4) The last 16 nucleotides are from the half-linkers
 * 5) The head and tail sequences are in two paired files with FastQ format
 * 
 */

import LGL.align.LocalAlignment;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class LinkerFilter {

    private static final String OPT_FLIP_HEAD = "flip-head";
    private static final String OPT_FLIP_TAIL = "flip-tail";
    private static final String OPT_BAR_START_1 = "bar-start_1";
    private static final String OPT_BAR_LENGTH_1 = "bar-length_1";
    private static final String OPT_BAR_START_2 = "bar-start_2";
    private static final String OPT_BAR_LENGTH_2 = "bar-length_2";
    private static final int NO_OF_LINKERS = 3;
    final static int TAG_LENGTH = 20;
    final static int MIN_SCORE = 20;
    // assume that the first 18 nucleotides are sequenced from the tags,
    // and the linker information starts from 18 (index starts with 0)
    final static int linker_start = 18;
    final static int debugLevel = 4;
    final static LocalAlignment aligner_head_1 = new LocalAlignment(20, 20); // Assume the initial sequence length is 20
    final static LocalAlignment aligner_head_2 = new LocalAlignment(20, 20); // Assume the initial sequence length is 20
    final static LocalAlignment aligner_tail_1 = new LocalAlignment(20, 20); // Assume the initial sequence length is 20
    final static LocalAlignment aligner_tail_2 = new LocalAlignment(20, 20); // Assume the initial sequence length is 20
    private static int barcode_start_1;
    private static int barcode_len_1;
    private static int barcode_start_2;
    private static int barcode_len_2;

    private static void log(Object msg) {
        // dummy method to prevent the program
        // from printing too much debugging output
    }

    private static String get_seqread_barcode(LocalAlignment a, int offset, int len) {
        // ****** assumptions ******
        // 1) the aligned strings are from the beginning of the original sequences
        // 2) the aligned str2 is the linker sequence
        // 3) the insertions and deletions are represented with '-'
        String alignedStr1 = a.getAlignedStr1();
        String alignedStr2 = a.getAlignedStr2();
        StringBuilder trimmedAlignedStr1 = new StringBuilder();
        for (int i = 0; i < alignedStr2.length(); i++) {
            if (alignedStr2.charAt(i) != '-') {
                trimmedAlignedStr1.append(alignedStr1.charAt(i));
            }
        }
        if (trimmedAlignedStr1.length() > offset + len - 1) {
            return (trimmedAlignedStr1.substring(offset, offset + len));
        }

        return "";
    }

    public static void main(String[] argv) throws IOException, ParseException {
        // commandline option processing
        Options opts = new Options();
        opts.addOption("h", "help", false, "display this help and exit");
        opts.addOption(null, OPT_BAR_START_1, true, "starting position of 'barcode' in the first linker sequence (index starts with 1)");
        opts.addOption(null, OPT_BAR_LENGTH_1, true, "length of barcode 1");
        opts.addOption(null, OPT_BAR_START_2, true, "starting position of 'barcode' in the second linker sequence (index starts with 1)");
        opts.addOption(null, OPT_BAR_LENGTH_2, true, "length of barcode 2");
        opts.addOption(null, OPT_FLIP_HEAD, false, "print reverse-complemented head sequences");
        opts.addOption(null, OPT_FLIP_TAIL, false, "print reverse-complemented tail sequences");

        CommandLineParser parser = new GnuParser();
        CommandLine cmd = parser.parse(opts, argv);

        if (cmd.hasOption('h') || cmd.getArgs().length < 5 || !cmd.hasOption(OPT_BAR_START_1) || !cmd.hasOption(OPT_BAR_LENGTH_1)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(LinkerFilter.class.getName(), opts);
            return;
        }

        String[] args = cmd.getArgs();

        String headseq_file = args[0];
        String tailseq_file = args[1];
        String output_prefix = args[2];

        String linker_1 = args[3].toUpperCase();
        String linker_1_rc = revComplement(linker_1);
        String linker_2 = args[4].toUpperCase();
        String linker_2_rc = revComplement(linker_2);

        barcode_start_1 = Integer.parseInt(cmd.getOptionValue(OPT_BAR_START_1)) - 1;
        barcode_len_1 = Integer.parseInt(cmd.getOptionValue(OPT_BAR_LENGTH_1));

        barcode_start_2 = Integer.parseInt(cmd.getOptionValue(OPT_BAR_START_2)) - 1;
        barcode_len_2 = Integer.parseInt(cmd.getOptionValue(OPT_BAR_LENGTH_2));

        String linker_1_barcode = linker_1.substring(barcode_start_1, barcode_start_1 + barcode_len_1);
        String linker_2_barcode = linker_2.substring(barcode_start_2, barcode_start_2 + barcode_len_2);

        String[] linkers = new String[NO_OF_LINKERS];
        linkers[0] = linker_1 + linker_1_rc;
        linkers[1] = linker_2 + linker_2_rc;
        linkers[2] = linker_2 + linker_1_rc;

        int[] linker_counters = new int[NO_OF_LINKERS];
        String[] linker_type = new String[NO_OF_LINKERS];
        linker_type[0] = "1-1";
        linker_type[1] = "2-2";
        linker_type[2] = "1-2";

        PrintWriter file_out[] = new PrintWriter[NO_OF_LINKERS];

        file_out[0] = new PrintWriter(new FileOutputStream(String.format("%s.%s%s", output_prefix, linker_1, linker_1_rc)));
        file_out[1] = new PrintWriter(new FileOutputStream(String.format("%s.%s%s", output_prefix, linker_2, linker_2_rc)));
        file_out[2] = new PrintWriter(new FileOutputStream(String.format("%s.%s%s", output_prefix, linker_2, linker_1_rc)));

        PrintWriter nolinker_file = new PrintWriter(new FileOutputStream(
                output_prefix + ".NNN"));

        BufferedReader headseq_reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(headseq_file)));
        BufferedReader tailseq_reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(tailseq_file)));

        String head_line = null, tail_line = null;
        int total = 0, count_without_linker = 0;
        int scores[] = new int[NO_OF_LINKERS];

        // array for alignment scores
        int[] halfLinkerScoreHist;               // for scores from the individual half linker
        int[] halfLinkerScoreDiffHist;           // for two half linker in the same PET
        int[] bestScoreHist;            // for scores from a full linker
        int[] secondBestScoreDiffHist; // for the difference between the best score and the second best score
        int halfLinkerLength = linker_1.length();
        halfLinkerScoreHist = new int[halfLinkerLength + 1];
        halfLinkerScoreDiffHist = new int[halfLinkerLength + 1];
        bestScoreHist = new int[halfLinkerLength * 2 + 1];
        secondBestScoreDiffHist = new int[halfLinkerLength * 2 + 1];
        Arrays.fill(halfLinkerScoreHist, 0);
        Arrays.fill(halfLinkerScoreDiffHist, 0);
        Arrays.fill(bestScoreHist, 0);
        Arrays.fill(secondBestScoreDiffHist, 0);

        PrintWriter fileOutScore = null;
        if (debugLevel > 3) {
            fileOutScore = new PrintWriter(new FileOutputStream(output_prefix
                    + ".scores.txt"));
        }

        // read sequences and align with the linkers
        while ((head_line = headseq_reader.readLine()) != null) {
            head_line = headseq_reader.readLine();
            tail_line = tailseq_reader.readLine();
            tail_line = tailseq_reader.readLine();

            total++;
            if (debugLevel > 4) {
                if (total % 1000000 == 0) {
                    log("Total: " + total / 1000000 + "M , No linker: " + count_without_linker);
                    for (int c1 = 0; c1 < NO_OF_LINKERS; c1++) {
                        log("\t" + linkers[c1] + "\t" + linker_counters[c1]);
                    }
                }
            }
            Arrays.fill(scores, 0);

            // get the linker part from the reads
            String head_linker = head_line.substring(linker_start, head_line.length()).toUpperCase().replace('.', 'N');
            String tail_linker = tail_line.substring(linker_start, tail_line.length()).toUpperCase().replace('.', 'N');

            // align the linker part with half linkers
            aligner_head_1.align(head_linker, linker_1);
            aligner_head_2.align(head_linker, linker_2);
            aligner_tail_1.align(tail_linker, linker_1);
            aligner_tail_2.align(tail_linker, linker_2);

            // get the alignment score from half linkers
            int score_head_1 = aligner_head_1.getScore();
            int score_head_2 = aligner_head_2.getScore();
            int score_tail_1 = aligner_tail_1.getScore();
            int score_tail_2 = aligner_tail_2.getScore();

            // update half-linker score distribution
            if ((score_head_1 >= 0) && (score_head_1 <= halfLinkerLength)) {
                halfLinkerScoreHist[score_head_1]++;
            }
            if ((score_tail_1 >= 0) && (score_tail_1 <= halfLinkerLength)) {
                halfLinkerScoreHist[score_tail_1]++;
            }
            if ((score_head_2 >= 0) && (score_head_2 <= halfLinkerLength)) {
                halfLinkerScoreHist[score_head_2]++;
            }
            if ((score_tail_2 >= 0) && (score_tail_2 <= halfLinkerLength)) {
                halfLinkerScoreHist[score_tail_2]++;
            }

            // update score difference distribution from half-linkers
            int scoreDiff_halfLinker = Math.abs(score_head_1 - score_tail_1);
            if ((scoreDiff_halfLinker >= 0) && (scoreDiff_halfLinker <= halfLinkerLength)) {
                halfLinkerScoreDiffHist[scoreDiff_halfLinker]++;
            }
            scoreDiff_halfLinker = Math.abs(score_head_2 - score_tail_2);
            if ((scoreDiff_halfLinker >= 0) && (scoreDiff_halfLinker <= halfLinkerLength)) {
                halfLinkerScoreDiffHist[scoreDiff_halfLinker]++;
            }

            // determine the barcodes
            String head_barcode_1 = get_seqread_barcode(aligner_head_1, barcode_start_1, barcode_len_1);
            String head_barcode_2 = get_seqread_barcode(aligner_head_2, barcode_start_2, barcode_len_2);

            String tail_barcode_1 = get_seqread_barcode(aligner_tail_1, barcode_start_1, barcode_len_1);
            String tail_barcode_2 = get_seqread_barcode(aligner_tail_2, barcode_start_2, barcode_len_2);

            // check whether the barcode from the reads is the same as any of the linker sequences
            // If yes, keep the original alignment score
            // otherwise, replace the alignment score with 0
            int chimeras = 0;
            if (head_barcode_1.equals(linker_1_barcode) && tail_barcode_1.equals(linker_1_barcode)) {
                scores[0] = score_head_1 + score_tail_1;
            } else if (head_barcode_2.equals(linker_2_barcode) && tail_barcode_2.equals(linker_2_barcode)) {
                scores[1] = score_head_2 + score_tail_2;
            } else if (head_barcode_1.equals(linker_1_barcode) && tail_barcode_2.equals(linker_2_barcode)) {
                scores[2] = score_head_1 + score_tail_2; // chimeric score
                chimeras = 0;
            } else if (head_barcode_2.equals(linker_2_barcode) && tail_barcode_1.equals(linker_1_barcode)) {
                scores[2] = score_head_2 + score_tail_1; // chimeric score
                chimeras = 1;
            }


            // get the best alignment score and the best linker
            int best = 0, secondbest = 0, bestindex = -1;
            for (int i = 0; i < NO_OF_LINKERS; i++) {
                if (scores[i] >= best) {
                    secondbest = best;
                    bestindex = i;
                    best = scores[i];
                } else if (scores[i] > secondbest) {
                    secondbest = scores[i];
                }
            }
            if ((best >= 0) && (best <= halfLinkerLength * 2)) {
                bestScoreHist[best]++;
            }
            int secondBestDiff = Math.abs(best - secondbest);
            if ((secondBestDiff >= 0) && (secondBestDiff <= halfLinkerLength * 2)) {
                secondBestScoreDiffHist[secondBestDiff]++;
            }

            if (debugLevel > 3) {
                fileOutScore.println(best + "\t" + secondbest + "\t"
                        + (best - secondbest));
            }

            final boolean flipHeadSeq = cmd.hasOption(OPT_FLIP_HEAD);
            final boolean flipTailSeq = cmd.hasOption(OPT_FLIP_TAIL);

            if (best >= MIN_SCORE) {
                linker_counters[bestindex]++;
                // file_out[bestindex].println(">PET_" + linker_counters[bestindex]);

                String head_out = head_line.substring(0, TAG_LENGTH);
                String tail_out = tail_line.substring(0, TAG_LENGTH);

                if (flipHeadSeq) {
                    head_out = revComplement(head_out);
                }
                if (flipTailSeq) {
                    tail_out = revComplement(tail_out);
                }

                file_out[bestindex].println(head_out + "\t" + tail_out);

                if (debugLevel > 3) {
                    debugMsg1(file_out, head_line, tail_line, scores, head_linker, tail_linker, aligner_head_1,
                            aligner_tail_1, aligner_head_2, aligner_tail_2, chimeras, bestindex);
                }
            } else {
                if (debugLevel > 4) {
                    log(head_line.substring(0, TAG_LENGTH) + "\t"
                            + tail_line.substring(0, TAG_LENGTH) + "\tBest Score: " + best
                            + "\tScore Diff: " + (best - secondbest));
                }

                count_without_linker++;
                nolinker_file.println(String.format("%d\t%d\t%d\t%s\t%s",
                        best, secondbest, best - secondbest, head_line, tail_line));
            }

            // skip the extra lines from FastQ format
            headseq_reader.readLine();
            headseq_reader.readLine();
            tailseq_reader.readLine();
            tailseq_reader.readLine();
        }

        headseq_reader.close();
        tailseq_reader.close();

        for (int i = 0; i < file_out.length; i++) {
            file_out[i].close();
        }

        nolinker_file.close();
        if (debugLevel > 3) {
            fileOutScore.close();
        }

        System.out.println(String.format("Total\t%d", total));
        System.out.println(String.format("Count Without Proper Linker\t%d\t(%.2f%%)", count_without_linker, count_without_linker * 100.0 / total));
        for (int i = 0; i < NO_OF_LINKERS; i++) {
            System.out.println(String.format("%s\t%s\t%d\t(%.2f%%)", linker_type[i], linkers[i], linker_counters[i], linker_counters[i] * 100.0 / total));
        }

        PrintWriter halfLinkerScoreHist_file = new PrintWriter(new FileOutputStream(
                output_prefix + ".halfLinkerScoreHist.txt"));
        for (int i = 0; i < halfLinkerScoreHist.length; i++) {
            halfLinkerScoreHist_file.println(i + "\t" + halfLinkerScoreHist[i]);
        }
        halfLinkerScoreHist_file.close();

        PrintWriter halfLinkerScoreDiffHist_file = new PrintWriter(new FileOutputStream(
                output_prefix + ".halfLinkerScoreDiffHist.txt"));
        for (int i = 0; i < halfLinkerScoreHist.length; i++) {
            halfLinkerScoreDiffHist_file.println(i + "\t" + halfLinkerScoreDiffHist[i]);
        }
        halfLinkerScoreDiffHist_file.close();

        PrintWriter bestScoreHist_file = new PrintWriter(new FileOutputStream(
                output_prefix + ".bestScoreHist.txt"));
        for (int i = 0; i < bestScoreHist.length; i++) {
            bestScoreHist_file.println(i + "\t" + bestScoreHist[i]);
        }
        bestScoreHist_file.close();

        PrintWriter secondBestScoreDiffHist_file = new PrintWriter(new FileOutputStream(
                output_prefix + ".secondBestScoreDiffHist.txt"));
        for (int i = 0; i < secondBestScoreDiffHist.length; i++) {
            secondBestScoreDiffHist_file.println(i + "\t" + secondBestScoreDiffHist[i]);
        }
        secondBestScoreDiffHist_file.close();
    }

    private static void debugMsg1(PrintWriter[] fileOut, String lineF,
            String lineR, int[] scores, String read1, String read2,
            LocalAlignment alignerX1, LocalAlignment alignerX2, LocalAlignment alignerY1,
            LocalAlignment alignerY2, int chimeras, int bestindex) {
        fileOut[bestindex].println(lineF + "\t"
                + revComplement(lineR));
        fileOut[bestindex].println(read1 + "\t" + read2);
        fileOut[bestindex].println("Score: " + scores[bestindex]);
        if (bestindex == 0) {
            String seqread_x1_barcode = get_seqread_barcode(aligner_head_1, barcode_start_1, barcode_len_1);
            String seqread_x2_barcode = get_seqread_barcode(aligner_tail_1, barcode_start_1, barcode_len_1);

            fileOut[bestindex].println(alignerX1.getAlignedStr1() + "\t"
                    + alignerX2.getAlignedStr1() + "\t" + seqread_x1_barcode + "\t" + seqread_x2_barcode);
            fileOut[bestindex].println(alignerX1.getAlignedStr2() + "\t"
                    + alignerX2.getAlignedStr2());
            fileOut[bestindex].println(alignerX1.getAlignedStatus() + "\t"
                    + alignerX2.getAlignedStatus());
            fileOut[bestindex].println(alignerX1.getMinI() + "\t"
                    + alignerX1.getMaxI());
            fileOut[bestindex].println(alignerX1.getMinJ() + "\t"
                    + alignerX1.getMaxJ());
            fileOut[bestindex].println(alignerX2.getMinI() + "\t"
                    + alignerX2.getMaxI());
            fileOut[bestindex].println(alignerX2.getMinJ() + "\t"
                    + alignerX2.getMaxJ());
        } else if (bestindex == 1) {
            String seqread_y1_barcode = get_seqread_barcode(aligner_head_2, barcode_start_2, barcode_len_2);
            String seqread_y2_barcode = get_seqread_barcode(aligner_tail_2, barcode_start_2, barcode_len_2);

            fileOut[bestindex].println(alignerY1.getAlignedStr1() + "\t"
                    + alignerY2.getAlignedStr1() + "\t" + seqread_y1_barcode + "\t" + seqread_y2_barcode);
            fileOut[bestindex].println(alignerY1.getAlignedStr2() + "\t"
                    + alignerY2.getAlignedStr2());
            fileOut[bestindex].println(alignerY1.getAlignedStatus() + "\t"
                    + alignerY2.getAlignedStatus());
            fileOut[bestindex].println(alignerY1.getMinI() + "\t"
                    + alignerY1.getMaxI());
            fileOut[bestindex].println(alignerY1.getMinJ() + "\t"
                    + alignerY1.getMaxJ());
            fileOut[bestindex].println(alignerY2.getMinI() + "\t"
                    + alignerY2.getMaxI());
            fileOut[bestindex].println(alignerY2.getMinJ() + "\t"
                    + alignerY2.getMaxJ());
        } else {
            if (chimeras == 0) {
                String seqread_x1_barcode = get_seqread_barcode(aligner_head_1, barcode_start_1, barcode_len_1);
                String seqread_y2_barcode = get_seqread_barcode(aligner_tail_2, barcode_start_2, barcode_len_2);
                fileOut[bestindex].println(alignerX1.getAlignedStr1() + "\t"
                        + alignerY2.getAlignedStr1() + "\t" + seqread_x1_barcode + "\t" + seqread_y2_barcode);
                fileOut[bestindex].println(alignerX1.getAlignedStr2() + "\t"
                        + alignerY2.getAlignedStr2());
                fileOut[bestindex].println(alignerX1.getAlignedStatus() + "\t"
                        + alignerY2.getAlignedStatus());
                fileOut[bestindex].println(alignerX1.getMinI() + "\t"
                        + alignerX1.getMaxI());
                fileOut[bestindex].println(alignerX1.getMinJ() + "\t"
                        + alignerX1.getMaxJ());
                fileOut[bestindex].println(alignerY2.getMinI() + "\t"
                        + alignerY2.getMaxI());
                fileOut[bestindex].println(alignerY2.getMinJ() + "\t"
                        + alignerY2.getMaxJ());
            } else {
                String seqread_y1_barcode = get_seqread_barcode(aligner_head_2, barcode_start_2, barcode_len_2);
                String seqread_x2_barcode = get_seqread_barcode(aligner_tail_1, barcode_start_1, barcode_len_1);
                fileOut[bestindex].println(alignerY1.getAlignedStr1() + "\t"
                        + alignerX2.getAlignedStr1() + "\t" + seqread_y1_barcode + "\t" + seqread_x2_barcode);
                fileOut[bestindex].println(alignerY1.getAlignedStr2() + "\t"
                        + alignerX2.getAlignedStr2());
                fileOut[bestindex].println(alignerY1.getAlignedStatus() + "\t"
                        + alignerX2.getAlignedStatus());
                fileOut[bestindex].println(alignerY1.getMinI() + "\t"
                        + alignerY1.getMaxI());
                fileOut[bestindex].println(alignerY1.getMinJ() + "\t"
                        + alignerY1.getMaxJ());
                fileOut[bestindex].println(alignerX2.getMinI() + "\t"
                        + alignerX2.getMaxI());
                fileOut[bestindex].println(alignerX2.getMinJ() + "\t"
                        + alignerX2.getMaxJ());
            }
        }
    }
    private static char[] complTable = new char[255];

    static {
        complTable['A'] = 'T';
        complTable['C'] = 'G';
        complTable['G'] = 'C';
        complTable['T'] = 'A';
        complTable['N'] = 'N';

        complTable['a'] = 't';
        complTable['c'] = 'g';
        complTable['g'] = 'c';
        complTable['t'] = 'a';
        complTable['n'] = 'n';
    }



    private static String revComplement(String seq) {
        StringBuilder result = new StringBuilder(seq);
        result.reverse();
        for (int i = seq.length() - 1; i >= 0; i--) {
            switch (result.charAt(i)) {
                case 'A':
                case 'C':
                case 'G':
                case 'T':
                case 'N':
                case 'a':
                case 'c':
                case 'g':
                case 't':
                case 'n':
                    result.setCharAt(i, complTable[result.charAt(i)]);
                    break;
                default:
                    break;
            }
        }
        return result.toString();
    }
}
