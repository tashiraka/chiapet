import std.stdio;
import std.string;
import std.conv;


int main(string[] args) {
  auto linker_filtered_file = args[1];
  auto out_file = args[2];

  writeln("input");
  auto read = input_read(linker_filtered_file);
  writeln("output");
  convert_out_fasta(read, out_file);
  
  return 0;
}

string[] input_read(string file) {
  auto fin = File(file);
  string[] read;

  int i = 0;
  foreach(line; fin.byLine) {
    if(i == 0 && line != "") {
      read ~= to!string(line);
      i = 11;
    }
    i--;
  }
  
  return read;
} 


void convert_out_fasta(string[] read, string out_file) {
  auto fout1 = File(out_file, "w");
  auto fasta_line = ">PET_";
  uint PET_index = 0;
  foreach(r; read) {
    fout1.writeln(fasta_line ~ to!string(PET_index));
    fout1.writeln(r);
    PET_index++;
  }
}
