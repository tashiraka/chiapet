#!/bin/bash
cd extract_unique_mapped_PET

echo "filtering out the mapped unique PETs by the map file..."
python2.6 map_to_list.py -S ../data/CHM040/CHM040.extract_unique_mapped_list.info \
< ../data/CHM040/CHM040.map > ../data/CHM040/CHM040.unique_mapped_list

echo "sorting the mapped unique PETs and concatenating the PETs in the same position..."
java -Xmx2048m -cp bin:lib/java/commons-cli-1.2.jar sg.edu.astar.gis.chiapet.SortListV2 \
-m 1000000 ../data/CHM040/CHM040.unique_mapped_list | python2.6 extract_uniq.py \
> ../data/CHM040/CHM040.unique_mapped_PET

cd ..