#!bin/bash
echo "Merging identical PETs for faster mapping..."
grep -v '^>' data/CHM040/CHM040.linker_1-1_and_2-2.fasta | sort | \
python2.6 batman/bin/flatten_fasta.py CHM040 > \
data/CHM040/CHM040.faster_mapping.txt

echo "Batman is mapping..."
./batman/bin/batman --printblanks --scanboth -n 1 -m 2 -f 3 \
-g data/hg19/hg19 -q data/CHM040/CHM040.faster_mapping.txt -o data/CHM040/CHM040.encode

echo "Decoding the batman's output"
./batman/bin/decode --location=data/hg19/hg19.location -g data/hg19/hg19 \
-i data/CHM040/CHM040.encode -o data/CHM040/CHM040.bat

echo "Convert the decoded file to the .map format"
cat data/CHM040/CHM040.bat | python2.6 batman/bin/batmap.py > data/CHM040/CHM040.map