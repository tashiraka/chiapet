import sys

def main():
  if len(sys.argv) < 2:
    print "Usage: grep -v '^>' <fasta> | sort | flatten_fasta.py <library name>"
    return 1
  
  lib = sys.argv[1]
  count = 0
  id = 1
  prev = None
  for line in sys.stdin:
    if prev and prev != line:
      print '>%s_U-%d_COUNT:%d' %(lib, id, count)
      print prev,
      count = 1
      id += 1
    else:
      count += 1
      
    prev = line

  # Last one
  print '>%s_U-%d_COUNT:%d' %(lib, id, count)
  print prev,
  
if __name__ == '__main__':
  sys.exit(main())  