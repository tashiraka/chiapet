'''
Converts Batman's L3 decoded output to Map format recognized by ChIA-PET pipeline.
'''

import re, sys
from optparse import OptionParser

def dump(id, hseq, tseq, hits):
  print '\t'.join((id, hseq, tseq))
  for hit in hits:
    print '\t'.join(hit)

def main():
  parser = OptionParser(usage="usage: %prog [options]")
  parser.add_option('--strict', action='store_true', help='drop hits on random chromosome/scaffold')
  (opts, args) = parser.parse_args()

  hits = list()
  id = None
  
  for line in sys.stdin:
    line = line.strip()
    if not line: 
      continue
    elif line[0] == '>':
      if id: dump(id, hseq, tseq, hits)
      
      id, hseq, tseq = line.split('\t')
      hits = list()
    elif line[0] in 'HT':
      a = [s.strip() for s in line.split('\t')]
      
      # Don't print no-hits
      if not a[2]: continue
      
      # Print hit locations one by one
      pos = (p for p in a[2].split(';') if p)
      for p in pos:
        ch, loc = p.split(':')
        
        if opts.strict:
          # allow only chrXX, scaffoldXX, or scaffold_XX
          if not (re.match(r'^chr(\d+|[XYM])$', ch) or re.match(r'^scaffold_?\d+$', ch)): 
            continue
        
        hits.append((a[0], ch, a[1], loc, a[3], a[4]))

  # last entry
  if id: dump(id, hseq, tseq, hits)

if __name__ == '__main__':
  sys.exit(main())
  