//remember to remove current tag as parameter
//{-----------------------------  INCLUDE FILES  -------------------------------------------------/
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <xmmintrin.h>
#include <emmintrin.h>
//#include <dvec.h>
#include <getopt.h>
extern "C" 
{
	#include <time.h>
	#include "MemManager.h"
	#include "MiscUtilities.h"
	#include "TextConverter.h"
	#include "BWT.h"
}
//}-----------------------------  INCLUDE FILES  -------------------------------------------------/

//{-----------------------------  DEFINES  -------------------------------------------------/

#define MAXDES 500 
#define MAXTAG 80
//#define LOOKUPSIZE 6
//#define ARRAYLENGTH 1000000//1000000//0//3415291//100000//70
//#define STRINGLENGTH 36//36//6+EXTRA//6+EXTRA//36
//#define HALFSTRINGLENGTH 18
//#define QUARTERSTRINGLENGTH 9
#define EXTRA 0//Stuff like cr/lf at string seperators in the input file
#define INTEGERSIZE 8 //integer size
#define PACKEDBITSIZE 2
#define BITMASK 1 //3
//#define DISKBUFFERSIZE 1000
#define FALSE 0
#define TRUE 1
#define NOMISMATCHES 100 

#define START_OF_MARK 9//18  //Start of scanning the tag for the pruning of one mismatch...
#define BRANCHTHRESHOLD 80 //30 //Threshold at which to check the BWT instead of branching
//}-----------------------------  DEFINES  -------------------------------------------------/
//{-----------------------------  STRUCTS  -------------------------------------------------/
struct Header
{
	char ID[3] ;
	unsigned MAXHITS;
	char HITMODE;
	char Index_Count;
	int Tag_Length;
	char Print_Desc;//print the tag desc?
}__attribute__((__packed__));

struct SARange
{

	unsigned long Start;
	unsigned long End;
	int Level;//at what relative node are we?
	char Mismatches;//number of mismatches at this level
	int Tag;//Tag number
	unsigned Mismatch_Pos;//6|6|...
	unsigned Mismatch_Char;//2|2|...
	
};

struct Range
{
	unsigned Start;
	unsigned End;
	int Label;//Final Label of the range...
};

struct Mismatches_Record
{

	int 	Gap;
	int Mismatch_Pos;//6|6|...
	unsigned Mismatch_Char;//2|2|...


}__attribute__((__packed__));

struct Output_Record
{
	unsigned Tag;
	unsigned  Start;
	char Index;
	char Mismatches;
	int Gap;
}__attribute__((__packed__));
#define REVERSE 1
#define FORWARD 0


struct Branches
{
	long Is_Branch [4];
};
//}-----------------------------  STRUCTS  -------------------------------------------------/*

//{-----------------------------  FUNCTION PRTOTYPES  -------------------------------------------------/*

void Convert_To_Reverse(SARange &Tag);
void Build_Tables();
void Allocate_Memory();
void Init_Variables();
void Load_Indexes();
void Verbose();
void Open_Files();
void Build_Preindex_Forward(Range Range, int Level, int Bit);
void Build_Preindex_Backward(Range Range, int Level, int Bit);
void Parse_Command_line(int argc, char* argv[]);
void One_Branch(struct SARange Tag,BWT *fmi);
void Branch_Detect (const struct SARange Tag,BWT *fmi,int Start);
void Branch_Detect_Backwards (const struct SARange Tag,BWT *fmi,int Start);
//void Mark_Branches(char* Current_Tag,int Start,int StringLength,BWT *fmi);//marks potential branches and impossible branches
void Set_Bit(char Bit_Array[],unsigned long Position);
void Clear_Bit(char Bit_Array[],long Position);
void Print_LocationX (struct SARange & Tag);
void Get_SARange_Fast( long New_Char,struct SARange & Range,BWT *fmi);

int Read_Bits(char Bit_Array[],long Position);// read one bit
void sse2_bit_count(unsigned Start,unsigned End,BWT *fmi);
//int One_Count(Iu32vec4 b);

void Reverse(struct SARange & Tag,int Start,int StringLength);
void Backwards(struct SARange & Tag,int Start,int StringLength);

void Search_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi);

void Search_Forwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Forwards(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Forwards_0X(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Forwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_X01(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_X01_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_01X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_01X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Half_Tag_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);

void Search_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Half_Tag_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Backwards_Exact_X0(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Backwards(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_X10(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_X10_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_10X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_10X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);

struct SARange Get_SARange( long New_Char,struct SARange Range,BWT *fmi);

FILE* File_Open(const char* File_Name,const char* Mode);
BWT* initFMI(const char* BWTCodeFileName,const char* BWTOccValueFileName);
//}-----------------------------  FUNCTION PRTOTYPES  -------------------------------------------------/*

//{---------------------------- GLOBAL VARIABLES -------------------------------------------------
BWT *fwfmi,*revfmi;

FILE* Input_File;
FILE* Output_File;
FILE* Unique_File;
FILE* Mishit_File;

struct Output_Record Record;
struct Mismatches_Record Mismatches;
struct SARange* Exact_Match_Left;
struct SARange* Exact_Match_Right;

int Left_Mishits_Pointer;
int Right_Mishits_Pointer;
int Mismatches_Forward_Pointer;
int Mismatches_Backward_Pointer;
int Two_Mismatches_At_End_Pointer; 
int Two_Mismatches_At_End_Forward_Pointer=0;
int Possible_20_Pointer=0;
int Possible_02_Pointer=0;
int FMIndex;//keeps track of which fm-index to use...
int One_Mismatch_Already;

struct SARange* BMHStack;//[2*STRINGLENGTHMAX];
struct SARange* FSHStack;
struct SARange* FSHStackX0X;
struct SARange* FSSStack;
struct SARange* BMStack;
struct SARange* BMStack_X11;
struct SARange* BMStack_X11H;
struct SARange* PSBStack;


struct SARange* Exact_Match_Forward;
struct SARange* Exact_Match_Backward;
struct SARange* Left_Mishits;//stores mismatches in first half
struct SARange* Right_Mishits;//stores mismatches in first half
struct SARange* Mismatches_Forward;//stores possible 2-mismatches
struct SARange* Mismatches_Backward;//stores possible 2-mismatches
struct SARange* Two_Mismatches_At_End_Forward;//stores 2-mismatches whose last mismatch occurs at the last nuce..
struct SARange* Two_Mismatches_At_End;//stores 2-mismatches whose last mismatch occurs at the last nuce..
struct SARange* Possible_20;//stores possible 2-mismatches in right half... 
struct SARange* Possible_02;//stores possible 2-mismatches in right half... 

SARange Branch_Ranges[4];
SARange Temp_Branch_Ranges[4];
SARange Temp_Branch_Ranges2[4];

struct Header Header;
MMPool *mmPool;

char* Source;
char* Do_Branch;//decides on quality whether to branch or not...
char* High_Quality;
char* Low_Quality;
char* Do_All;
char Translated_String[100];//STRINGLENGTHMAX+1];
int debug;
unsigned long Character_Count[256];
char Char_To_Code[256];
char Temp_Char_Array[20];
char* Read_Buffer;
unsigned long SOURCELENGTH;
unsigned int FWDInverseSA0,REVInverseSA0;
long int Branch_Characters[4],Temp_BC[4],Temp_BC1[4],Temp_BC2[4];//counts the characters in a SARange.
unsigned Hits,Total_Hits=0;
unsigned Tags_Read;
clock_t Start_Time,End_Time; 

const char* Code_To_Char="acgt";
int c;//to hold a character temporarily
unsigned Forward_Start_Lookup[4],Forward_End_Lookup[4],Backward_Start_Lookup[4],Backward_End_Lookup[4];
unsigned* Forward_Start_LookupX;
unsigned* Forward_End_LookupX;
unsigned* Backward_Start_LookupX;
unsigned* Backward_End_LookupX;

char Minimum_Quality,Minimum_Quality_Location;
char QUALITY_THRESHOLD=127;//']';
char SUPER_QUALITY=127;
char PRINT_MISHITS=FALSE;
char PRINT_DESC=TRUE;//FALSE;
char MAX_MISMATCHES=5;//3;//2;
int LOOKUPSIZE=6;
char* BWTFILE ; 
char* OCCFILE ;
char* REVBWTINDEX;
char* REVOCCFILE;
char* GENOMEFILE;

char BWTFILE_DEFAULT[] = "chr11.bwt";//"genome.bwt";// 
char OCCFILE_DEFAULT[] ="chr11.fmv";//"genome.fmv";//
char REVBWTINDEX_DEFAULT[] ="revchr11.bwt";//"revgenome.bwt";//
char REVOCCFILE_DEFAULT[] ="revchr11.fmv";//"revgenome.fmv";//
char GENOMEFILE_DEFAULT[]="chr11";
//char MISHITFILE_DEFAULT[]="mishits";
char Description[MAXDES+1];
char Current_Tag_Buffer[MAXTAG+1];
char* Current_Tag=&Current_Tag_Buffer[1];
//char Current_Tag[MAXTAG+1];
char Quality[MAXTAG];
char Quality_Bound;
char Maximum_Quality;
char Sum;

char* PATTERNFILE;
char PATTERNFILE_DEFAULT[]="bigchr11.fq";//"100chr11.fq";//"rikky.flat";
char* HITSFILE;
char HITSFILE_DEFAULT[]="hits.txt";
char* UNIQUEFILE;
char UNIQUEFILE_DEFAULT[]="unique.txt";
char* GENFILE;
//char* MISHITFILE;
unsigned DISKBUFFERSIZE =1000;
int MAXHITS =1;
#define DEFAULT 0
#define DEEP 1
char UNIQUEHITS = FALSE;//do we need to separate uniquehits to a file?
int HITMODE = DEFAULT;
char USEQUALITY=FALSE;

int STRINGLENGTH=36;// 36//36//6+EXTRA//6+EXTRA//36
int HALFSTRINGLENGTH=18;// 18
int QUARTERSTRINGLENGTH=9;// 9
int LH,RH,LHQL,LHQR,RHQL,RHQR;

char ALLHITS=FALSE;
char COUNT_ALLHITS=TRUE;
char ONEFMINDEX =FALSE;
char MISMATCHES_TO_TRY_FIRST_LEFT=2;//4;
char MISMATCHES_TO_TRY_FIRST_RIGHT=2;//4;

int Actual_Tag;
//}---------------------------- GLOBAL VARIABLES -------------------------------------------------

//{---------------------------- Command Line  -------------------------------------------------
option Long_Options[]=
{
{"help",0,NULL,'h'},
{"query",1,NULL,'q'},
{"output",1,NULL,'o'},
{"uniquefile",1,NULL,'u'},
{"genome",1,NULL,'g'},
{"buffersize",1,NULL,'b'},
{"maxhits",1,NULL,'m'},
{"format",1,NULL,'f'},
{"singleindex",0,NULL,'s'},
//{"length",1,NULL,'l'},
{"thresholdquality",1,NULL,'t'},
{"keepthreshold",1,NULL,'k'},
{"mishits",0,NULL,'x'},
{"maxmismatches",1,NULL,'n'},
{"noquality",0,NULL,'p'},
{0,0,0,0}
};
//}---------------------------- Command Line -------------------------------------------------
	

int main(int argc, char* argv[])
{
//{-----------------------------  INITIALIZE ----------------------------------------------

	Parse_Command_line(argc,argv);	
	Open_Files();
	Init_Variables();
	Allocate_Memory();
	Load_Indexes();	
	Build_Tables();
	Verbose();
	
//}-----------------------------  INITIALIZE ----------------------------------------------
	Start_Time=clock();
	struct SARange Range,TRange;
	//char* Current_Tag;//later make global...
	int Start;
	Actual_Tag= -1;
//---------------------------------------------------------------------
	Header.ID[0]='B';//"BAT";
	Header.ID[1]='A';//"BAT";
	Header.ID[2]='T';//"BAT";
	Header.MAXHITS=MAXHITS;
	Header.HITMODE = HITMODE;
	Header.Tag_Length=STRINGLENGTH;
	Header.Index_Count=ONEFMINDEX;
	Header.Print_Desc=PRINT_DESC;
	fwrite(&Header,sizeof(Header),1,Output_File);
	if(UNIQUEHITS)
	{
		Header.HITMODE = DEFAULT;
		fwrite(&Header,sizeof(Header),1,Unique_File);
	}
//---------------------------------------------------------------------
	char Quality_Count[256];
	char All_Zero[256];
	for (int i=1;i<256;i++)
	{
		All_Zero[i]=0;
	}

	for (int i=0;i<STRINGLENGTH;i++) Do_All[i]=TRUE;

	for(;;)//Tag Processing loop.....
	{

		if (fgets(Description,MAXDES,Input_File)!=0)//des .... read a tag
		{
			fgets(Current_Tag_Buffer,MAXDES,Input_File);//tag
			for (unsigned i=0;i<=STRINGLENGTH-1;i++)
			{
				Current_Tag[i]=Char_To_Code[Current_Tag[i]];
			}
		}
		else break;

//----------------------------------------------------------------------------------------------------------------------------------------
//{------------------------------------- QUALITY CHECK --------------------------------------------------------------------------------------------------
		
//}------------------------------------- QUALITY CHECK --------------------------------------------------------------------------------------------------
	
//----------------------------------------------------------------------------------------------------------------------------------------
		Actual_Tag++;
		Hits=0;
		Left_Mishits_Pointer=0;
		Right_Mishits_Pointer=0;
		Possible_20_Pointer=0;
		Possible_02_Pointer=0;
		Mismatches_Forward_Pointer=0;//first node where SA range was not found, all other nodes will not have matches..
		Mismatches_Backward_Pointer=0;
		One_Mismatch_Already=FALSE;
		Two_Mismatches_At_End_Pointer=0;
		Two_Mismatches_At_End_Forward_Pointer=0;
		Start=1-2;//Adjust for offsets...
		FMIndex=REVERSE;

		//c=Current_Tag[0];
		//Range.Start=Forward_Start_Lookup[c];Range.End=Forward_End_Lookup[c];Range.Mismatches=0;Range.Level=2;Range.Tag=Actual_Tag;
		if(LOOKUPSIZE ==3)
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		Search_Forwards_Exact(Range,Start,STRINGLENGTH,revfmi);//Find exact matches and report... if not found get the range for 0|?
		if(MAXHITS==Hits || MAX_MISMATCHES == 0) continue;
//{------------------------------------------- ONE MISMATCH ---------------------------------------------------------------------------------------------
		//One mismatches...
		Range=Exact_Match_Forward[Start+LH];
		if(Range.Start && Range.Tag == Actual_Tag)//if there are hits of the form 0|?
		{
			Range.Level=1;
			Do_Branch=Do_All;
			Search_Forwards(Range,1,LH+1,RH,revfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
			if(MAXHITS==Hits) continue;

		}		
		FMIndex=FORWARD;
		//c=Current_Tag[STRINGLENGTH-1];
		//Range.Start=Backward_Start_Lookup[c] + 1;Range.End=Backward_End_Lookup[c];Range.Mismatches=0;Range.Level=2;Range.Tag=Actual_Tag;
		if(LOOKUPSIZE ==3)
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		Search_Backwards_Exact( Range,STRINGLENGTH,RH,fwfmi);//Backward scan for ?|0
		//Range=Exact_Match_Backward[Start-HALFSTRINGLENGTH];//useful for later heuristics...
		if(Range.Start)//if there are possible hits of the form ?|0
		{
			Range.Level=1;
			Do_Branch=Do_All;
			Search_Backwards(Range,1,LH,LH,fwfmi);//Backward scan for one mismatches of the form 1|0, store possible mismatches of the form 2|0
			if(MAXHITS==Hits) continue;
		}
		if (MAX_MISMATCHES == 1 ) continue;
//}------------------------------------------- ONE MISMATCH ---------------------------------------------------------------------------------------------

//{------------------------------------------- TWO MISMATCH ---------------------------------------------------------------------------------------------
		FMIndex=REVERSE;
		if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
		{
			for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
			{
				Two_Mismatches_At_End_Forward[i].Mismatch_Pos=Two_Mismatches_At_End_Forward[i].Mismatch_Pos | ((STRINGLENGTH-1)<<6);//mismatches of the form 0|2, with last mismatch at the end...
				Print_LocationX(Two_Mismatches_At_End_Forward[i]);
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Forward_Pointer=0;

		FMIndex=FORWARD;
		if(Two_Mismatches_At_End_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End[i]);//Mismatches of the form 2|0, with one mismatch at the first position
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}
		Two_Mismatches_At_End_Pointer=0;
		FMIndex=REVERSE;
		int Possible_03_Pointer=Mismatches_Forward_Pointer;
		if(Mismatches_Forward_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_03_Pointer-1;i>=0;i--)
			{
				Search_Forwards(Mismatches_Forward[i],2,LH+1,RH,revfmi);//scan for possible two mismatches of the form 0|2, and store candidates for 0|3
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}

		FMIndex=FORWARD;
		int Possible_30_Pointer=Mismatches_Backward_Pointer;
		if(Mismatches_Backward_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_30_Pointer-1;i>=0;i--)
			{
				Search_Backwards(Mismatches_Backward[i],2,LH,LH,fwfmi);//scan for possible two mismatches of the form 2|0, and stores the candidates for 3|0
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}

//----------------------------------------------------------------------------------------------------------------------------------------
		//c=Current_Tag[STRINGLENGTH-1];
		//Range.Start=Backward_Start_Lookup[c] + 1;Range.End=Backward_End_Lookup[c];Range.Mismatches=0;Range.Level=2;Range.Tag=Actual_Tag;
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;//Range.Tag=Actual_Tag;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;

		Search_Backwards_Exact_X0( Range,STRINGLENGTH,RHQR,fwfmi);// ?|?|0
		//Mark_Branches(Current_Tag,0,HALFSTRINGLENGTH,revfmi);
		Range.Level=1;

		Do_Branch=Do_All;//High_Quality;//Do_All;
		Search_Backwards_X10(Range,1,LH + RHQL, RHQL,fwfmi);//?|1|0 and extend, finds mismatches of the form 1|1 and stres candidates for 2|1
		if(MAXHITS==Hits) continue;

//----------------------------------------------------------------------------------------------------------------------------------------
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[LH+0] | (Current_Tag[LH+1]<<2) | (Current_Tag[LH+2]<<4);// | (Current_Tag[LH+3]<<6) | Current_Tag[LH+4]<<8 | (Current_Tag[LH+5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[LH+0] | (Current_Tag[LH+1]<<2) | (Current_Tag[LH+2]<<4) | (Current_Tag[LH+3]<<6) | Current_Tag[LH+4]<<8 | (Current_Tag[LH+5]<<10);//Use lookup table...
		}
		Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		//c=Current_Tag[HALFSTRINGLENGTH];
		//Range.Start=Forward_Start_Lookup[c];Range.End=Forward_End_Lookup[c];Range.Mismatches=0;Range.Level=2;
		Search_Forwards_0X(Range,LH+1,RHQL,revfmi);
		Range.Level=1;

		Do_Branch=Do_All;//High_Quality;//Do_All;
		Search_X01(Range,1,LH + RHQL +1,RHQR,revfmi);//?|0|1 and extend, finds mismatches of the form 1|1 and stres candidates for 2|1
		if(MAXHITS==Hits) continue;
		if( MAX_MISMATCHES ==2) continue;
		
//}------------------------------------------- TWO MISMATCH ---------------------------------------------------------------------------------------------
//{------------------------------------------- THREE MISMATCH ---------------------------------------------------------------------------------------------
		//Find three mismatches....
		FMIndex=REVERSE;
		if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
		{
			for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End_Forward[i]);//mismatches of the form 0|3, with last mismatch at the end...
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Forward_Pointer=0;

		FMIndex=FORWARD;
		if(Two_Mismatches_At_End_Pointer)
		{
			for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End[i]);//Mismatches of the form 3|0, with one mismatch at the first position
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Pointer=0;

		FMIndex=REVERSE;
		int Possible_04_Pointer=Mismatches_Forward_Pointer;
		if(Mismatches_Forward_Pointer!=Possible_03_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_04_Pointer-1;i>=Possible_03_Pointer;i--)
			{
				Search_Forwards(Mismatches_Forward[i],3,LH+1,RH,revfmi);//scan for possible three mismatches of the form 0|3, and finds mismatches of the form 1|2, stores possibles in the form 1|3
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		//Mismatches_Forward_Pointer=0;

		FMIndex=FORWARD;
		int Possible_40_Pointer=Mismatches_Backward_Pointer;
		if(Mismatches_Backward_Pointer!=Possible_30_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_40_Pointer-1;i>=Possible_30_Pointer;i--)
			{
				Search_Backwards(Mismatches_Backward[i],3,LH,LH,fwfmi);//scan for possible mismatches of the form 3|0, 2|1 and sotres the candidates for 4|0, 3|1
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}
		//Mismatches_Backward_Pointer=0;
//----------------------------------------------------------------------------------------------------------------------------------------
		FMIndex=REVERSE;
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		//c=Current_Tag[HALFSTRINGLENGTH];
		//Range.Start=Forward_Start_Lookup[c];Range.End=Forward_End_Lookup[c];Range.Mismatches=0;Range.Level=2;
		Search_Forwards_0X(Range,1,LHQL,revfmi);
		Range.Level=1;
		Do_Branch=Do_All;//High_Quality;//Do_All;
		Search_01X(Range,1,LHQL +1,LHQR,revfmi);
		if(MAXHITS==Hits) continue;
//----------------------------------------------------------------------------------------------------------------------------------------
		//c=Current_Tag[STRINGLENGTH-1];
		//Range.Start=Backward_Start_Lookup[c] + 1;Range.End=Backward_End_Lookup[c];Range.Mismatches=0;Range.Level=2;Range.Tag=Actual_Tag;
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[LH-1-0] | (Current_Tag[LH-1-1]<<2) | (Current_Tag[LH-1-2]<<4);// | (Current_Tag[LH-1-3]<<6) | Current_Tag[LH-1-4]<<8 | (Current_Tag[LH-1-5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[LH-1-0] | (Current_Tag[LH-1-1]<<2) | (Current_Tag[LH-1-2]<<4) | (Current_Tag[LH-1-3]<<6) | Current_Tag[LH-1-4]<<8 | (Current_Tag[LH-1-5]<<10);//Use lookup table...
		}

		Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;//Range.Tag=Actual_Tag;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		Search_Backwards_Exact_X0( Range,LH,LHQR,fwfmi);// ?|0|?
		Range.Level=1;
		Do_Branch=Do_All;
		Search_10X(Range,1,LHQL, LHQL,fwfmi);//search for three mismatches of the form 1|2 and stores the candidates for 1|3
		if(MAXHITS==Hits) continue;//Do_Branch=Do_All;//High_Quality;//Do_All;
		if( MAX_MISMATCHES ==3) continue;
//}------------------------------------------- THREE MISMATCH ---------------------------------------------------------------------------------------------
//{------------------------------------------- FOUR MISMATCH ---------------------------------------------------------------------------------------------
		FMIndex=REVERSE;
		if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
		{
			for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End_Forward[i]);//mismatches of the form 0|4, with last mismatch at the end...
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Forward_Pointer=0;

		FMIndex=FORWARD;
		if(Two_Mismatches_At_End_Pointer)
		{
			for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End[i]);//mismatches of the form 0|4, with one mismatch at the start...
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Pointer=0;

		FMIndex=REVERSE;
		int Possible_05_Pointer=Mismatches_Forward_Pointer;
		int Wrap=FALSE;
		if(Mismatches_Forward_Pointer)
		{
			if (Possible_04_Pointer > 46000) {Mismatches_Forward_Pointer=0;Wrap=TRUE;}
			Do_Branch=Do_All;
			for(int i=Possible_04_Pointer;i<Possible_05_Pointer;i++)//Mismatches_Forward_Pointer;i++)
			{
				Search_Forwards(Mismatches_Forward[i],4,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|4, and finds mismatches of the form 1|3, stores possibles in the form 1|4
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		if(Wrap) Possible_05_Pointer=0;
		int Mismatches_Forward_Pointer_Last4=Mismatches_Forward_Pointer;

		FMIndex=FORWARD;
		int Possible_50_Pointer=Mismatches_Backward_Pointer;
		if(Mismatches_Backward_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_50_Pointer-1;i>=Possible_40_Pointer;i--)//Mismatches_Backward_Pointer-1;i>=0;i--)
			{
				Search_Backwards(Mismatches_Backward[i],4,LH,LH,fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}

		FMIndex=REVERSE;
		int Left_Mishits_Pointer_1=Left_Mishits_Pointer;
		if(Left_Mishits_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=0;i<Left_Mishits_Pointer;i++)
			{
				Search_Forwards(Left_Mishits[i],4,1,STRINGLENGTH,revfmi);//find mismatches of the form 022 form, stores possibles of the form 023
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}

		int Mismatches_Forward_Pointer_Last5=Mismatches_Forward_Pointer;
		if( Right_Mishits_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=0;i<Right_Mishits_Pointer;i++)
			{

				if(Right_Mishits[i].Level!=LHQL) 
				{
					Search_Backwards_Exact( Right_Mishits[i],LHQL,LHQL,fwfmi);//finds mismatches of the form 202, stores possibles of the form 203
				}
				if(Right_Mishits[i].Start)
				{	
					Backwards(Right_Mishits[i],1,LH);
					if(Right_Mishits[i].Start)
					{
						Right_Mishits[i].Level=1;
						Search_Forwards(Right_Mishits[i],4,LH+1,RH,revfmi);
						if(MAXHITS==Hits) break;
					}
				}
			}
			if(MAXHITS==Hits) continue;
		}
		Range.Start=1;Range.End=SOURCELENGTH;Range.Mismatches=0;Range.Level=1;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		Do_Branch=Do_All;
		Search_11X(Range,1,1,LHQL,revfmi);//finds mismatches of the form 112, stores possibles 113 and in the left half tag,
		if(MAXHITS==Hits) continue;

		if( MAX_MISMATCHES ==4) continue;
//}------------------------------------------- FOUR MISMATCH ---------------------------------------------------------------------------------------------
//{------------------------------------------- FIVE MISMATCH ---------------------------------------------------------------------------------------------

		FMIndex=REVERSE;
		if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
		{
			for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End_Forward[i]);//mismatches of the form 0|4, with last mismatch at the end...
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Forward_Pointer=0;


		FMIndex=FORWARD;
		if(Two_Mismatches_At_End_Pointer)
		{
			for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
			{
				Print_LocationX(Two_Mismatches_At_End[i]);//mismatches of the form 0|5, with one mismatch at the start...
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}
		Two_Mismatches_At_End_Pointer=0;

		FMIndex=REVERSE;
		if(Mismatches_Forward_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_05_Pointer;i<Mismatches_Forward_Pointer_Last4;i++)//Mismatches_Forward_Pointer;i++)
			{
				Search_Forwards(Mismatches_Forward[i],5,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|5
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}

		FMIndex=REVERSE;
		if(Mismatches_Forward_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Mismatches_Forward_Pointer_Last4;i<Mismatches_Forward_Pointer_Last5;i++)//Mismatches_Forward_Pointer;i++)
			{
				Search_Forwards(Mismatches_Forward[i],5,1,STRINGLENGTH,revfmi);//scan for possible five mismatches of the form 0|5, and finds mismatches of the form 1|4,2|3 
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}

		FMIndex=REVERSE;
		if(Mismatches_Forward_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Mismatches_Forward_Pointer_Last5;i<Mismatches_Forward_Pointer;i++)//Mismatches_Forward_Pointer;i++)
			{
				Search_Forwards(Mismatches_Forward[i],5,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|5
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;
		}

		FMIndex=REVERSE;
		if(Left_Mishits_Pointer!=Left_Mishits_Pointer_1)
		{
			Do_Branch=Do_All;
			for(int i=Left_Mishits_Pointer_1;i<Left_Mishits_Pointer;i++)
			{
				Search_Forwards(Left_Mishits[i],5,1,STRINGLENGTH,revfmi);//find mismatches of the form 122 form
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}

		FMIndex=FORWARD;
		if(Mismatches_Backward_Pointer!=Possible_50_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Mismatches_Backward_Pointer-1;i>=Possible_50_Pointer;i--)
			{
				Search_Backwards(Mismatches_Backward[i],5,LH,LH,fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}

		FMIndex=FORWARD;
		if (Possible_20_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=Possible_20_Pointer-1;i>=0;i--)
			{
				Search_Backwards(Possible_20[i],5,LH + RHQL,(LH + RHQL),fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
				if(MAXHITS==Hits) break;
			}
			if(MAXHITS==Hits) continue;

		}

		if(Possible_02_Pointer)
		{
			Do_Branch=Do_All;
			for(int i=0;i<Possible_02_Pointer;i++)
			{

				if(Possible_02[i].Level!=RHQR) 
				{
					Search_Exact( Possible_02[i],LH + RHQL-1 ,RHQR,revfmi);//finds mismatches of the form 202, stores possibles of the form 203
				}
				if(Possible_02[i].Start) 
				{
					Reverse(Possible_02[i],STRINGLENGTH,RH);
					if(Possible_02[i].Start)
					{
						Possible_02[i].Level=1;
						Search_Backwards(Possible_02[i],5,LH,LH,fwfmi);
					}
				}
				if(MAXHITS==Hits) break;
			}

			if(MAXHITS==Hits) continue;
		}

		Range.Start=1;Range.End=SOURCELENGTH;Range.Mismatches=0;Range.Level=1;
		Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
		Do_Branch=Do_All;
		Search_X11(Range,1,STRINGLENGTH,RHQL,fwfmi);//finds mismatches of the form 112, stores possibles 113 and in the left half tag,

//}------------------------------------------- FIVE MISMATCH ---------------------------------------------------------------------------------------------
		if(PRINT_MISHITS && !Hits)
		{
			for (unsigned i=0;i<=STRINGLENGTH-1;i++)
			{
				Current_Tag[i]=Code_To_Char[Current_Tag[i]];
			}
			fprintf(Mishit_File,"%s\n%s+\n%s\n", Description,Current_Tag,Quality);
		}
	}
	if(HITMODE == DEFAULT)
	{
		Record.Start=0;
		fwrite(&Record,sizeof(Record),1,Output_File);//write sentinel..
	}
	else
	{
		char End_Mark='&';
		fwrite(&End_Mark,1,1,Output_File);//write sentinel..
	}	
	printf("Total Hits : %d\t Tags parsed : %d\n",Total_Hits,Actual_Tag+1);
	End_Time = clock();printf("\n Time Taken  - %ld Seconds\n ",(End_Time-Start_Time)/CLOCKS_PER_SEC);



}


void Build_Preindex_Backward(Range Range, int Level, int Bit)
{

	if (LOOKUPSIZE==Level) 
	{
		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label
		Backward_Start_LookupX[Range.Label] = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.Start , Bit) + 1;
		Backward_End_LookupX[Range.Label] = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.End+1, Bit);
	}
	else
	{


		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label 
		Range.Start = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.Start , Bit) + 1;
		Range.End = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.End+1, Bit);
		Level ++;
		for ( int i=0;i<4;i++)
		{
			Build_Preindex_Backward( Range, Level,i);
		}

	}

}

void Build_Preindex_Forward(Range Range, int Level, int Bit)
{

	if (LOOKUPSIZE==Level) 
	{
		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label
		Forward_Start_LookupX[Range.Label] = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.Start , Bit) + 1;
		Forward_End_LookupX[Range.Label] = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.End+1, Bit);
	}
	else
	{


		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label 
		Range.Start = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.Start , Bit) + 1;
		Range.End = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.End+1, Bit);
		Level ++;
		for ( int i=0;i<4;i++)
		{
			Build_Preindex_Forward( Range, Level,i);
		}

	}

}
/*
//{-----------------------------  SSE ROUTINES  -------------------------------------------------/

void sse2_bit_count(unsigned Start,unsigned End,BWT *fmi)
{
	unsigned* Data;
	Data=fmi->bwtCode;
	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;
	Iu32vec4* Vector_List;
	Iu32vec4 Temp_Vector(0,0,0,0),First_Bits,Last_Bits;
	Iu32vec4 Isolate_First(0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA);
	Iu32vec4 Isolate_Last (0x55555555,0x55555555,0x55555555,0x55555555);

	#define ALLSET  0xFFFFFFFF
	#define mu1  0x55555555
	#define mu2  0x33333333
	#define mu3  0x0F0F0F0F
	#define mu4  0x3F
	unsigned int Aligned_Start, Aligned_End,Int_Start,Start_Offset,End_Offset,Start_Index,End_Index;
	unsigned int Start_Mask,End_Mask,End_Bucket,Count=0;

	unsigned Count_01_11=0,Count_10_11=0;		
	unsigned Last_Int,First_Int;

//Last_Int=_mm_popcnt_u32(First_Int);//ref page 156...
	Start_Index = (Start-1)/16;//start position of packed bit in integer array, with 16 packed chars per integer...
	Start_Offset = (Start-1)%16;//if(Start_Offset==0) Start_Offset=16;
	End_Index = (End-1)/16;//End position of packed bit in integer array
	End_Offset = (End)%16;//if (End_Offset==0) End_Offset=16; 
	End_Bucket = ((End-Start+Start_Offset)%64)/16;
	Start_Mask = ALLSET >>(2*Start_Offset);
	End_Mask = ~(ALLSET >>(2*End_Offset));if (!End_Mask) End_Mask=ALLSET;

	Vector_List=(Iu32vec4*)&Data[Start_Index+1];
	if(Start_Index != End_Index)//block spans at least two blocks...
	{	
		unsigned  Index=Start_Index+1;
		if(Index + 3 < End_Index)
		{	
			for (unsigned j=Index;Index<=End_Index-4;Index=Index+4,j=j+4)//loop the whole region
			{
				Last_Bits[0]=Data[j];Last_Bits[1]=Data[j+1];Last_Bits[2]=Data[j+2];Last_Bits[3]=Data[j+3];
				First_Bits=(Last_Bits & Isolate_First)>>1;//isolate the hi bit of character
				Count_10_11 += One_Count(First_Bits);//count of 11 + 10
				Last_Bits=Last_Bits & Isolate_Last;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
				Count_01_11 += One_Count(Last_Bits);//count of 11 + 01
				Branch_Characters[3]+=One_Count(Last_Bits & First_Bits);//double the number of 11's
			}
			//Index=Index-4;
		}
		Last_Int=Data[Start_Index]&Start_Mask;
		First_Int=(Last_Int & 0xAAAAAAAA)>>1;//isolate the hi bit of character
		Temp_Vector[0]=First_Int;Count_10_11 += One_Count(Temp_Vector);//count of 11 + 10
		Last_Int=Last_Int & 0x55555555;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
		Temp_Vector[0]=Last_Int;Count_01_11 += One_Count(Temp_Vector);//count of 11 + 01
		Temp_Vector[0]=Last_Int & First_Int; Branch_Characters[3] += One_Count(Temp_Vector);//double the number of 11's

		switch(End_Bucket)
		{
			case 1:
				Last_Int=Data[Index]&End_Mask;
				First_Int=(Last_Int & 0xAAAAAAAA)>>1;//isolate the hi bit of character
				Temp_Vector[0]=First_Int;Count_10_11 += One_Count(Temp_Vector);//count of 11 + 10
				Last_Int=Last_Int & 0x55555555;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
				Temp_Vector[0]=Last_Int;Count_01_11 += One_Count(Temp_Vector);//count of 11 + 01
				Temp_Vector[0]=Last_Int & First_Int; Branch_Characters[3] += One_Count(Temp_Vector);//double the number of 11's
				break;
			case 2:
				Last_Bits[0]=Data[Index];Last_Bits[1]=Data[Index+1]&End_Mask;Last_Bits[2]=0;Last_Bits[3]=0;
				First_Bits=(Last_Bits & Isolate_First)>>1;//isolate the hi bit of character
				Count_10_11 += One_Count(First_Bits);//count of 11 + 10
				Last_Bits=Last_Bits & Isolate_Last;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
				Count_01_11 += One_Count(Last_Bits);//count of 11 + 01
				Branch_Characters[3]+=One_Count(Last_Bits & First_Bits);//double the number of 11's
				break;
			case 3:
				Last_Bits[0]=Data[Index];Last_Bits[1]=Data[Index+1];Last_Bits[2]=Data[Index+2]&End_Mask;Last_Bits[3]=0;
				First_Bits=(Last_Bits & Isolate_First)>>1;//isolate the hi bit of character
				Count_10_11 += One_Count(First_Bits);//count of 11 + 10
				Last_Bits=Last_Bits & Isolate_Last;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
				Count_01_11 += One_Count(Last_Bits);//count of 11 + 01
				Branch_Characters[3]+=One_Count(Last_Bits & First_Bits);//double the number of 11's
				break;
			case 0:
				Last_Bits[0]=Data[Index];Last_Bits[1]=Data[Index+1];Last_Bits[2]=Data[Index+2];Last_Bits[3]=Data[Index+3]&End_Mask;
				First_Bits=(Last_Bits & Isolate_First)>>1;//isolate the hi bit of character
				Count_10_11 += One_Count(First_Bits);//count of 11 + 10
				Last_Bits=Last_Bits & Isolate_Last;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
				Count_01_11 += One_Count(Last_Bits);//count of 11 + 01
				Branch_Characters[3]+=One_Count(Last_Bits & First_Bits);//double the number of 11's
				break;
				
		}
		Branch_Characters[1]=Count_01_11-Branch_Characters[3];
		Branch_Characters[2]=Count_10_11-Branch_Characters[3];
		Branch_Characters[0]=End-Start+1-Branch_Characters[3]-Branch_Characters[1]-Branch_Characters[2];
	}
	else//bits are in the same integer...
	{
		Last_Int=(Data[Start_Index] & End_Mask)&Start_Mask;
		First_Int=(Last_Int & 0xAAAAAAAA)>>1;//isolate the hi bit of character
		Temp_Vector[0]=First_Int;Count_10_11 += One_Count(Temp_Vector);//count of 11 + 10
		Last_Int=Last_Int & 0x55555555;//Isolate last bit by shaking left and right... in the process we also get count of 11+01
		Temp_Vector[0]=Last_Int;Count_01_11 += One_Count(Temp_Vector);//count of 11 + 01
		Temp_Vector[0]=Last_Int & First_Int; Branch_Characters[3] += One_Count(Temp_Vector);//the number of 11's
		Branch_Characters[1]=Count_01_11-Branch_Characters[3];
		Branch_Characters[2]=Count_10_11-Branch_Characters[3];
		Branch_Characters[0]=End-Start+1-Branch_Characters[3]-Branch_Characters[1]-Branch_Characters[2];
	}
}
int One_Count(Iu32vec4 b)
{
	Iu32vec4 m4(mu4, mu4, mu4, mu4);
	Iu32vec4 m1(mu1, mu1, mu1, mu1);
	Iu32vec4 m2(mu2, mu2, mu2, mu2);
	Iu32vec4 m3(mu3, mu3, mu3, mu3);
	Iu32vec4 mcnt(0,0,0,0);

        Iu32vec4 tmp1, tmp2;
        // b = (b & 0x55555555) + (b >> 1 & 0x55555555);
        tmp1 = b>> 1;                    // tmp1 = (b >> 1 & 0x55555555)
        tmp1 = tmp1 &  m1; 
        tmp2 = b & m1;                    // tmp2 = (b & 0x55555555)
        b    = tmp1 + tmp2;               //  b = tmp1 + tmp2

        // b = (b & 0x33333333) + (b >> 2 & 0x33333333);
        tmp1 = b >> 2;                    // (b >> 2 & 0x33333333)
        tmp1 = tmp1 & m2; 
        tmp2 = b & m2;                    // (b & 0x33333333)
        b    = tmp1 + tmp2;               // b = tmp1 + tmp2

        // b = (b + (b >> 4)) & 0x0F0F0F0F;
        tmp1 = b >>4;                    // tmp1 = b >> 4
        b = b + tmp1;                     // b = b + (b >> 4)
        b = b & m3;                       //           & 0x0F0F0F0F

        // b = b + (b >> 8);
        tmp1 = b >> 8;                   // tmp1 = b >> 8
        b = b + tmp1;                     // b = b + (b >> 8)

        // b = (b + (b >> 16)) & 0x0000003F;
        tmp1 = b >> 16;                  // b >> 16
        b = b + tmp1;                     // b + (b >> 16)
        b = b & m4;                       // (b >> 16) & 0x0000003F;

        mcnt = mcnt + b;                  // mcnt += b
	return mcnt[0]+mcnt[1]+mcnt[2]+mcnt[3];
}
//}-----------------------------  SSE ROUTINES  -------------------------------------------------/
*/
//{-----------------------------  BACKWARD SEARCH ROUTINE half Tag  -------------------------------------------------/

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Backwards_Exact_XX0
 *  Description:  finds SA range of exact matches of the Current_Tag from Start to a length of string length
 *  		  Enters with Tag having sarange [1..SOURCELENGTH]
 *  		  returns with Tag having the exact match range....
 * =====================================================================================
 */
void Search_Backwards_Exact_X0(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	
	for(;;)
	{
		Get_SARange_Fast(Current_Tag[Start-Tag.Level],Tag,fmi);
		if (Tag.Start!=0)
		{
			if(Tag.Level== StringLength)
			{
				return;
			}
			else Tag.Level++;
		} 
		else
		{
			return;//No hit
		}
	}
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Backward_Search_Tag_Mismatch_Half_Tag
 *  Description:  Performs backward search allowing for at most Count mismatches...
 *  		  Searches by performing backward search in the FMI fmi for the string having prefix given by SARange of Tag, 
 *  		  starting from <Start-StringLength> to <Start> in Exact_Match[Sorted[i]] 
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Start = 1 indexed end position of the start of the backward scan
 *  		  String length = Natural string length.
 =====================================================================================
 */

void Search_10X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	
	struct SARange Range,Temp_Range=Tag;
	int BMHStack_Top=0;
	BMHStack[0]=Tag;
	//struct SARange Range,Temp_Range;
	while(BMHStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMHStack[BMHStack_Top];
		BMHStack_Top--;	//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_10X_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start , Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)//only one mismatch allowed here...
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)//a tag of the form ?|1|0
							{

								Backwards(Temp_Range,1,LH);
								if (Temp_Range.Start)
								{
									Temp_Range.Level=1;
									Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
									memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
									Search_Forwards(Temp_Range,3,LH+1,RH,revfmi);
									if(MAXHITS==Hits) return;
									Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
									memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
								}

							}
							else continue;
						}
						else
						{
							BMHStack_Top++;//Push range
							Temp_Range.Level++;
							BMHStack[BMHStack_Top]=Temp_Range;
						}
					}
					else //store mismatches for later use...
					{
						if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
						Right_Mishits[Right_Mishits_Pointer]=Temp_Range;
						Right_Mishits_Pointer++;
					}
					
				} 
			}
		}
	}
	return;
}

void Search_10X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{

	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= FWDInverseSA0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start-Tag.Level] && Now != Current_Tag[Start-Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start-Tag.Level] != Now)
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)//a tag of the form ?|1|0 , remove zero mismatch
				{
					Backwards(Tag,1,LH);
					if(Tag.Start)
					{
						Tag.End=Tag.Start;
						Tag.Level=1;
						Search_Forwards(Tag,3,LH+1,RH,revfmi);
					}
					return;

				}
				else return;
			}
			else { Tag.Level++;continue; }
		} 
//		else return;//to many mismatches...
		else //store mismatches for later use...
		{
			Tag.End=Tag.Start;
			if (Tag.Level!=StringLength) Tag.Level++; 
			//Tag.Level=Tag.Level+1;
			Right_Mishits[Right_Mishits_Pointer]=Tag;
			Right_Mishits_Pointer++;
			return;
		}
	}
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Backward_Search_Tag_Mismatch_Half_Tag
 *  Description:  Performs backward search allowing for at most Count mismatches...
 *  		  Searches by performing backward search in the FMI fmi for the string having prefix given by SARange of Tag, 
 *  		  starting from <Start-StringLength> to <Start> in Exact_Match[Sorted[i]] 
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Start = 1 indexed end position of the start of the backward scan
 *  		  String length = Natural string length.
 =====================================================================================
 */

void Search_Backwards_X10(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	int BMHStack_Top=0;
	BMHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMHStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMHStack[BMHStack_Top];
		BMHStack_Top--;	//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Backwards_X10_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start , Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)//only one mismatch allowed here...
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)//a tag of the form ?|1|0
							{
								Temp_Range.Level=1;
								Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
								memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
								Search_Backwards(Temp_Range,2,LH,LH,fwfmi);
								if(MAXHITS==Hits) return;
								Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
								memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
							}
							else continue;
						}
						else
						{
							BMHStack_Top++;//Push range
							Temp_Range.Level++;
							BMHStack[BMHStack_Top]=Temp_Range;
						}
					}
					else
					{
						if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
						Possible_20[Possible_20_Pointer]=Temp_Range;
						Possible_20_Pointer++;
					}
				} 
			}
		}
	}
	return;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Backwards_Mismatch_Half_Tag
 *  Description:  Does a search along an SA range with only one branch, and tries matching 
 *  		  upto count mismatches.
 * =====================================================================================
 */ 
// replace fmi->bwtcode, hard code count etc., remove printzero mismatch
void Search_Backwards_X10_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{

	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= FWDInverseSA0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start-Tag.Level] && Now != Current_Tag[Start-Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start-Tag.Level] != Now)//only one mismatch allowed here...
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)//a tag of the form ?|1|0 , remove zero mismatch
				{
					Tag.End=Tag.Start;
					Tag.Level=1;
					Search_Backwards(Tag,2,LH,LH,fwfmi);
					return;
				}
				else return;
			}
			else { Tag.Level++;continue; }
		} 
		//else return;//to many mismatches...
		else
		{
			Tag.End=Tag.Start;
			if (Tag.Level!=StringLength) Tag.Level++; 
			Possible_20[Possible_20_Pointer]=Tag;
			Possible_20_Pointer++;
			return;
		}
	}
}
//}-----------------------------  BACKWARD SEARCH ROUTINE half tag -------------------------------------------------/

//{-----------------------------  FORWARD SEARCH ROUTINE half Tag -------------------------------------------------/
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Scan_Left_Half_Tag 
 *  Description:  Find the exact matches of tags in Exact_Match.
 *  		  Searches by performing Forward search in the reverse FMI fmi for the string 
 *  		  starting from  <Start> to <Start+StringLength> in Exact_Match[Sorted[i]] 
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Tags in Exact_Match[] are initialised to the whole SARange.
 *  		  Start = 1 indexed end position of the start of the Forward scan
 *  		  String length = Natural string length.
 * =====================================================================================
 */

void Search_Forwards_0X(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{

	Start=Start-2;//Adjust for offsets...
	for(;;)	
	{
		Get_SARange_Fast(Current_Tag[Start+Tag.Level],Tag,fmi);
		if (Tag.Start!=0)
		{
			if(Tag.Level== StringLength)
			{
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			return;
		}
	}
}

void Search_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	int BMStack_Top=0;
	BMStack_X11[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMStack_X11[BMStack_Top];
		BMStack_Top--;	//Pop the range
		/*if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Backwards_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}*/
		//else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=1)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)
							{
								Temp_Range.Level++;
								Temp_BC2[0]=Branch_Characters[0];Temp_BC2[1]=Branch_Characters[1];Temp_BC2[2]=Branch_Characters[2];Temp_BC2[3]=Branch_Characters[3];
								memcpy(Temp_Branch_Ranges2,Branch_Ranges,4*sizeof(SARange));//X|8
								Search_Half_Tag_X11(Temp_Range,2,STRINGLENGTH,RH,fwfmi);
								if(MAXHITS==Hits) return;
								memcpy(Branch_Ranges,Temp_Branch_Ranges2,4*sizeof(SARange));
								Branch_Characters[0]=Temp_BC2[0];Branch_Characters[1]=Temp_BC2[1];Branch_Characters[2]=Temp_BC2[2];Branch_Characters[3]=Temp_BC2[3];

							}
							else continue;
						}
						else
						{
							BMStack_Top++;//Push range
							Temp_Range.Level++;
							BMStack_X11[BMStack_Top]=Temp_Range;
						}
					}
				} 
			}
		}
	}
	return;
}
void Search_Half_Tag_X11_OneSA(struct SARange & Tag,char* Current_Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if (!Do_Branch[Start-Tag.Level] && Current_Tag[Start-Tag.Level]!=Now) return;  
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;

		if (Current_Tag[Start-Tag.Level] != Now)
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength && Tag.Mismatches==2)
			{
				Tag.End=Tag.Start;
				Tag.Level=1;
				Search_Backwards(Tag,5,LH,LH,fwfmi);//LH,fwfmi);
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else return;
	}
}

void Search_Half_Tag_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	int BMStack_Top=0;
	BMStack_X11H[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMStack_X11H[BMStack_Top];
		BMStack_Top--;	//Pop the range
		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Half_Tag_X11_OneSA(Range,Current_Tag,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=2)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength && Temp_Range.Mismatches == Count)
						{
							Temp_Range.Level=1;
							Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
							memcpy(Temp_Branch_Ranges2,Branch_Ranges,4*sizeof(SARange));//X|16
							Search_Backwards(Temp_Range,5,LH,LH,fwfmi);//LH,fwfmi);
							if(MAXHITS==Hits) return;
							memcpy(Branch_Ranges,Temp_Branch_Ranges2,4*sizeof(SARange));
							Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
						}
						else
						{
							BMStack_Top++;//Push range
							Temp_Range.Level++;
							BMStack_X11H[BMStack_Top]=Temp_Range;
						}
					}
				} 
			}
		}
	}
	return;
}
void Search_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)////////////////
{
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStackX0X[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStackX0X[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		/*if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_01X_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}*/
		//else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=1)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							Temp_Range.Level=1;
							Temp_BC2[0]=Branch_Characters[0];Temp_BC2[1]=Branch_Characters[1];Temp_BC2[2]=Branch_Characters[2];Temp_BC2[3]=Branch_Characters[3];
							memcpy(Temp_Branch_Ranges2,Branch_Ranges,4*sizeof(SARange));
							Search_Half_Tag_11X(Temp_Range,2,LHQL +1,LHQR,revfmi);
							if(MAXHITS==Hits) return;
							memcpy(Branch_Ranges,Temp_Branch_Ranges2,4*sizeof(SARange));
							Branch_Characters[0]=Temp_BC2[0];Branch_Characters[1]=Temp_BC2[1];Branch_Characters[2]=Temp_BC2[2];Branch_Characters[3]=Temp_BC2[3];
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStackX0X[FSHStack_Top]=Temp_Range;
						}
					}
				} 
			}

		}
	}
	return;
}
void Search_Half_Tag_11X_OneSA(struct SARange & Tag,char* Current_Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start+Tag.Level] && Now != Current_Tag[Start+Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start+Tag.Level] != Now) 
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}
		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength && Tag.Mismatches==Count)
			{
				Tag.End=Tag.Start;
				Tag.Level=1;
				Search_Forwards(Tag,4,LH+1,RH,revfmi);
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			Tag.End=Tag.Start;
			Tag.Level=LHQR+Tag.Level+1;
			Left_Mishits[Left_Mishits_Pointer]=Tag;
			Left_Mishits_Pointer++;
			return;
		}
	}
}

void Search_Half_Tag_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStack[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Half_Tag_11X_OneSA(Range,Current_Tag,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength && Temp_Range.Mismatches==Count)
						{
							Temp_Range.Level=1;
							Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
							memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
							Search_Forwards(Temp_Range,4,LH+1,RH,revfmi);
							if(MAXHITS==Hits) return;
							memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
							Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStack[FSHStack_Top]=Temp_Range;
						}
					}
					else //store mismatches for later use...
					{
						Temp_Range.Level=LHQR+Temp_Range.Level+1;
						Left_Mishits[Left_Mishits_Pointer]=Temp_Range;
						Left_Mishits_Pointer++;
					}
				} 
			}

		}
	}
	return;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Forward_Search_Tag_Prune
 *  Description:  Performs forward search with branch pruning, allowing for at most Count mismatches on Tag...
 *  		  Searches by performing forward search in the reverse FMI fmi for the string 
 *  		  starting from <Start> to <Start+StringLength> in Tag.
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Tag is assumed to have SARanges with some prefix, from 1..Start.
 *  		  Start = 1 indexed end position of the start of the forward scan
 *  		  String length = Natural string length.
  * =====================================================================================
 */

void Search_01X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStack[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_01X_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)
							{
								Temp_Range.Level=1;
								Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
								memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
								Search_Forwards(Temp_Range,3,LH+1,RH,revfmi);
								memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
								if(MAXHITS==Hits) return;
								Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
							}
							else continue;
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStack[FSHStack_Top]=Temp_Range;
						}
					}
				} 
			}

		}
	}
	return;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Forwars_Exact_Prune
 *  Description:  Does a search along an SA range with only one branch, and tries matching 
 *  		  upto count mismatches.
 * =====================================================================================
 */
void Search_01X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start+Tag.Level] && Now != Current_Tag[Start+Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start+Tag.Level] != Now) 
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}
		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)
				{
					Tag.End=Tag.Start;
					Tag.Level=1;
					Search_Forwards(Tag,3,LH+1,RH,revfmi);
					return;
				}
				else return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			return;
		}
	}
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Forward_Search_Tag_Prune
 *  Description:  Performs forward search with branch pruning, allowing for at most Count mismatches on Tag...
 *  		  Searches by performing forward search in the reverse FMI fmi for the string 
 *  		  starting from <Start> to <Start+StringLength> in Tag.
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Tag is assumed to have SARanges with some prefix, from 1..Start.
 *  		  Start = 1 indexed end position of the start of the forward scan
 *  		  String length = Natural string length.
  * =====================================================================================
 */

void Search_X01(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStack[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_X01_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)
							{
								Reverse(Temp_Range,STRINGLENGTH,RH);
								if(Temp_Range.Start)
								{
									Temp_Range.Level=1;
									Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
									Search_Backwards(Temp_Range,2,LH,LH,fwfmi);
									if(MAXHITS==Hits) return;
									Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
								}
							}
							else continue;
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStack[FSHStack_Top]=Temp_Range;
						}
					}
					else
					{
						if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
						Possible_02[Possible_02_Pointer]=Temp_Range;
						Possible_02_Pointer++;
					}
				} 
			}

		}
	}
	return;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Forwars_Exact_Prune
 *  Description:  Does a search along an SA range with only one branch, and tries matching 
 *  		  upto count mismatches.
 * =====================================================================================
 */
void Search_X01_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start+Tag.Level] && Now != Current_Tag[Start+Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start+Tag.Level] != Now) 
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}
		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)
				{
					Tag.End=Tag.Start;
					Reverse(Tag,STRINGLENGTH,RH);
					if(Tag.Start)
					{
						Tag.Level=1;
						Search_Backwards(Tag,2,LH,LH,fwfmi);
					}
					return;
				}
				else return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			Tag.End=Tag.Start;
			if (Tag.Level!=StringLength) Tag.Level++; 
			Possible_02[Possible_02_Pointer]=Tag;
			Possible_02_Pointer++;
			return;
		}
	}
}

void Backwards(struct SARange & Tag,int Start,int StringLength)
{	

	char Temp=0;
	char Mismatch_Count=Tag.Mismatches;
	int Temp_Pos=0;//New_Char;
	int pos;
	Start=Start-2;
	
	for( int i=0;i<Mismatch_Count;i++)
	//if (Tag.Mismatches)//Save character if one mismatch
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Temp=Temp | (Current_Tag[pos]<<i*2);
		Current_Tag[pos]=Tag.Mismatch_Char>>(2*i) & 3;
	}

	Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
	memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
	{
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		Tag.Start=Forward_Start_LookupX[c];Tag.End=Forward_End_LookupX[c];
		Tag.Level=LOOKUPSIZE + 1;
		Search_Exact(Tag,Start,StringLength,revfmi);
	}
	Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
	memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
	for( int i=0;i<Tag.Mismatches;i++)
	//if (Tag.Mismatches)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Current_Tag[pos]=(Temp>>(2*i)) & 3;
	}
	return;
}

void Reverse(struct SARange & Tag,int Start,int StringLength)
{	
	char Temp=0;
	char New_Char;
	char Mismatch_Count=Tag.Mismatches;
	unsigned pos;
	for( int i=0;i<Mismatch_Count;i++)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Temp=Temp | (Current_Tag[pos]<<i*2);
		Current_Tag[pos]=Tag.Mismatch_Char>>(2*i) & 3;
	}
	Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
	{
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		Tag.Start=Backward_Start_LookupX[c];Tag.End=Backward_End_LookupX[c];
		Tag.Level=LOOKUPSIZE + 1;
		Search_Backwards_Exact( Tag,STRINGLENGTH,RH,fwfmi);//Backward scan for ?|0
	}
	Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
	for( int i=0;i<Tag.Mismatches;i++)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Current_Tag[pos]=(Temp>>(2*i)) & 3;
	}
	return;
}
//}-----------------------------  FORWARD SEARCH ROUTINE half Tag -------------------------------------------------/

//{-------------------------- BIT ROUTINES ---------------------------------------------
/***************************************************************************************/
int Read_Bits_FMI(long unsigned Bit_Array[],long Position)
/***************************************************************************************/
{

	long Remainder=Position%(CHAR_BIT*PACKEDBITSIZE);
	long Nearest_Index=Position/(CHAR_BIT*PACKEDBITSIZE);
	unsigned long isolate =(Bit_Array[Nearest_Index]>>CHAR_BIT-PACKEDBITSIZE-Remainder);
	return isolate & BITMASK;
}

/***************************************************************************************/
int Read_Bits(char Bit_Array[],long Position)// read one bit
/***************************************************************************************/
{

	Position --;
	unsigned int Remainder=Position%INTEGERSIZE +1;
	unsigned int Nearest_Index=Position/INTEGERSIZE;
	unsigned int isolate =(Bit_Array[Nearest_Index]>>INTEGERSIZE-Remainder);
	return isolate & BITMASK;
}


/***************************************************************************************/
void Set_Bit(char Bit_Array[],unsigned long Position)
/***************************************************************************************/
{

	Position--;
	unsigned int Remainder=Position%INTEGERSIZE;
	unsigned int Nearest_Index=Position/INTEGERSIZE;
	Bit_Array[Nearest_Index]=Bit_Array[Nearest_Index] | (1 <<INTEGERSIZE-Remainder-1);
}

/***************************************************************************************/
void Clear_Bit(char Bit_Array[],long Position)
/***************************************************************************************/
{

	int Remainder=Position%INTEGERSIZE;
	int Nearest_Index=Position/INTEGERSIZE;
	Bit_Array[Nearest_Index]=Bit_Array[Nearest_Index] & ~(1 <<INTEGERSIZE-Remainder-1);
}
//}--------------------------------BIT ROUTINES ----------------------------------------

//{-----------------------------  FORWARD SEARCH ROUTINE  -------------------------------------------------/
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Forwards_Exact
 *  Description:  Search for exact match from Current_Tag[Start..Start+StringLength], indices in
 *  		  1-index format, using forward search. Stores the exact match range for 
 *  		  Current_Tag[Start.. n] in Current_Tag[Start+n]
 * =====================================================================================
 */
void Search_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	int Level;
	unsigned long Index,Now,First,Last;
	for(;;)	
	{
		if(Tag.End==Tag.Start)//Only one branch?
		{
			Level=Tag.Level;
			for(;;)
			{
				Index=Tag.Start;
				if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
				Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
				if (Current_Tag[Start+Level] == Now)
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End=Tag.Start;
					if(Level== StringLength)
					{
						return;	
					}
					else {Level++;continue;}
				} 
				else//mismatch...
				{
					Tag.Start=0;//Tag.End=0;
					return;	
				}
			}
		}
		else//SA range has sevaral possible hits... 
		{
			if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
			{
				Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

				if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
				for (unsigned long Pos=First;Pos<=Last;Pos++)
				{
					Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
					Branch_Characters[Now]++;	
				}

				Now=Current_Tag[Tag.Level+Start];
				if (Branch_Characters[Now])//we have a match... 
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End = Tag.Start + Branch_Characters[Now]-1;// Calculate SAranges
				}
				else//mismatch..
				{
					Tag.Start=0;
				}
			} 
			else
			{
				Get_SARange_Fast(Current_Tag[Start+Tag.Level],Tag,fmi);
			}

			if (Tag.Start!=0)
			{
				if(Tag.Level== StringLength)
				{
					return;
				}
				else {Tag.Level++;continue;}
			} 
			else//Mismatch
			{
				return;
			}

		}
	}
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Forwards_Exact
 *  Description:  Search for exact match from Current_Tag[Start..Start+StringLength], indices in
 *  		  1-index format, using forward search. Stores the exact match range for 
 *  		  Current_Tag[Start.. n] in Current_Tag[Start+n]
 * =====================================================================================
 */
void Search_Forwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	int Level;
	Exact_Match_Forward[Start+LH].Start=0;
	unsigned long Index,Now,First,Last;
	for(;;)	
	{
		if(Tag.End==Tag.Start)//Only one branch?
		{
			Level=Tag.Level;
			for(;;)
			{
				Index=Tag.Start;
				if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
				Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
				if (Current_Tag[Start+Level] == Now)
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End=Tag.Start;
					Exact_Match_Forward[Start+Level]=Tag;
					if(Level== StringLength)
					{
						Print_LocationX(Tag);
						return;	
					}
					else {Level++;continue;}
				} 
				else//mismatch...
				{
					Tag.Start=0;//Tag.End=0;
					Exact_Match_Forward[Start+Level]=Tag;
					return;	
				}
			}
		}
		else//SA range has sevaral possible hits... 
		{
			if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
			{
				Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

				if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
				for (unsigned long Pos=First;Pos<=Last;Pos++)
				{
					Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
					Branch_Characters[Now]++;	
				}

				Now=Current_Tag[Tag.Level+Start];
				if (Branch_Characters[Now])//we have a match... 
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End = Tag.Start + Branch_Characters[Now]-1;// Calculate SAranges
				}
				else//mismatch..
				{
					Tag.Start=0;
				}
			} 
			else
			{
				Get_SARange_Fast(Current_Tag[Start+Tag.Level],Tag,fmi);
			}

			Exact_Match_Forward[Tag.Level+Start]=Tag;
			if (Tag.Start!=0)
			{
				if(Tag.Level== StringLength)
				{
					Print_LocationX(Tag);
					return;
				}
				else {Tag.Level++;continue;}
			} 
			else//Mismatch
			{
				Exact_Match_Forward[Start+Tag.Level]=Tag;
				return;
			}

		}
	}
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Forward_Search_Tag_Prune
 *  Description:  Performs forward search with branch pruning, allowing for at most Count mismatches on Tag...
 *  		  Searches by performing forward search in the reverse FMI fmi for the string 
 *  		  starting from <Start> to <Start+StringLength> in Tag.
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Tag is assumed to have SARanges with some prefix, from 1..Start.
 *  		  Start = 1 indexed end position of the start of the forward scan
 *  		  String length = Natural string length.
  * =====================================================================================
 */

void Search_Forwards(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	Start=Start-2;//Adjust for offset difference
	int FSStack_Top=0;
	FSSStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSSStack[FSStack_Top];
		FSStack_Top--;		//Pop the range
		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Forwards_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);//One_Branch(Range,revfmi);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{

						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;

					}


					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if (Temp_Range.Mismatches) //dont print exact matches
							{
								Print_LocationX(Temp_Range);
								if (MAXHITS==Hits) return;
							}
							else continue;
						}
						else 
						{

							FSStack_Top++;//Push range
							Temp_Range.Level++;
							FSSStack[FSStack_Top]=Temp_Range;
						}
					}
					else
					{
						if(MAX_MISMATCHES !=Count)//store only for one mismatch... and last node will not branch
						{
							if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
							else //2 mismatches with the last at the end...
							{
								Two_Mismatches_At_End_Forward[Two_Mismatches_At_End_Forward_Pointer]=Temp_Range;
								Two_Mismatches_At_End_Forward_Pointer++;
								continue;
							}
							Mismatches_Forward[Mismatches_Forward_Pointer]=Temp_Range;
							Mismatches_Forward_Pointer++;
						}
						continue;
					}
				} 
			}
		}
	}
	return;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Forwars_Exact_Prune
 *  Description:  Does a search along an SA range with only one branch, and tries matching 
 *  		  upto count mismatches.
 * =====================================================================================
 */
void Search_Forwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);
		if (!Do_Branch[Tag.Level+Start] && Current_Tag[Tag.Level+Start]!=Now) return;  
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Tag.Level+Start]!=Now)
		{

			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
			
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)//avoid printing exact matches...
				{
					Tag.End=Tag.Start;
					Print_LocationX(Tag);
				}
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else//log 2 mismatches 
		{
			if(MAX_MISMATCHES !=Count)//store only for one mismatch... later report these on a seperate stack..
			{
				Tag.End=Tag.Start;//possibly two mismatch exists..
				if (Tag.Level != StringLength) Tag.Level++; 
				else //2 mismatches occuring in last position...
				{
					Two_Mismatches_At_End_Forward[Two_Mismatches_At_End_Forward_Pointer]=Tag;
					Two_Mismatches_At_End_Forward_Pointer++;
					return;
				}

				Mismatches_Forward[Mismatches_Forward_Pointer]=Tag;
				Mismatches_Forward_Pointer++;
			}
			return;
		}
	}
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  One_Branch
 *  Description:  Finds the number of occurances of each character in the range of Tag.
 *  		  Tag= tag given to search for the characters.
 *  		  fmi= FM index where tag ranges are given.
 * =====================================================================================
 */
void One_Branch(struct SARange Tag,BWT *fmi)
{
	unsigned Last, First;
	//Tag.Start++;Tag.End++;
	/*if(Tag.End-Tag.Start>30)
	{	
		if(Tag.Start>=fmi->inverseSa0)
		{
			sse2_bit_count(Tag.Start,Tag.End,fmi);
		}
		else
		{

			sse2_bit_count(Tag.Start+1,Tag.End+1,fmi);
		}
		return;
	}*/
	char Now;
	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

	if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
	for (unsigned long Pos=First;Pos<=Last;Pos++)
	{
		Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
		Branch_Characters[Now]++;	
	}

}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Branch_Detect
 *  Description:  Finds the number of occurances of each character in the range of Tag.
 *  		  Detects whether we should branch at this char..
 *  		  Tag= tag given to search for the characters.
 *  		  fmi= FM index where tag ranges are given.
 * =====================================================================================
 */
void Branch_Detect (const struct SARange Tag,BWT *fmi,int Start)
{

	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;
	if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
	{
		unsigned Last, First;
		//Tag.Start++;Tag.End++;
		/*if(Tag.End-Tag.Start>30)
		  {	
		  if(Tag.Start>=fmi->inverseSa0)
		  {
		  sse2_bit_count(Tag.Start,Tag.End,fmi);
		  }
		  else
		  {

		  sse2_bit_count(Tag.Start+1,Tag.End+1,fmi);
		  }
		  return;
		  }*/
		char Now;

		if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 

		for (unsigned long Pos=First;Pos<=Last;Pos++)
		{
			Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
			Branch_Characters[Now]++;	
		}

		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Tag.Level+Start] && Branch != Current_Tag[Start+Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else if (Branch_Characters[Branch])
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = Branch_Ranges[Branch].Start + Branch_Characters[Branch]-1;// Calculate SAranges
			}
		}
	}
	else
	{
		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Tag.Level+Start] && Branch != Current_Tag[Start+Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.End+1, Branch);
				if(!(Branch_Ranges[Branch].End<Branch_Ranges[Branch].Start)) Branch_Characters[Branch]=1;
			}
		}

	}
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Branch_Detect_Backwards
 *  Description:  Finds the number of occurances of each character in the range of Tag.
 *  		  Detects whether we should branch at this char..
 *  		  Tag= tag given to search for the characters.
 *  		  fmi= FM index where tag ranges are given.
 * =====================================================================================
 */
void Branch_Detect_Backwards (const struct SARange Tag,BWT *fmi,int Start)
{

	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;
	if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
	{
		unsigned Last, First;
		//Tag.Start++;Tag.End++;
		/*if(Tag.End-Tag.Start>30)
		  {	
		  if(Tag.Start>=fmi->inverseSa0)
		  {
		  sse2_bit_count(Tag.Start,Tag.End,fmi);
		  }
		  else
		  {

		  sse2_bit_count(Tag.Start+1,Tag.End+1,fmi);
		  }
		  return;
		  }*/
		char Now;

		if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 

		for (unsigned long Pos=First;Pos<=Last;Pos++)
		{
			Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
			Branch_Characters[Now]++;	
		}

		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Start-Tag.Level] && Branch != Current_Tag[Start-Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else if (Branch_Characters[Branch])
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = Branch_Ranges[Branch].Start + Branch_Characters[Branch]-1;// Calculate SAranges
			}
		}
	}
	else
	{
		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Start-Tag.Level] && Branch != Current_Tag[Start-Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.End+1, Branch);
				if(!(Branch_Ranges[Branch].End<Branch_Ranges[Branch].Start)) Branch_Characters[Branch]=1;
			}
		}

	}
}
//}-----------------------------  FORWARD SEARCH ROUTINE  -------------------------------------------------/

//{-----------------------------  BACKWARD SEARCH ROUTINE  -------------------------------------------------/

void Search_Backwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	int Level;
	unsigned long Index,Now,First,Last;
	for(;;)	
	{
		if(Tag.End==Tag.Start)//Only one branch?
		{
			Level=Tag.Level;
			for(;;)
			{
				Index=Tag.Start;
				if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
				Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
				if (Current_Tag[Start-Level] == Now)
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End=Tag.Start;
					Exact_Match_Backward[Level]=Tag;
					if(Level== StringLength)//no need to print as we are still halfway..
					{
						return;	
					}
					else {Level++;continue;}
				} 
				else
				{
					Tag.Start=0;//Tag.End=0;
					return;	
				}
			}
		}
		else 
		{
			Exact_Match_Backward[Tag.Level]=Tag;
			if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
			{
				Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

				if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
				for (unsigned long Pos=First;Pos<=Last;Pos++)
				{
					Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
					Branch_Characters[Now]++;	
				}

				Now=Current_Tag[Start-Tag.Level];
				if (Branch_Characters[Now])//we have a match... 
				{

					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End = Tag.Start + Branch_Characters[Now]-1;// Calculate SAranges
				
				}
				else
				{
					Tag.Start=0;//Tag.End=0;
				}
			}
			else Get_SARange_Fast(Current_Tag[Start-Tag.Level],Tag,fmi);

			//Exact_Match_Backward[Start-Tag.Level]=Tag;//remove later..
			if (Tag.Start!=0)
			{
				if(Tag.Level== StringLength)
				{
					return;
				}
				else {Tag.Level++;continue;}
			} 
			else
			{
				return;
			}

		}
	}
}

//}-----------------------------  BACKWARD SEARCH ROUTINE  -------------------------------------------------/


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Backward_Search_Tag_Prune
 *  Description:  Performs backward search allowing for at most Count mismatches...
 *  		  Searches by performing backward search in the FMI fmi for the string having prefix given by SARange of Tag, 
 *  		  starting from <Start-StringLength> to <Start> in Exact_Match[Sorted[i]] 
 *  		     (i.e. a StringLength away from the starting position.)
 *  		  Start = 1 indexed end position of the start of the backward scan
 *  		  String length = Natural string length.
 *  		  pushes 1 mismatches to a stack...
 =====================================================================================
 */

void Search_Backwards(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	int BMStack_Top=0;
	BMStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMStack[BMStack_Top];
		BMStack_Top--;	//Pop the range
		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Backwards_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)
							{
								Print_LocationX(Temp_Range);
								if(MAXHITS==Hits) return;
							}
							else continue;
						}
						else
						{
							BMStack_Top++;//Push range
							Temp_Range.Level++;
							BMStack[BMStack_Top]=Temp_Range;
						}
					}
					else 
					{
						if(MAX_MISMATCHES !=Count)// 2 mismatches...
						{
							if(Temp_Range.Level != StringLength) Temp_Range.Level++;
							else // 2 mismatches with the last at the end?
							{
								Two_Mismatches_At_End[Two_Mismatches_At_End_Pointer]=Temp_Range;
								Two_Mismatches_At_End_Pointer++;
								continue;
							}
							Mismatches_Backward[Mismatches_Backward_Pointer]=Temp_Range;
							Mismatches_Backward_Pointer++;
						}
						continue;
					}
				} 
			}
		}
	}
	return;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Search_Backwards_Exact
 *  Description:  Does a search along an SA range with only one branch, and tries matching 
 *  		  upto count mismatches.
 * =====================================================================================
 */
void Search_Backwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if (!Do_Branch[Start-Tag.Level] && Current_Tag[Start-Tag.Level]!=Now) return;  
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;

		if (Current_Tag[Start-Tag.Level] != Now)
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)
				{
					Tag.End=Tag.Start;
					Print_LocationX(Tag);
				}
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else 
		{
			if(MAX_MISMATCHES >= Tag.Mismatches && MAX_MISMATCHES != Count)// 2 mismatches
			{
				Tag.End=Tag.Start;//possibly two mismatch exists..
				if (Tag.Level != StringLength) Tag.Level++; 
				else//two mismatches with the last at the end ... 
				{
					Two_Mismatches_At_End[Two_Mismatches_At_End_Pointer]=Tag;
					Two_Mismatches_At_End_Pointer++;
					return;
				}
				Mismatches_Backward[Mismatches_Backward_Pointer]=Tag;
				Mismatches_Backward_Pointer++;
			}
			return;
		} 
	}
}


//{----------------------------------- FILE HANDLING ---------------------------------------------------------

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  File_Open
 *  Description:  Open a file:
 *  Mode - "w" create/Write text from top    - "wb" Write/Binary  -"w+" open text read/write            -"w+b" same as prev/Binary
 *         "r" Read text from top            - "rb" Read/Binary   -"r+" open text read/write/nocreate   -"r+b" same as prev/binary
 *       - "a" text append/write                                  -"a+" text open file read/append      -"a+b" open binary read/append
 *
 * =====================================================================================
 */
FILE* File_Open(const char* File_Name,const char* Mode)
{
	FILE* Handle;
	Handle=fopen(File_Name,Mode);
	if (Handle==NULL)
	{
		printf("File %s Cannot be opened ....",File_Name);
		exit(1);
	}
	else return Handle;
}

//}----------------------------------- FILE HANDLING ---------------------------------------------------------

//{----------------------------------- FM INDEX ROUTINES ---------------------------------------------------------
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  initFMI
 *  Description:  Opens FM index fmiFile
 * =====================================================================================
 */
BWT* initFMI(const char* BWTCodeFileName,const char* BWTOccValueFileName ) 

{
	BWT *fmi;
        int PoolSize = 524288;
	MMMasterInitialize(3, 0, FALSE, NULL);
	mmPool = MMPoolCreate(PoolSize);

	fmi = BWTLoad(mmPool, BWTCodeFileName, BWTOccValueFileName, NULL, NULL, NULL, NULL);//Load FM index
	return fmi;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Get_SARange
 *  Description:  gets the SA range of strings having prefix [New_Char][Range]
 * =====================================================================================
 */
SARange Get_SARange( long New_Char,struct SARange Range,BWT *fmi)
{

	Range.Start = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.Start, New_Char) + 1;
	Range.End = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.End+1, New_Char);
	if (Range.End<Range.Start) 
	{
		Range.Start=0;
	}
	return Range;

}

void Get_SARange_Fast( long New_Char,struct SARange & Range,BWT *fmi)
{

	Range.Start = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.Start, New_Char) + 1;
	Range.End = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.End+1, New_Char);
	if (Range.End<Range.Start) 
	{
		//Range.End=0;
		Range.Start=0;
	}
}

void Get_SARange_Fast_2( long New_Char,struct SARange & Start_Range, struct SARange & Dest_Range,BWT *fmi)
{

	Dest_Range.Start = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Start_Range.Start, New_Char) + 1;
	Dest_Range.End = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Start_Range.End+1, New_Char);
	if (Dest_Range.End<Dest_Range.Start) 
	{
		//Range.End=0;
		Dest_Range.Start=0;
	}
}
//}----------------------------------- FM INDEX ROUTINES ---------------------------------------------------------

//{-----------------------------------DEBUG ROUTINE---------------------------------------------------------
void Convert_To_Reverse(SARange &Tag)
{
	char New_Char,Temp_One,Temp_Two;
	unsigned pos;

	if(Tag.Mismatches)
	{
		for( int i=0;i<Tag.Mismatches;i++)
		{
			pos=63 & (Tag.Mismatch_Pos>>(6*i));
			Temp_Char_Array[i]=Current_Tag[pos];
			Current_Tag[pos]=Tag.Mismatch_Char>>(2*i) & 3;
		}

	}
	if(LOOKUPSIZE == 3)
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}
	else
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}
	Tag.Start=Forward_Start_LookupX[c];Tag.Level=LOOKUPSIZE+1;
	while (Tag.Level <= STRINGLENGTH)
	{
		New_Char=Current_Tag[1-2+Tag.Level];
		Tag.Start = revfmi->cumulativeFreq[New_Char] + BWTOccValue(revfmi, Tag.Start, New_Char) + 1;
		Tag.Level++;
	}

	if(Tag.Mismatches)
	{
		for( int i=0;i<Tag.Mismatches;i++)
		{
			Current_Tag[63 & (Tag.Mismatch_Pos>>(6*i))]=Temp_Char_Array[i];
		}
	} 
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Print_Location
 *  Description:  Prints the hex location of start and end of Range
 * =====================================================================================
 */
void Print_LocationX (struct SARange & Tag)
{
	unsigned Gap;
	char New_Record;
	
	switch(HITMODE)
	{
		case(DEFAULT):// lowest mismatch output of "best hit"
			{
				Total_Hits++;
				Gap=Tag.End-Tag.Start;
				Record.Tag=Tag.Tag;
				if(ONEFMINDEX && FORWARD == FMIndex) 
				{
					Convert_To_Reverse(Tag);
					Record.Index=REVERSE;
				} 
				else 
				{
					Record.Index=FMIndex;
				}
				Record.Start=Tag.Start;
				//Record.Index=FMIndex;
				Record.Mismatches=Tag.Mismatches;
				Record.Gap=Gap;Gap++;
				fwrite(&Record,sizeof(Record),1,Output_File);
				if (COUNT_ALLHITS)
				{
					if(Hits + Gap > MAXHITS ) Hits = MAXHITS; else Hits=Hits+Gap;//COUNT_ALLHITS =1 => count all the hits, COUNT_ALLHITS =0 => count saranges... i.e. unique hits...
				}
				else
				{
					Hits++;
				}
				return;
			}
		case(DEEP)://output first MAXHITS
			{
				Gap=Tag.End-Tag.Start;
				if ( ONEFMINDEX && FORWARD==FMIndex )// forward search index...
				{
					Convert_To_Reverse(Tag);
				}
				if(UNIQUEHITS && !Gap)//a unique hit...
				{
					Record.Start=Tag.Start;
					Record.Tag=Tag.Tag;
					if(ONEFMINDEX) Record.Index=REVERSE; else Record.Index=FMIndex;
					Record.Mismatches=Tag.Mismatches;
					fwrite(&Record,sizeof(Record),1,Unique_File);
					if (! ALLHITS) {Hits++;Total_Hits++;return;}//write only the unique hits...
				}
				if(ALLHITS)//do we need to report all hits?
				{
					if(!Hits) //first hit...
					{
						New_Record='@';
						fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global
						
						if(PRINT_DESC) fprintf(Output_File,"%s",Description);
						for( int i=0;i<STRINGLENGTH; i++) Translated_String[i]=Code_To_Char[Current_Tag[i]];
						fwrite(Translated_String,1,STRINGLENGTH+1,Output_File);//write tag...
						if(!COUNT_ALLHITS) Hits++;//count distinct saranges
					}
					//else
					{
						New_Record='%';
						fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global
					}
					Mismatches.Mismatch_Pos=Tag.Mismatch_Pos;
					Mismatches.Mismatch_Char=Tag.Mismatch_Char;

					Mismatches.Gap=Gap;Gap++;

					fwrite(&Mismatches,sizeof(Mismatches),1,Output_File);
					if (COUNT_ALLHITS)
					{
						if(Hits + Gap > MAXHITS ) Hits = MAXHITS; else Hits=Hits+Gap;//COUNT_ALLHITS =1 => count all the hits, COUNT_ALLHITS =0 => count saranges... i.e. unique hits...
					}
					Record.Start=Tag.Start;
					Record.Tag=Tag.Tag;
					if (ONEFMINDEX) Record.Index=REVERSE; else Record.Index=FMIndex;
					Record.Mismatches=Tag.Mismatches;
					fwrite(&Record,sizeof(Record),1,Output_File);
					Total_Hits++;
				}
			}
	}
}
//}-----------------------------------DEBUG ROUTINE---------------------------------------------------------

//{-----------------------------  Parse Command Line  -------------------------------------------------
void Parse_Command_line(int argc, char* argv[])
{
	int Current_Option=0;
	char* Short_Options ="hq:o:b:m:f:g:u:st:k:xn:";//allowed options....
	char* This_Program = argv[0];//Current program name....
	char* Help_String=
"Parameters:\n"
" --help | -h\t\t\t\t Print help\n"
" --query | -q <filename>\t\t Query file(File of Tags)\n"
" --output | -o <filename>\t\t Name of output file\n"
" --uniquefile | -u <filename>\t\t Name of output file for unique hits\n"
" --genome | -g <filename>\t\t Name of the reference genome\n"
" --buffersize | -b <integer> \t\t Size of disk buffers\n"
" --maxhits | -m <integer> \t\t Maximum number of hits to output...\n"
" --singleindex | -s <integer> \t\t Convert output to be read by reverse index only...\n"
" --thresholdquality | -t <char> \t Threshold of weakest links to try first ...\n"
" --keepthreshold | -k <char> \t\t Fix nucleotide that has quality abouve this ..\n"
" --mishits | -x \t\t Print unprocessed hits to the fasta file mishits.fq..\n"
" --maxmismatches | -n <number> \t\t maximum mismatches allowed..\n"
//" --length | -l <number> \t\t Length of the tags \n"
" --format | -f <level>  \t\t Format of the output where Level is\n"
"                          \t\t 0 - outputs without detailed information\n"
"                          \t\t 1 - outputs only unique hits\n"//the uniqu hits are written to uniquhits.txt in filtering modes..
"                          \t\t 2 - Write unique hits to a seperate file in addition to the detailed output file\n"
"                          \t\t 3 - Detailed output file\n"
"                          \t\t 4 - Minimum format needed to run verification\n"
;

	Source=(char*)malloc(sizeof(char)*5000);//create space for file names...

	PATTERNFILE=PATTERNFILE_DEFAULT;HITSFILE=HITSFILE_DEFAULT;UNIQUEFILE=UNIQUEFILE_DEFAULT;GENOMEFILE=GENOMEFILE_DEFAULT;
	BWTFILE = BWTFILE_DEFAULT; 
	OCCFILE = OCCFILE_DEFAULT;
	REVBWTINDEX=REVBWTINDEX_DEFAULT;
	REVOCCFILE=REVOCCFILE_DEFAULT;

	Translated_String[STRINGLENGTH]='\0';//temporary buffer for translation of string...

	for(;;)	
	{
		Current_Option=getopt_long(argc, argv, Short_Options, Long_Options, NULL);
		if (Current_Option == -1 ) break;
		switch(Current_Option)
		{
			case 'h':
				printf("%s \n",Help_String);exit(0);
			case 'k':
				SUPER_QUALITY=optarg[0];
				break;
			case 'n':
				MAX_MISMATCHES = atoi(optarg);
				if (MAX_MISMATCHES <0 or MAX_MISMATCHES >5) MAX_MISMATCHES=5;
				break;
			case 'x':
				PRINT_MISHITS=TRUE;
				Mishit_File=File_Open("mishits.fq","w");
				break;
			case 't':
				QUALITY_THRESHOLD=optarg[0];
				break;
			case 's':
				ONEFMINDEX =TRUE;
				break;
			case 'q':
				PATTERNFILE=optarg;
				break;
			case 'o':
				HITSFILE=optarg;
				break;
			case 'u':
				UNIQUEFILE=optarg;
				break;
			case 'b':
				DISKBUFFERSIZE=atol(optarg);
				break;
			case 'm':
				if(!(MAXHITS=atoi(optarg))) {printf("Maximum hits defaulted to 1\n");MAXHITS=1;};
				break;
			case 'l':
				STRINGLENGTH=atoi(optarg);
				break;
			case 'f':
				HITMODE=atoi(optarg);
				if(HITMODE ==0) { UNIQUEHITS =FALSE;ALLHITS = FALSE; }
				else if(HITMODE ==1) { HITMODE=DEEP;UNIQUEHITS =TRUE;ALLHITS = FALSE; }
				else if(HITMODE ==2) { HITMODE=DEEP;UNIQUEHITS =TRUE;ALLHITS = TRUE; }
				else if(HITMODE ==3) { HITMODE=DEEP;UNIQUEHITS =FALSE;ALLHITS = TRUE;}
				else if(HITMODE ==4) { HITMODE=DEEP;UNIQUEHITS =FALSE;ALLHITS = TRUE;PRINT_DESC=FALSE;}
				break;
			case 'g':
				REVBWTINDEX = (char*)Source;
				REVBWTINDEX[0]='r';REVBWTINDEX[1]='e';REVBWTINDEX[2]='v';
				strcpy(REVBWTINDEX+3,optarg);
				strcat(REVBWTINDEX+3,".bwt"); 
				BWTFILE=REVBWTINDEX+3;
				REVOCCFILE = BWTFILE+1000;
				REVOCCFILE[0]='r';REVOCCFILE[1]='e';REVOCCFILE[2]='v';
				strcpy(REVOCCFILE+3,optarg);	
				strcat(REVOCCFILE+3,".fmv"); 
				OCCFILE=REVOCCFILE+3;			
				break;
			default:
				printf("%s \n",Help_String);
				exit(0);
		}
	}	
}

//}-----------------------------  Parse Command Line  -------------------------------------------------
void Build_Tables()
{
	Range LRange;
	for( int i=0;i<4;i++)//fill lookup tables for first character...
	{
		Forward_Start_Lookup[i]=revfmi->cumulativeFreq[i] + 1;
		Forward_End_Lookup[i]=revfmi->cumulativeFreq[i + 1];
		Backward_Start_Lookup[i]=fwfmi->cumulativeFreq[i] + 1;
		Backward_End_Lookup[i]=fwfmi->cumulativeFreq[i + 1];//
		LRange.Start=revfmi->cumulativeFreq[i] + 1;
		LRange.End=revfmi->cumulativeFreq[i + 1];
		LRange.Label=i;
		for(int j=0;j<4;j++) Build_Preindex_Forward(LRange, 2, j);
		LRange.Start=fwfmi->cumulativeFreq[i] + 1;
		LRange.End=fwfmi->cumulativeFreq[i + 1];
		LRange.Label=i;
		for(int j=0;j<4;j++) Build_Preindex_Backward(LRange, 2, j);

	}
}

void Allocate_Memory()
{
	int Max_Allocate=1;
	int Max_Limit=5;
	for (int i=0;i<Max_Limit-1;i++) Max_Allocate=Max_Allocate*STRINGLENGTH;
	Forward_Start_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	Forward_End_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	Backward_Start_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	Backward_End_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	BMHStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	FSHStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	FSHStackX0X=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	FSSStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	BMStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	BMStack_X11=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	BMStack_X11H=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	PSBStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Exact_Match_Forward=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Exact_Match_Backward=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Left_Mishits=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);	
	Right_Mishits=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);	
	Mismatches_Backward=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);//STRINGLENGTH*STRINGLENGTH);//*STRINGLENGTH);	
	Mismatches_Forward=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);//STRINGLENGTH*STRINGLENGTH*STRINGLENGTH);	
	Two_Mismatches_At_End_Forward=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	Two_Mismatches_At_End=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	Possible_20=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	Possible_02=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	if (NULL==Mismatches_Backward||NULL==Two_Mismatches_At_End_Forward||NULL==Two_Mismatches_At_End||NULL==BMHStack||NULL==FSHStackX0X||NULL==FSHStack||NULL==FSSStack||NULL==BMStack_X11H||NULL==BMStack_X11||NULL==BMStack||NULL==Exact_Match_Backward||NULL==Exact_Match_Forward||NULL==Mismatches_Forward||NULL==PSBStack||NULL==Forward_End_LookupX||NULL==Forward_Start_LookupX||NULL==Source)
	{
		printf("out of memory");
		exit(1);
	}

}

void Open_Files()
{

	Input_File=File_Open(PATTERNFILE,"r");//Load tags
	Output_File=File_Open(HITSFILE,"w");//Open output file...
	Unique_File=File_Open(UNIQUEFILE,"w");//Open output file...
	if(setvbuf(Unique_File,NULL,_IOFBF,DISKBUFFERSIZE*sizeof(long))||setvbuf(Output_File,NULL,_IOFBF,DISKBUFFERSIZE*sizeof(long)))
	{
		printf("Allocating disk buffers failed...\n");
		exit(1);
	}

}

void Init_Variables()
{
	fpos64_t File_Offset;
	while(fgets(Description,MAXDES,Input_File)!=0)
	{
		if(Description[0]=='>') break;
		fgetpos64(Input_File,&File_Offset);
	}
	fgets(Current_Tag_Buffer,MAXTAG,Input_File);//fseek(Input_File,0,SEEK_SET);//go top
	fsetpos64(Input_File,&File_Offset);
	for(STRINGLENGTH=0;Current_Tag[STRINGLENGTH]!='\n' && Current_Tag[STRINGLENGTH]!='\r' && Current_Tag[STRINGLENGTH]!=0;STRINGLENGTH++);

	if (STRINGLENGTH < 28) LOOKUPSIZE=3;
	LH=STRINGLENGTH/2;//calculate tag portions...
	LHQL=LH/2;
	if ((STRINGLENGTH % 2)) {LH++;LHQL++;}	
	LHQR=LH-LHQL;
	RH=STRINGLENGTH-LH;
	RHQL=RH/2;RHQR=RH-RHQL;

	High_Quality=(char*)malloc(STRINGLENGTH+1);
	Low_Quality=(char*)malloc(STRINGLENGTH+1);
	Do_All=(char*)malloc(STRINGLENGTH+1);
	

	Char_To_Code['A']=0;Char_To_Code['C']=1;Char_To_Code['G']=2;Char_To_Code['T']=3;Char_To_Code['a']=0;Char_To_Code['c']=1;Char_To_Code['g']=2;Char_To_Code['t']=3;//we are using character count to store the fmicode for acgt
	Char_To_Code['0']=0;Char_To_Code['1']=1;Char_To_Code['2']=2;Char_To_Code['3']=3;Char_To_Code['a']=0;Char_To_Code['c']=1;Char_To_Code['g']=2;Char_To_Code['t']=3;//we are using character count to store the fmicode for acgt


}

void Load_Indexes()
{
	fwfmi=initFMI(BWTFILE,OCCFILE);//Load FM indexes
	revfmi=initFMI(REVBWTINDEX,REVOCCFILE);
	SOURCELENGTH = fwfmi->textLength;
	if (SOURCELENGTH!=revfmi->textLength)
	{ 
		printf("FM index load error \n"); 
		exit(1);
	}
	FWDInverseSA0=fwfmi->inverseSa0;
	REVInverseSA0=revfmi->inverseSa0;
}

void Verbose()
{
	printf("\n-= BATMAN beta version(SOLiD) =-\n");
	printf("Using the genome files\n %s\t %s\n %s\t %s\n", BWTFILE,OCCFILE,REVBWTINDEX,REVOCCFILE); 
	printf("Query File : %s \t\t Output file: %s\n",PATTERNFILE,HITSFILE);
	printf("Length of Tags: %d\t Quality threshold : %c\n", STRINGLENGTH, QUALITY_THRESHOLD);
	printf("Mismatches allowed : %d\n",MAX_MISMATCHES);
	if (SUPER_QUALITY!=127) printf("Positions with quality larger than %c will be fixed..\n",SUPER_QUALITY);

}

