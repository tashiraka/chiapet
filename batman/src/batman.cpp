//BATMAN 1.1 - added Substring search..
//BATMAN 1.10 - enhanced default output processing...
//		handle N's.
//		print blanks...
//		--maxhits bug fixes
//{-----------------------------  INCLUDE FILES  -------------------------------------------------/
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <xmmintrin.h>
#include <emmintrin.h>
//#include <dvec.h>
#include <getopt.h>
extern "C" 
{
	#include "iniparser.h"
	#include <time.h>
	#include "MemManager.h"
	#include "MiscUtilities.h"
	#include "TextConverter.h"
	#include "BWT.h"
}
//}-----------------------------  INCLUDE FILES  -------------------------------------------------/


//{-----------------------------  DEFINES  -------------------------------------------------/
#define DEFAULT 0
#define DEEP 1
#define PAIREND 2
#define PAIR_END_SEPERATOR '\t'
#define MAXDES 500 
#define MAXTAG 180
#define EXTRA 0//Stuff like cr/lf at string seperators in the input file
#define INTEGERSIZE 8 //integer size
#define PACKEDBITSIZE 2
#define BITMASK 1 //3
#define FALSE 0
#define TRUE 1
#define NOMISMATCHES 100 

#define START_OF_MARK 9//18  //Start of scanning the tag for the pruning of one mismatch...
#define BRANCHTHRESHOLD 80 //30 //Threshold at which to check the BWT instead of branching
//}-----------------------------  DEFINES  -------------------------------------------------/
//{-----------------------------  STRUCTS  -------------------------------------------------/
struct Header
{
	char ID[3] ;
	unsigned MAXHITS;
	char HITMODE;
	char Index_Count;
	int Tag_Length;
	char Print_Desc;//print the tag desc?
}__attribute__((__packed__));

struct SARange
{

	unsigned long Start;
	unsigned long End;
	int Level;//at what relative node are we?
	char Mismatches;//number of mismatches at this level
	int Tag;//Tag number
	unsigned Mismatch_Pos;//6|6|...
	unsigned Mismatch_Char;//2|2|...
	
};

struct Range
{
	unsigned Start;
	unsigned End;
	int Label;//Final Label of the range...
};

struct Mismatches_Record
{

	int 	Gap;
	int Mismatch_Pos;//6|6|...
	unsigned Mismatch_Char;//2|2|...


}__attribute__((__packed__));

struct Output_Record
{
	unsigned Tag;
	unsigned  Start;
	char Index;
	char Mismatches;
	int Gap;
}__attribute__((__packed__));
#define REVERSE 1
#define FORWARD 0


struct Branches
{
	long Is_Branch [4];
};
//}-----------------------------  STRUCTS  -------------------------------------------------/*

//{-----------------------------  FUNCTION PRTOTYPES  -------------------------------------------------/*

void Convert_To_Reverse(SARange &Tag);
void Build_Tables();
void Allocate_Memory();
void Init_Variables();
void Load_Indexes();
void Verbose();
void Open_Files();
void Build_Preindex_Forward(Range Range, int Level, int Bit);
void Build_Preindex_Backward(Range Range, int Level, int Bit);
void Parse_Command_line(int argc, char* argv[]);
void Read_INI();

void One_Branch(struct SARange Tag,BWT *fmi);
void Branch_Detect (const struct SARange Tag,BWT *fmi,int Start);
void Branch_Detect_Backwards (const struct SARange Tag,BWT *fmi,int Start);

void Print_LocationX (struct SARange & Tag);
void Show_Progress(unsigned Percentage);
void Get_SARange_Fast( long New_Char,struct SARange & Range,BWT *fmi);

void Reverse(struct SARange & Tag,int Start,int StringLength);
void Backwards(struct SARange & Tag,int Start,int StringLength);

void Search_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Forwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Forwards(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Forwards_Indel(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Forwards_0X(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Forwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Forwards_OneSA_Indel(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_X01(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_X01_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_01X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_01X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Half_Tag_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);

void Search_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Half_Tag_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Backwards_Exact_X0(struct SARange & Tag,int Start,int StringLength,BWT *fmi);
void Search_Backwards(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_Indel(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_X10(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_Backwards_X10_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_10X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);
void Search_10X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi);

SARange Get_SARange( long New_Char,struct SARange Range,BWT *fmi);
char Guess_Orientation();
FILE* File_Open(const char* File_Name,const char* Mode);
BWT* initFMI(const char* BWTCodeFileName,const char* BWTOccValueFileName);
//}-----------------------------  FUNCTION PRTOTYPES  -------------------------------------------------/*

//{---------------------------- GLOBAL VARIABLES -------------------------------------------------

FILE* Input_File;
FILE* Output_File;
FILE* Unique_File;
FILE* Mishit_File;
FILE* Ambiguous_File;
FILE* Stat_File;

Output_Record Record;
Header Header;
MMPool *mmPool;
Mismatches_Record Mismatches;
BWT *fwfmi,*revfmi;
off_t File_Size;
time_t Start_Time,End_Time;

SARange* BMHStack;//[2*STRINGLENGTHMAX];
SARange* FSHStack;
SARange* FSHStackX0X;
SARange* FSSStack;
SARange* BMStack;
SARange* BMStack_X11;
SARange* BMStack_X11H;
SARange* PSBStack;

SARange* Exact_Match_Left;
SARange* Exact_Match_Right;
SARange* Exact_Match_Forward;
SARange* Exact_Match_ForwardF;
SARange* Exact_Match_ForwardC;
SARange* Exact_Match_Backward;
SARange* Left_Mishits;//stores mismatches in first half
SARange* Right_Mishits;//stores mismatches in first half
SARange* Mismatches_Forward;//stores possible 2-mismatches
SARange* Mismatches_Backward;//stores possible 2-mismatches
SARange* Two_Mismatches_At_End_Forward;//stores 2-mismatches whose last mismatch occurs at the last nuce..
SARange* Two_Mismatches_At_End;//stores 2-mismatches whose last mismatch occurs at the last nuce..
SARange* Possible_20;//stores possible 2-mismatches in right half... 
SARange* Possible_02;//stores possible 2-mismatches in right half... 

SARange Branch_Ranges[4];
SARange Temp_Branch_Ranges[4];
SARange Temp_Branch_Ranges2[4];


int Left_Mishits_Pointer;
int Right_Mishits_Pointer;
int Mismatches_Forward_Pointer;
int Mismatches_Backward_Pointer;
int Two_Mismatches_At_End_Pointer; 
int Two_Mismatches_At_End_Forward_Pointer=0;
int Possible_20_Pointer=0;
int Possible_02_Pointer=0;
int FMIndex;//keeps track of which fm-index to use...
int One_Mismatch_Already;
int c;//to hold a character temporarily
int Actual_Tag;
int LOOKUPSIZE=6;
int PLOOKUPSIZE,PRLOOKUPSIZE;
int MAXHITS =1;
int HITMODE = DEFAULT;
int STRINGLENGTH=36;// 36//36//6+EXTRA//6+EXTRA//36
int PAIR_LENGTH_RIGHT,PAIR_LENGTH_LEFT;
int HALFSTRINGLENGTH=18;// 18
int QUARTERSTRINGLENGTH=9;// 9
int LH,RH,LHQL,LHQR,RHQL,RHQR;
int PLH,PRH,PLHQL,PLHQR,PRHQL,PRHQR;
int PRLH,PRRH,PRLHQL,PRLHQR,PRRHQL,PRRHQR;
int MAX_HIGH_QUALITY_LENGTH=0;
int MAX_MISMATCHES_H=0;
int IGNOREHEAD=0;
int FORCELENGTH=0;

unsigned FWDInverseSA0;
unsigned Branch_Characters[4],Temp_BC[4],Temp_BC1[4],Temp_BC2[4];//counts the characters in a SARange.
unsigned Hits,M0,M1,M2,Total_Hits=0;
unsigned Total_Tags=0;
unsigned Tags_Read;
unsigned Number_of_Tags,Average_Length;
unsigned Forward_Start_Lookup[4],Forward_End_Lookup[4],Backward_Start_Lookup[4],Backward_End_Lookup[4];
unsigned* Forward_Start_LookupX;
unsigned* Forward_End_LookupX;
unsigned* Backward_Start_LookupX;
unsigned* Backward_End_LookupX;
unsigned SOURCELENGTH;
unsigned DISKBUFFERSIZE =1000;

char Description[MAXDES+1];
char Original[MAXTAG+1];
char Tag_Copy[MAXTAG+1];
char Complement[MAXTAG+1];
char Quality[MAXTAG];
char Quality_Bound;
char Maximum_Quality;
char Sum;
char Print_Header;
char New_Record;
char Ignore_Print=FALSE;
char One_Added_To_Hits;

char Minimum_Quality,Minimum_Quality_Location;
char Translated_String[100];//STRINGLENGTHMAX+1];
char Char_To_Code[256];
char Char_To_CodeC[256];
char Temp_Char_Array[20];
char Direction=0;//Direction =0,1 scan org, then comple. Direction =1,2 scan compl, then org
char First_Pass=TRUE;
char First_Pass_Printed=FALSE;
char Rollover_Step=FALSE;
char TagProcessed=FALSE;
char Tag_Printed=FALSE;
char TAG_GUESS=TRUE;
char ROLLOVER=FALSE;
char FILTER_AMBIGUOUS=FALSE;//decides whether to filter the ambiguous entries to ambiguous.fq
char PRINT_MISHITS=FALSE;
char PRINT_DESC=TRUE;//FALSE;
char INDELSCAN=TRUE;
char MAX_MISMATCHES=4;
char DIRECTIONS = 2;//DIRECTION=1 scan one leg,=2 scan original then comp, =3 scan comple, then original.
char BWTFILE_DEFAULT[] = "genome.bwt"; 
char OCCFILE_DEFAULT[] ="genome.fmv";
char REVBWTINDEX_DEFAULT[] ="revgenome.bwt";
char REVOCCFILE_DEFAULT[] ="revgenome.fmv";
char GENOMEFILE_DEFAULT[]="genome";
char PATTERNFILE_DEFAULT[]="tags.fq";
char HITSFILE_DEFAULT[]="hits.txt";
char UNIQUEFILE_DEFAULT[]="unique.txt";
char AMBIGUOUSFILE_DEFAULT[]="ambiguous.txt";
char MISHITFILE_DEFAULT[]="mishits.fq";
char UNIQUEHITS = FALSE;//do we need to separate uniquehits to a file?
char USEQUALITY=FALSE;
char HEURISTIC=FALSE;
char ALLHITS=FALSE;
char SUPERACCURATE=FALSE;
char COUNT_ALLHITS=TRUE;
char ONEFMINDEX =FALSE;
char MISMATCHES_TO_TRY_FIRST_LEFT=2;//4;
char MISMATCHES_TO_TRY_FIRST_RIGHT=2;//4;
char NORMAL_TAGS=TRUE;
char PRINTBLANKS=FALSE;
char SCANBOTH=FALSE;
char STATMODE=FALSE;

char* Source;
char* Do_Branch;//decides on quality whether to branch or not...
char* Low_Quality;
char* Do_All;
char* Read_Buffer;
char* BWTFILE ; 
char* OCCFILE ;
char* REVBWTINDEX;
char* REVOCCFILE;
char* GENOMEFILE;
const char* Code_To_Char="acgt";
char* Current_Tag=Original;
char* PATTERNFILE;
char* HITSFILE;
char* UNIQUEFILE;
char* AMBIGUOUSFILE=AMBIGUOUSFILE_DEFAULT;
char* MISHITFILE=MISHITFILE_DEFAULT;
char* GENFILE;

 
//}---------------------------- GLOBAL VARIABLES -------------------------------------------------

//{---------------------------- Command Line  -------------------------------------------------
option Long_Options[]=
{
{"help",0,NULL,'h'},
{"query",1,NULL,'q'},
{"output",1,NULL,'o'},
{"uniquefile",1,NULL,'u'},
{"genome",1,NULL,'g'},
{"buffersize",1,NULL,'b'},
{"maxhits",1,NULL,'m'},
{"format",1,NULL,'f'},
{"singleindex",0,NULL,'s'},
{"mishits",2,NULL,'x'},
{"maxmismatches",1,NULL,'n'},
{"scancomplement",0,NULL,'c'},
{"noquality",0,NULL,'p'},
{"filterambiguous",2,NULL,'A'},
{"statistics",0,NULL,'S'},
{"accuracy",1,NULL,'a'},
{"heuristics",2,NULL,'H'},
{"noindel",0,NULL,'i'},
{"ignorehead",1,NULL,'I'},
{"forcelength",1,NULL,'F'},
{"rollover",0,NULL,'r'},
{"printblanks",0,NULL,'B'},
{"scanboth",0,NULL,'M'},
{0,0,0,0}
};

char *myopts[] = { 
#define HEU_LENGTH 0 
"length",
 #define HEU_MISMATCHES 1 
"mm", 
 NULL}; 

//}---------------------------- Command Line -------------------------------------------------

int main(int argc, char* argv[])
{
//{-----------------------------  INITIALIZE ----------------------------------------------

	Read_INI();
	Parse_Command_line(argc,argv);	
	Open_Files();
	Init_Variables();
	Allocate_Memory();
	Load_Indexes();	
	Build_Tables();
	Verbose();
	
//}-----------------------------  INITIALIZE ----------------------------------------------
	time(&Start_Time);
	struct SARange Range,TRange;
	int Start;
	int Progress=0;
	char Quality_Count[256];
	char All_Zero[256];
	int Possible_04_Pointer,Possible_40_Pointer,Possible_50_Pointer;
	Actual_Tag= -1;
//---------------------------------------------------------------------

	if (!NORMAL_TAGS){ HITMODE=PAIREND;}
	Header.ID[0]='B';//"BAT";
	Header.ID[1]='A';//"BAT";
	Header.ID[2]='T';//"BAT";
	Header.MAXHITS=MAXHITS;
	Header.HITMODE = HITMODE;
	Header.Tag_Length=STRINGLENGTH;
	Header.Index_Count=ONEFMINDEX;
	Header.Print_Desc=PRINT_DESC;
	fwrite(&Header,sizeof(Header),1,Output_File);
	if(!NORMAL_TAGS)
	{
		fwrite(&PAIR_LENGTH_LEFT,sizeof(int),1,Output_File);
		fwrite(&PAIR_LENGTH_RIGHT,sizeof(int),1,Output_File);
	}
	if(UNIQUEHITS)
	{
		Header.HITMODE = DEFAULT;
		fwrite(&Header,sizeof(Header),1,Unique_File);
	}
//---------------------------------------------------------------------

	for (int i=1;i<256;i++)
	{
		All_Zero[i]=0;
	}
	printf("======================]\r[");//progress bar....
	fflush(stdout);

	for (int i=0;i<STRINGLENGTH;i++) Do_All[i]=TRUE;
	Tag_Printed=TRUE;//avoid first printblank...
	Hits=0;

	for(;;)//Tag Processing loop.....
	{
		Total_Hits=Total_Hits+Hits;
		if (PRINTBLANKS && !Tag_Printed && First_Pass)// && !First_Pass_Printed)
		{
			if(!NORMAL_TAGS && First_Pass_Printed){}
			else
			{
				New_Record='@';
				fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global

				if(PRINT_DESC) fprintf(Output_File,"%s",Description);
				if (!NORMAL_TAGS)
				{
					fprintf(Output_File,"%s",Tag_Copy);
				}
				else
				{
					for( int i=0;i<STRINGLENGTH; i++) Translated_String[i]=Code_To_Char[Current_Tag[i]];
					Translated_String[STRINGLENGTH]='\n';//Current_Tag[STRINGLENGTH];
					fwrite(Translated_String,1,STRINGLENGTH+1,Output_File);//write tag...
				}
			}
		}

		Current_Tag=Original+IGNOREHEAD;
		Direction=0;//this is the default for brute force scan...
		Actual_Tag++;
		Progress++;
		One_Added_To_Hits=FALSE;
		if (Progress==Number_of_Tags) 
		{
			off_t Current_Pos=ftello(Input_File);
			Average_Length=Current_Pos/Actual_Tag+1;//+1 avoids divide by zero..
			Number_of_Tags=(File_Size/Average_Length)/20;
			Progress=0;
			Show_Progress(Current_Pos*100/File_Size);
		}
		Hits=0;M0=0;M1=0;M2=0;Ignore_Print=FALSE;//GIS mod..
		Print_Header=FALSE;//Header not printed yet...

		if(NORMAL_TAGS)
		{
			Tag_Printed=FALSE;
			if (fgets(Description,MAXDES,Input_File)!=0)//des .... read a tag
			{
				fgets(Current_Tag-IGNOREHEAD,MAXDES,Input_File);//tag
				fgets(Quality,MAXTAG,Input_File);//plus
				fgets(Quality,MAXTAG,Input_File);//phred
				for (unsigned i=0;i<=STRINGLENGTH-1;i++)
				{
					Current_Tag[i]=Char_To_Code[Current_Tag[i]];
					Complement[STRINGLENGTH-1-i]=Char_To_CodeC[Current_Tag[i]];//change later...
				}
				Current_Tag[STRINGLENGTH]='+';
				Complement[STRINGLENGTH]='-';
			}
			else {break;}
		}
		else //pair end tags...
		{
			if (First_Pass)//Process head...
			{
				Tag_Printed=FALSE;
				First_Pass_Printed=FALSE;
				STRINGLENGTH=PAIR_LENGTH_LEFT;
				if (fgets(Description,MAXDES,Input_File)!=0)//des .... read a tag
				{
					fgets(Current_Tag-IGNOREHEAD,MAXDES,Input_File);//tag
					strcpy(Tag_Copy,Current_Tag);
					for (unsigned i=0;i<=STRINGLENGTH-1;i++)
					{
						Current_Tag[i]=Char_To_Code[Current_Tag[i]];
						Complement[STRINGLENGTH-1-i]=Char_To_CodeC[Current_Tag[i]];//change later...
					}
					Current_Tag[STRINGLENGTH]='+';
					Complement[STRINGLENGTH]='-';
				}
				else {break;}
				LH=PLH;LHQL=PLHQL;LHQR=PLHQR;RH=PRH;RHQL=PRHQL;RHQR=PRHQR;//LOOKUPSIZE=PLOOKUPSIZE;
				First_Pass=FALSE;
			}
			else//Process tail
			{
				STRINGLENGTH=PAIR_LENGTH_RIGHT;
				for (unsigned i=0;i<=STRINGLENGTH-1;i++)
				{
					Current_Tag[i]=Char_To_Code[Current_Tag[i+PAIR_LENGTH_LEFT+1]];
					Complement[STRINGLENGTH-1-i]=Char_To_CodeC[Current_Tag[i]];//change later...
				}
				Current_Tag[STRINGLENGTH]='+';
				Complement[STRINGLENGTH]='-';
				LH=PRLH;LHQL=PRLHQL;LHQR=PRLHQR;RH=PRRH;RHQL=PRRHQL;RHQR=PRRHQR;//LOOKUPSIZE=PRLOOKUPSIZE;
				First_Pass=TRUE;
			}
		}
//----------------------------------------------------------------------------------------------------------------------------------------
//{------------------------------------- QUALITY CHECK --------------------------------------------------------------------------------------------------
		if (USEQUALITY)
		{
			Minimum_Quality=Quality[0];
			Maximum_Quality=Quality[0];
			memcpy(Quality_Count,All_Zero,256);
			for (int i=0;i<LH;i++)//filter high/low quality scores
			{
				Quality_Count[Quality[i]]++;
				if (Minimum_Quality>Quality[i]) { Minimum_Quality=Quality[i];}
				if (Maximum_Quality<Quality[i]) { Maximum_Quality=Quality[i];}
			}

			Sum=0;
			for(int i=Minimum_Quality;i<Maximum_Quality;i++)
			{
				Sum=Sum+Quality_Count[i];
				if (Sum >= MISMATCHES_TO_TRY_FIRST_LEFT)
				{
					Quality_Bound=i;
					break;
				}
			}

			for (int i=0;i<LH;i++)//filter high/low quality scores
			{
				if(Quality[i] > Quality_Bound)//never mutate very high quality score...
				{
					Low_Quality[i]=FALSE;
				}
				else
				{
					Low_Quality[i]=TRUE;

				}
			}
			Maximum_Quality=Quality[LH];

			for (int i=LH;i<STRINGLENGTH;i++)//filter high/low quality scores
			{
				Quality_Count[Quality[i]]++;
				if (Minimum_Quality>Quality[i]) { Minimum_Quality=Quality[i];}
				if (Maximum_Quality<Quality[i]) { Maximum_Quality=Quality[i];}
			}

			Sum=0;
			for(int i=Minimum_Quality;i<Maximum_Quality;i++)
			{
				Sum=Sum+Quality_Count[i];
				if (Sum >= MISMATCHES_TO_TRY_FIRST_RIGHT)
				{
					Quality_Bound=i;
					break;
				}
			}

			for (int i=LH;i<STRINGLENGTH;i++)//filter high/low quality scores
			{
				if(Quality[i] > Quality_Bound)//never mutate very high quality score...
				{
					Low_Quality[i]=FALSE;
				}
				else
				{
					Low_Quality[i]=TRUE;

				}
			}
		}
//}------------------------------------- QUALITY CHECK --------------------------------------------------------------------------------------------------
		char Reverse_Exact_Not_Scanned=TRUE;//in tag guess, we did not find an exact match...
//{------------------------------------------TAG GUESS ---------------------------------------------------------------------------------------------
		if (TAG_GUESS)
		{
			if (!Guess_Orientation()) //Exact match found...
			{
				if(MAX_MISMATCHES == 0) continue;
				if (Direction) Reverse_Exact_Not_Scanned=FALSE;
			}//else tag direction guessed only...
			else
			{
				Print_Header=FALSE;//Do not print description...
			}
		}
//}------------------------------------------TAG GUESS ---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------
		Rollover_Step=FALSE;
		TagProcessed=FALSE;
		char Loop_Enter;//var to allow forced entry to loop..
		Loop_Enter=STATMODE;

		while(Hits<MAXHITS || SCANBOTH || Loop_Enter) 
		{
			Loop_Enter=FALSE;
			if (TagProcessed)
			{
				if (!ROLLOVER)
				{
					if(Hits) break;
				}
				else
				{ // rolling over ...
					if(Rollover_Step) break;//already in a rollover...
					else
					{
						if (SCANBOTH) {Total_Hits=Total_Hits+Hits;Hits=0;}
						Rollover_Step=TRUE;
					}
				}
			}
			TagProcessed=TRUE;

			if (Direction == DIRECTIONS)//have tried both directions.. 
			{
				if(PRINT_MISHITS)
				{
					for (unsigned i=0;i<=STRINGLENGTH-1;i++)//convert and write fasta
					{
						Current_Tag[i]=Code_To_Char[Current_Tag[i]];
					}
					Current_Tag[STRINGLENGTH]=0;
					fprintf(Mishit_File,"%s%s\n+\n%s", Description,Current_Tag,Quality);
				}
				break;
			}
//----------------------------------------------------------------------------------------------------------------------------------------
			if (Direction == 1)//complementary direction
			{
				Current_Tag=Complement;
				Exact_Match_Forward=Exact_Match_ForwardC;
				Current_Tag[STRINGLENGTH]='-';
				Print_Header=TRUE;

				if(Reverse_Exact_Not_Scanned) //Forward scan found an exact match, and so reverse complement was not scanned for exact match...
				{
					FMIndex=REVERSE;
					if(LOOKUPSIZE ==3)
					{
						c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
					}
					else
					{
						c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
					}

					Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
					Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
					Search_Forwards_Exact(Range,-1,STRINGLENGTH,revfmi);//Find exact matches and report... if not found get the range for 0|?
					if (Hits ==MAXHITS) continue;
				}
			}
			else if (Direction ==2)//happens if the first dirction tried is comp, and then we have to try original dirn.
			{
				Current_Tag=Original+IGNOREHEAD;
				Exact_Match_Forward=Exact_Match_ForwardF;
				Current_Tag[STRINGLENGTH]='+';
			}

			Direction++;
			Left_Mishits_Pointer=0;
			Right_Mishits_Pointer=0;
			Possible_20_Pointer=0;
			Possible_02_Pointer=0;
			Mismatches_Forward_Pointer=0;//first node where SA range was not found, all other nodes will not have matches..
			Mismatches_Backward_Pointer=0;
			Two_Mismatches_At_End_Pointer=0;
			Two_Mismatches_At_End_Forward_Pointer=0;
			Start=1-2;//Adjust for offsets...
			FMIndex=REVERSE;

			if( !TAG_GUESS)
			{
				if(LOOKUPSIZE ==3)
				{
					c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
				}
				else
				{
					c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
				}
				Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
				Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
				Search_Forwards_Exact(Range,Start,STRINGLENGTH,revfmi);//Find exact matches and report... if not found get the range for 0|?
				if(MAXHITS==Hits || MAX_MISMATCHES == 0) continue;
			}

			if(HEURISTIC)
			{
				if(Range.Level >MAX_HIGH_QUALITY_LENGTH)
				{
					Range=Exact_Match_Forward[Start+Range.Level-1];
					Range.Level++;
					Do_Branch=Low_Quality;
					Search_Forwards(Range,MAX_MISMATCHES_H,1,STRINGLENGTH,revfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
					if(MAXHITS==Hits) continue;
					Do_Branch=Do_All;
					Search_Forwards(Range,MAX_MISMATCHES_H,1,STRINGLENGTH,revfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
					if(MAXHITS==Hits) continue;
					Two_Mismatches_At_End_Pointer=0;
					Two_Mismatches_At_End_Forward_Pointer=0;
				}
			}
			M0=Hits;if (One_Added_To_Hits) M0--;

//{------------------------------------------- ONE MISMATCH ---------------------------------------------------------------------------------------------
			//One mismatches...
			Range=Exact_Match_Forward[Start+LH];
			if(Range.Start && Range.Tag == Actual_Tag)//if there are hits of the form 0|?
			{
				Range.Level=1;
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					Search_Forwards(Range,1,LH+1,RH,revfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				Search_Forwards(Range,1,LH+1,RH,revfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
				if(MAXHITS==Hits) continue;


			}		
			FMIndex=FORWARD;
			if(LOOKUPSIZE ==3)
			{
				c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
			}
			else
			{
				c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
			}
			Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
			Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
			Search_Backwards_Exact( Range,STRINGLENGTH,RH,fwfmi);//Backward scan for ?|0
			if(Range.Start)//if there are possible hits of the form ?|0
			{
				Range.Level=1;
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					Search_Backwards(Range,1,LH,LH,fwfmi);//Backward scan for one mismatches of the form 1|0, store possible mismatches of the form 2|0
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				Search_Backwards(Range,1,LH,LH,fwfmi);//Backward scan for one mismatches of the form 1|0, store possible mismatches of the form 2|0
				if(MAXHITS==Hits) continue;
			}
			M1=Hits-M0;if (One_Added_To_Hits) M1--;
			if (MAX_MISMATCHES == 1 ) {if (STATMODE && Hits) fprintf(Stat_File,"%d:%d:%d:%d\n",Actual_Tag,M0,M1,M2);continue;}
//}------------------------------------------- ONE MISMATCH ---------------------------------------------------------------------------------------------

//{------------------------------------------- TWO MISMATCH ---------------------------------------------------------------------------------------------
			FMIndex=REVERSE;
			if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
			{
				for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
				{
					Two_Mismatches_At_End_Forward[i].Mismatch_Pos=Two_Mismatches_At_End_Forward[i].Mismatch_Pos | ((STRINGLENGTH-1)<<6);//mismatches of the form 0|2, with last mismatch at the end...
					Print_LocationX(Two_Mismatches_At_End_Forward[i]);
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Forward_Pointer=0;

			FMIndex=FORWARD;
			if(Two_Mismatches_At_End_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
					{
						Print_LocationX(Two_Mismatches_At_End[i]);//Mismatches of the form 2|0, with one mismatch at the first position
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End[i]);//Mismatches of the form 2|0, with one mismatch at the first position
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}

			Two_Mismatches_At_End_Pointer=0;
			FMIndex=REVERSE;
			int Possible_03_Pointer=Mismatches_Forward_Pointer;
			if(Mismatches_Forward_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_03_Pointer-1;i>=0;i--)
					{
						Search_Forwards(Mismatches_Forward[i],2,LH+1,RH,revfmi);//scan for possible two mismatches of the form 0|2, and store candidates for 0|3
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_03_Pointer-1;i>=0;i--)
				{
					Search_Forwards(Mismatches_Forward[i],2,LH+1,RH,revfmi);//scan for possible two mismatches of the form 0|2, and store candidates for 0|3
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}

			FMIndex=FORWARD;
			int Possible_30_Pointer=Mismatches_Backward_Pointer;
			if(Mismatches_Backward_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_30_Pointer-1;i>=0;i--)
					{
						Search_Backwards(Mismatches_Backward[i],2,LH,LH,fwfmi);//scan for possible two mismatches of the form 2|0, and stores the candidates for 3|0
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_30_Pointer-1;i>=0;i--)
				{
					Search_Backwards(Mismatches_Backward[i],2,LH,LH,fwfmi);//scan for possible two mismatches of the form 2|0, and stores the candidates for 3|0
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}

//----------------------------------------------------------------------------------------------------------------------------------------
			if(LOOKUPSIZE==3)
			{
				c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
			}
			else
			{
				c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
			}
			Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;//Range.Tag=Actual_Tag;
			Range.Mismatch_Pos=0;Range.Mismatch_Char=0;

			Search_Backwards_Exact_X0( Range,STRINGLENGTH,RHQR,fwfmi);// ?|?|0
			Range.Level=1;

			if(USEQUALITY)
			{
				Do_Branch=Low_Quality;
				Search_Backwards_X10(Range,1,LH + RHQL, RHQL,fwfmi);//?|1|0 and extend, finds mismatches of the form 1|1 and stres candidates for 2|1
				if(MAXHITS==Hits) continue;
			}

			Do_Branch=Do_All;
			Search_Backwards_X10(Range,1,LH + RHQL, RHQL,fwfmi);//?|1|0 and extend, finds mismatches of the form 1|1 and stres candidates for 2|1
			if(MAXHITS==Hits) continue;

//----------------------------------------------------------------------------------------------------------------------------------------
			if(LOOKUPSIZE==3)
			{
				c=Current_Tag[LH+0] | (Current_Tag[LH+1]<<2) | (Current_Tag[LH+2]<<4);// | (Current_Tag[LH+3]<<6) | Current_Tag[LH+4]<<8 | (Current_Tag[LH+5]<<10);//Use lookup table...
			}
			else
			{
				c=Current_Tag[LH+0] | (Current_Tag[LH+1]<<2) | (Current_Tag[LH+2]<<4) | (Current_Tag[LH+3]<<6) | Current_Tag[LH+4]<<8 | (Current_Tag[LH+5]<<10);//Use lookup table...
			}
			Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;
			Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
			Search_Forwards_0X(Range,LH+1,RHQL,revfmi);
			Range.Level=1;TRange=Range;
			if(USEQUALITY)
			{
				Do_Branch=Low_Quality;

				Search_X01(Range,1,LH + RHQL +1,RHQR,revfmi);//?|0|1 and extend, finds mismatches of the form 1|1 and stres candidates for 2|1
				if(MAXHITS==Hits) continue;
			}

			Do_Branch=Do_All;
			Search_X01(TRange,1,LH + RHQL +1,RHQR,revfmi);//?|0|1 and extend, finds mismatches of the form 1|1 and stres candidates for 2|1
			if(MAXHITS==Hits) continue;
			M2=Hits-M1-M0;if (One_Added_To_Hits) M2--;
			if( MAX_MISMATCHES ==2) {if (STATMODE && Hits) fprintf(Stat_File,"%d:%d:%d:%d\n",Actual_Tag,M0,M1,M2);continue;}

//}------------------------------------------- TWO MISMATCH ---------------------------------------------------------------------------------------------
//{------------------------------------------- THREE MISMATCH ---------------------------------------------------------------------------------------------
			//Find three mismatches....
			FMIndex=REVERSE;
			if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
			{
				for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End_Forward[i]);//mismatches of the form 0|3, with last mismatch at the end...
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Forward_Pointer=0;

			FMIndex=FORWARD;
			if(Two_Mismatches_At_End_Pointer)
			{
				for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End[i]);//Mismatches of the form 3|0, with one mismatch at the first position
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Pointer=0;

			FMIndex=REVERSE;
			Possible_04_Pointer=Mismatches_Forward_Pointer;
			if(Mismatches_Forward_Pointer!=Possible_03_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_04_Pointer-1;i>=Possible_03_Pointer;i--)
					{
						Search_Forwards(Mismatches_Forward[i],3,LH+1,RH,revfmi);//scan for possible three mismatches of the form 0|3, and finds mismatches of the form 1|2, stores possibles in the form 1|3
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_04_Pointer-1;i>=Possible_03_Pointer;i--)
				{
					Search_Forwards(Mismatches_Forward[i],3,LH+1,RH,revfmi);//scan for possible three mismatches of the form 0|3, and finds mismatches of the form 1|2, stores possibles in the form 1|3
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}

			FMIndex=FORWARD;
			Possible_40_Pointer=Mismatches_Backward_Pointer;
			if(Mismatches_Backward_Pointer!=Possible_30_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_40_Pointer-1;i>=Possible_30_Pointer;i--)
					{
						Search_Backwards(Mismatches_Backward[i],3,LH,LH,fwfmi);//scan for possible mismatches of the form 3|0, 2|1 and sotres the candidates for 4|0, 3|1
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_40_Pointer-1;i>=Possible_30_Pointer;i--)
				{
					Search_Backwards(Mismatches_Backward[i],3,LH,LH,fwfmi);//scan for possible mismatches of the form 3|0, 2|1 and sotres the candidates for 4|0, 3|1
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}

//----------------------------------------------------------------------------------------------------------------------------------------
			FMIndex=REVERSE;
			if(LOOKUPSIZE==3)
			{
				c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
			}
			else
			{
				c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
			}
			Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;
			Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
			Search_Forwards_0X(Range,1,LHQL,revfmi);
			Range.Level=1;
			if(USEQUALITY)
			{
				Do_Branch=Low_Quality;
				Search_01X(Range,1,LHQL +1,LHQR,revfmi);//search for three mismatches of the form 1|2 and stores the candidates for 1|3
				if(MAXHITS==Hits) continue;
			}

			Do_Branch=Do_All;
			Search_01X(Range,1,LHQL +1,LHQR,revfmi);
			if(MAXHITS==Hits) continue;
//----------------------------------------------------------------------------------------------------------------------------------------
			if(LOOKUPSIZE==3)
			{
				c=Current_Tag[LH-1-0] | (Current_Tag[LH-1-1]<<2) | (Current_Tag[LH-1-2]<<4);// | (Current_Tag[LH-1-3]<<6) | Current_Tag[LH-1-4]<<8 | (Current_Tag[LH-1-5]<<10);//Use lookup table...
			}
			else
			{
				c=Current_Tag[LH-1-0] | (Current_Tag[LH-1-1]<<2) | (Current_Tag[LH-1-2]<<4) | (Current_Tag[LH-1-3]<<6) | Current_Tag[LH-1-4]<<8 | (Current_Tag[LH-1-5]<<10);//Use lookup table...
			}

			Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;//Range.Tag=Actual_Tag;
			Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
			Search_Backwards_Exact_X0( Range,LH,LHQR,fwfmi);// ?|0|?
			Range.Level=1;
			if(USEQUALITY)
			{
				TRange=Range;
				Do_Branch=Low_Quality;
				Search_10X(TRange,1,LHQL, LHQL,fwfmi);//search for three mismatches of the form 1|2 and stores the candidates for 1|3
				if(MAXHITS==Hits) continue;
			}
			Do_Branch=Do_All;
			Search_10X(Range,1,LHQL, LHQL,fwfmi);//search for three mismatches of the form 1|2 and stores the candidates for 1|3
			if(MAXHITS==Hits) continue;
			if( MAX_MISMATCHES ==3 && !INDELSCAN) continue;
//}------------------------------------------- THREE MISMATCH ---------------------------------------------------------------------------------------------
//{-------------------------------------------  INDEL  ---------------------------------------------------------------------------------------------

			if (INDELSCAN)
			{
				FMIndex=REVERSE;
				Range=Exact_Match_Forward[Start+LH];
				Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
				if(Range.Start && Range.Tag == Actual_Tag)//if there are hits of the form 0|?
				{
					Range.Level=1;
					Do_Branch=Do_All;
					Search_Forwards_Indel(Range,1,LH+1,RH,revfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
					if(MAXHITS==Hits) continue;
				}

				FMIndex=FORWARD;
				if(LOOKUPSIZE ==3)
				{
					c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
				}
				else
				{
					c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
				}
				Range.Start=Backward_Start_LookupX[c];Range.End=Backward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
				Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
				Search_Backwards_Exact( Range,STRINGLENGTH,RH,fwfmi);//Backward scan for ?|0
				if(Range.Start)//if there are possible hits of the form ?|0
				{
					Range.Level=1;

					Do_Branch=Do_All;
					Search_Backwards_Indel(Range,0,LH,LH,fwfmi);//scan for one mismatches of the form 0|1, store possible two mismatches of the form 0|2...
					if(MAXHITS==Hits) continue;
				}
				if( MAX_MISMATCHES ==3 ) continue;
			}

//}-------------------------------------------  INDEL  ---------------------------------------------------------------------------------------------

//{------------------------------------------- FOUR MISMATCH ---------------------------------------------------------------------------------------------
			FMIndex=REVERSE;
			if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
			{
				for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End_Forward[i]);//mismatches of the form 0|4, with last mismatch at the end...
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Forward_Pointer=0;

			FMIndex=FORWARD;
			if(Two_Mismatches_At_End_Pointer)
			{
				for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End[i]);//mismatches of the form 0|4, with one mismatch at the start...
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Pointer=0;

			FMIndex=REVERSE;
			int Possible_05_Pointer=Mismatches_Forward_Pointer;
			int Wrap=FALSE;
			if(Mismatches_Forward_Pointer)
			{
				if (Possible_04_Pointer > 46000) {Mismatches_Forward_Pointer=0;Wrap=TRUE;}
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_04_Pointer;i<Possible_05_Pointer;i++)//Mismatches_Forward_Pointer;i++)
					{
						Search_Forwards(Mismatches_Forward[i],4,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|4, and finds mismatches of the form 1|3, stores possibles in the form 1|4
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_04_Pointer;i<Possible_05_Pointer;i++)//Mismatches_Forward_Pointer;i++)
				{
					Search_Forwards(Mismatches_Forward[i],4,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|4, and finds mismatches of the form 1|3, stores possibles in the form 1|4
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			if(Wrap) Possible_05_Pointer=0;
			int Mismatches_Forward_Pointer_Last4=Mismatches_Forward_Pointer;

			FMIndex=FORWARD;
			Possible_50_Pointer=Mismatches_Backward_Pointer;
			if(Mismatches_Backward_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_50_Pointer-1;i>=Possible_40_Pointer;i--)//Mismatches_Backward_Pointer-1;i>=0;i--)
					{
						Search_Backwards(Mismatches_Backward[i],4,LH,LH,fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_50_Pointer-1;i>=Possible_40_Pointer;i--)//Mismatches_Backward_Pointer-1;i>=0;i--)
				{
					Search_Backwards(Mismatches_Backward[i],4,LH,LH,fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}

			FMIndex=REVERSE;
			int Left_Mishits_Pointer_1=Left_Mishits_Pointer;
			if(Left_Mishits_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=0;i<Left_Mishits_Pointer;i++)
					{
						Search_Forwards(Left_Mishits[i],4,1,STRINGLENGTH,revfmi);//find mismatches of the form 022 form, stores possibles of the form 023
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=0;i<Left_Mishits_Pointer;i++)
				{
					Search_Forwards(Left_Mishits[i],4,1,STRINGLENGTH,revfmi);//find mismatches of the form 022 form, stores possibles of the form 023
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}

			int Mismatches_Forward_Pointer_Last5=Mismatches_Forward_Pointer;
			if( Right_Mishits_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=0;i<Right_Mishits_Pointer;i++)
					{
						TRange=Right_Mishits[i];
						if(Right_Mishits[i].Level!=LHQL) 
						{
							Search_Backwards_Exact( Right_Mishits[i],LHQL,LHQL,fwfmi);//finds mismatches of the form 202, stores possibles of the form 203
						}
						if(Right_Mishits[i].Start)
						{	
							Backwards(Right_Mishits[i],1,LH);
							if(Right_Mishits[i].Start)
							{
								Right_Mishits[i].Level=1;
								Search_Forwards(Right_Mishits[i],4,LH+1,RH,revfmi);
								if(MAXHITS==Hits) break;
							}
						}
						Right_Mishits[i]=TRange;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=0;i<Right_Mishits_Pointer;i++)
				{

					if(Right_Mishits[i].Level!=LHQL) 
					{
						Search_Backwards_Exact( Right_Mishits[i],LHQL,LHQL,fwfmi);//finds mismatches of the form 202, stores possibles of the form 203
					}
					if(Right_Mishits[i].Start)
					{	
						Backwards(Right_Mishits[i],1,LH);
						if(Right_Mishits[i].Start)
						{
							Right_Mishits[i].Level=1;
							Search_Forwards(Right_Mishits[i],4,LH+1,RH,revfmi);
							if(MAXHITS==Hits) break;
						}
					}
				}
				if(MAXHITS==Hits) continue;
			}
			Range.Start=1;Range.End=SOURCELENGTH;Range.Mismatches=0;Range.Level=1;
			Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
			TRange=Range;
			if(USEQUALITY)
			{
				Do_Branch=Low_Quality;
				Search_11X(Range,1,1,LHQL,revfmi);//finds mismatches of the form 112, stores possibles 113 and in the left half tag,
				if(MAXHITS==Hits) continue;
			}

			Do_Branch=Do_All;
			Search_11X(TRange,1,1,LHQL,revfmi);//finds mismatches of the form 112, stores possibles 113 and in the left half tag,
			if(MAXHITS==Hits) continue;

			if( MAX_MISMATCHES ==4) continue;
//}------------------------------------------- FOUR MISMATCH ---------------------------------------------------------------------------------------------
//{------------------------------------------- FIVE MISMATCH ---------------------------------------------------------------------------------------------

			FMIndex=REVERSE;
			if(Two_Mismatches_At_End_Forward_Pointer)//give priority to forward direction as most erros occur in the end..
			{
				for(int i=0;i<Two_Mismatches_At_End_Forward_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End_Forward[i]);//mismatches of the form 0|4, with last mismatch at the end...
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Forward_Pointer=0;


			FMIndex=FORWARD;
			if(Two_Mismatches_At_End_Pointer)
			{
				for(int i=0;i<Two_Mismatches_At_End_Pointer;i++)
				{
					Print_LocationX(Two_Mismatches_At_End[i]);//mismatches of the form 0|5, with one mismatch at the start...
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}
			Two_Mismatches_At_End_Pointer=0;

			FMIndex=REVERSE;
			if(Mismatches_Forward_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_05_Pointer;i<Mismatches_Forward_Pointer_Last4;i++)//Mismatches_Forward_Pointer;i++)
					{
						Search_Forwards(Mismatches_Forward[i],5,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|5
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_05_Pointer;i<Mismatches_Forward_Pointer_Last4;i++)//Mismatches_Forward_Pointer;i++)
				{
					Search_Forwards(Mismatches_Forward[i],5,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|5
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}


			FMIndex=REVERSE;
			if(Mismatches_Forward_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Mismatches_Forward_Pointer_Last4;i<Mismatches_Forward_Pointer_Last5;i++)//Mismatches_Forward_Pointer;i++)
					{
						Search_Forwards(Mismatches_Forward[i],5,1,STRINGLENGTH,revfmi);//scan for possible five mismatches of the form 0|5, and finds mismatches of the form 1|4,2|3 
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Mismatches_Forward_Pointer_Last4;i<Mismatches_Forward_Pointer_Last5;i++)//Mismatches_Forward_Pointer;i++)
				{
					Search_Forwards(Mismatches_Forward[i],5,1,STRINGLENGTH,revfmi);//scan for possible five mismatches of the form 0|5, and finds mismatches of the form 1|4,2|3 
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}

			FMIndex=REVERSE;
			if(Mismatches_Forward_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Mismatches_Forward_Pointer_Last5;i<Mismatches_Forward_Pointer;i++)//Mismatches_Forward_Pointer;i++)
					{
						Search_Forwards(Mismatches_Forward[i],5,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|5
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Mismatches_Forward_Pointer_Last5;i<Mismatches_Forward_Pointer;i++)//Mismatches_Forward_Pointer;i++)
				{
					Search_Forwards(Mismatches_Forward[i],5,LH+1,RH,revfmi);//scan for possible four mismatches of the form 0|5
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;
			}

			if(SUPERACCURATE)
			{
				FMIndex=REVERSE;
				if(Left_Mishits_Pointer!=Left_Mishits_Pointer_1)
				{
					if(USEQUALITY)
					{
						Do_Branch=Low_Quality;
						for(int i=Left_Mishits_Pointer_1;i<Left_Mishits_Pointer;i++)
						{
							Search_Forwards(Left_Mishits[i],5,1,STRINGLENGTH,revfmi);//find mismatches of the form 122 form
							if(MAXHITS==Hits) break;
						}
						if(MAXHITS==Hits) continue;
					}

					Do_Branch=Do_All;
					for(int i=Left_Mishits_Pointer_1;i<Left_Mishits_Pointer;i++)
					{
						Search_Forwards(Left_Mishits[i],5,1,STRINGLENGTH,revfmi);//find mismatches of the form 122 form
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;

				}
			}

			FMIndex=FORWARD;
			if(Mismatches_Backward_Pointer!=Possible_50_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Mismatches_Backward_Pointer-1;i>=Possible_50_Pointer;i--)
					{
						Search_Backwards(Mismatches_Backward[i],5,LH,LH,fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Mismatches_Backward_Pointer-1;i>=Possible_50_Pointer;i--)
				{
					Search_Backwards(Mismatches_Backward[i],5,LH,LH,fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}
			FMIndex=FORWARD;
			if (Possible_20_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=Possible_20_Pointer-1;i>=0;i--)
					{
						Search_Backwards(Possible_20[i],5,LH + RHQL,(LH + RHQL),fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
						if(MAXHITS==Hits) break;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=Possible_20_Pointer-1;i>=0;i--)
				{
					Search_Backwards(Possible_20[i],5,LH + RHQL,(LH + RHQL),fwfmi);//scan for possible mismatches of the form 4|0, 3|1 and sotres the candidates for 5|0, 4|1
					if(MAXHITS==Hits) break;
				}
				if(MAXHITS==Hits) continue;

			}

			if(Possible_02_Pointer)
			{
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					for(int i=0;i<Possible_02_Pointer;i++)
					{
						TRange=Possible_02[i];
						if(Possible_02[i].Level!=RHQR) 
						{
							Search_Exact( Possible_02[i],LH + RHQL-1 ,RHQR,revfmi);//finds mismatches of the form 202, stores possibles of the form 203
						}
						if(Possible_02[i].Start) 
						{
							Reverse(Possible_02[i],STRINGLENGTH,RH);
							if(Possible_02[i].Start)
							{
								Possible_02[i].Level=1;
								Search_Backwards(Possible_02[i],5,LH,LH,fwfmi);
							}
						}
						if(MAXHITS==Hits) break;
						Possible_02[i]=TRange;
					}
					if(MAXHITS==Hits) continue;
				}

				Do_Branch=Do_All;
				for(int i=0;i<Possible_02_Pointer;i++)
				{

					if(Possible_02[i].Level!=RHQR) 
					{
						Search_Exact( Possible_02[i],LH + RHQL-1 ,RHQR,revfmi);//finds mismatches of the form 202, stores possibles of the form 203
					}
					if(Possible_02[i].Start) 
					{
						Reverse(Possible_02[i],STRINGLENGTH,RH);
						if(Possible_02[i].Start)
						{
							Possible_02[i].Level=1;
							Search_Backwards(Possible_02[i],5,LH,LH,fwfmi);
						}
					}
					if(MAXHITS==Hits) break;
				}

				if(MAXHITS==Hits) continue;
			}
			if(SUPERACCURATE)
			{
				Range.Start=1;Range.End=SOURCELENGTH;Range.Mismatches=0;Range.Level=1;
				Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
				TRange=Range;
				if(USEQUALITY)
				{
					Do_Branch=Low_Quality;
					Search_X11(Range,1,STRINGLENGTH,RHQL,fwfmi);//finds mismatches of the form 112, stores possibles 113 and in the left half tag,
				}

				Do_Branch=Do_All;
				Search_X11(TRange,1,STRINGLENGTH,RHQL,fwfmi);//finds mismatches of the form 112, stores possibles 113 and in the left half tag,
			}		

//}------------------------------------------- FIVE MISMATCH ---------------------------------------------------------------------------------------------
			if(PRINT_MISHITS && !Hits)
			{
				for (unsigned i=0;i<=STRINGLENGTH-1;i++)//convert and write fasta
				{
					Current_Tag[i]=Code_To_Char[Current_Tag[i]];
				}
				Current_Tag[STRINGLENGTH]=0;
				fprintf(Mishit_File,"%s%s\n+\n%s", Description,Current_Tag,Quality);
			}
			
		}
	}
	if(HITMODE == DEFAULT)
	{
		Record.Start=0;
		fwrite(&Record,sizeof(Record),1,Output_File);//write sentinel..
	}
	else
	{
		char End_Mark='&';
		fwrite(&End_Mark,1,1,Output_File);//write sentinel..
	}	

	printf("\r[++++++++100%%+++++++++]\n");//progress bar....
	if(NORMAL_TAGS) printf("Total Tags/Hits : %d/%d\t Tags parsed : %d\n",Total_Tags,Total_Hits,Actual_Tag); else printf("Total Tags/Hits : %d/%d\t Tags parsed : %d [%d]\n",Total_Tags,Total_Hits,Actual_Tag,Actual_Tag/2);
	time(&End_Time);printf("\n Time Taken  - %.0lf Seconds ..\n ",difftime(End_Time,Start_Time));



}

char Guess_Orientation()
{
	int Complement_Length;
	int Org_Length;
	SARange Range;
	FMIndex=REVERSE;

//-------------------------------------------------- NORMAL SCAN --------------------------------------------------------------------
	Exact_Match_Forward=Exact_Match_ForwardF;
	Current_Tag=Original+IGNOREHEAD;
	Direction = 0;//forward scn first...
	DIRECTIONS=2;

	if(LOOKUPSIZE ==3)
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}
	else
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}

	Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
	Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
	Search_Forwards_Exact(Range,-1,STRINGLENGTH,revfmi);//Find exact matches and report... if not found get the range for 0|?
	if(Hits) 
	{
		if (MAX_MISMATCHES != 0) return FALSE;
		if (!ROLLOVER ) return FALSE;
	}
	Org_Length=Range.Level;
//--------------------------------------------------REVERSE COMPLEMENT SCAN --------------------------------------------------------------------
	Exact_Match_Forward=Exact_Match_ForwardC;
	Current_Tag=Complement;
	Direction =1;//reverse complement scan first...
	DIRECTIONS=3;

	if(LOOKUPSIZE ==3)
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}
	else
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}

	Range.Start=Forward_Start_LookupX[c];Range.End=Forward_End_LookupX[c];Range.Mismatches=0;Range.Level=LOOKUPSIZE+1;Range.Tag=Actual_Tag;
	Range.Mismatch_Pos=0;Range.Mismatch_Char=0;
	Search_Forwards_Exact(Range,-1,STRINGLENGTH,revfmi);//Find exact matches and report... if not found get the range for 0|?
	if(Hits || MAX_MISMATCHES == 0) return FALSE;
	Complement_Length=Range.Level;
//-------------------------------------------------- EXACT SCAN END --------------------------------------------------------------------
	if (Complement_Length > Org_Length)//first try the complement
	{
		Direction =1;
		DIRECTIONS=3;
	}
	else
	{
		if (FILTER_AMBIGUOUS && Complement_Length == Org_Length )
		{
			Current_Tag=Original+IGNOREHEAD;
			for (int i=0;i<=STRINGLENGTH-1;i++)//convert and write fasta
			{
				Current_Tag[i]=Code_To_Char[Current_Tag[i]];
			}
			Current_Tag[STRINGLENGTH]=0;
			fprintf(Ambiguous_File,"%s%s\n+\n%s", Description,Current_Tag,Quality);
			return FALSE;
		}
		Current_Tag=Original+IGNOREHEAD;
		Exact_Match_Forward=Exact_Match_ForwardF;
		Direction = 0;
		DIRECTIONS=2;
	}
	return TRUE;

}


void Build_Preindex_Backward(Range Range, int Level, int Bit)
{

	if (LOOKUPSIZE==Level) 
	{
		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label
		Backward_Start_LookupX[Range.Label] = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.Start , Bit) + 1;
		Backward_End_LookupX[Range.Label] = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.End+1, Bit);
	}
	else
	{


		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label 
		Range.Start = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.Start , Bit) + 1;
		Range.End = fwfmi->cumulativeFreq[Bit] + BWTOccValue(fwfmi, Range.End+1, Bit);
		Level ++;
		for ( int i=0;i<4;i++)
		{
			Build_Preindex_Backward( Range, Level,i);
		}

	}

}

void Build_Preindex_Forward(Range Range, int Level, int Bit)
{

	if (LOOKUPSIZE==Level) 
	{
		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label
		Forward_Start_LookupX[Range.Label] = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.Start , Bit) + 1;
		Forward_End_LookupX[Range.Label] = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.End+1, Bit);
	}
	else
	{


		Range.Label=Range.Label | (Bit<<2*(Level-1));//Calculate label 
		Range.Start = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.Start , Bit) + 1;
		Range.End = revfmi->cumulativeFreq[Bit] + BWTOccValue(revfmi, Range.End+1, Bit);
		Level ++;
		for ( int i=0;i<4;i++)
		{
			Build_Preindex_Forward( Range, Level,i);
		}

	}

}

//{-----------------------------  BACKWARD SEARCH ROUTINE  -------------------------------------------------/

void Search_Backwards_Exact_X0(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	
	if (!Tag.Start) return;
	for(;;)
	{
		Get_SARange_Fast(Current_Tag[Start-Tag.Level],Tag,fmi);
		if (Tag.Start!=0)
		{
			if(Tag.Level== StringLength)
			{
				return;
			}
			else Tag.Level++;
		} 
		else
		{
			return;//No hit
		}
	}
}

void Search_10X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	
	if (!Tag.Start) return;
	struct SARange Range,Temp_Range=Tag;
	int BMHStack_Top=0;
	BMHStack[0]=Tag;
	while(BMHStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMHStack[BMHStack_Top];
		BMHStack_Top--;	//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_10X_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start , Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)//only one mismatch allowed here...
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)//a tag of the form ?|1|0
							{

								Backwards(Temp_Range,1,LH);
								if (Temp_Range.Start)
								{
									Temp_Range.Level=1;
									Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
									memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
									Search_Forwards(Temp_Range,3,LH+1,RH,revfmi);
									if(MAXHITS==Hits) return;
									Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
									memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
								}

							}
							else continue;
						}
						else
						{
							BMHStack_Top++;//Push range
							Temp_Range.Level++;
							BMHStack[BMHStack_Top]=Temp_Range;
						}
					}
					else //store mismatches for later use...
					{
						if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
						Right_Mishits[Right_Mishits_Pointer]=Temp_Range;
						Right_Mishits_Pointer++;
					}
					
				} 
			}
		}
	}
	return;
}

void Search_10X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{

	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= FWDInverseSA0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start-Tag.Level] && Now != Current_Tag[Start-Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start-Tag.Level] != Now)
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)//a tag of the form ?|1|0 , remove zero mismatch
				{
					Backwards(Tag,1,LH);
					if(Tag.Start)
					{
						Tag.End=Tag.Start;
						Tag.Level=1;
						Search_Forwards(Tag,3,LH+1,RH,revfmi);
					}
					return;

				}
				else return;
			}
			else { Tag.Level++;continue; }
		} 
		else //store mismatches for later use...
		{
			Tag.End=Tag.Start;
			if (Tag.Level!=StringLength) Tag.Level++; 
			Right_Mishits[Right_Mishits_Pointer]=Tag;
			Right_Mishits_Pointer++;
			return;
		}
	}
}

void Search_Backwards_X10(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	int BMHStack_Top=0;
	BMHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMHStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMHStack[BMHStack_Top];
		BMHStack_Top--;	//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Backwards_X10_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start , Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)//only one mismatch allowed here...
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)//a tag of the form ?|1|0
							{
								Temp_Range.Level=1;
								Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
								memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
								Search_Backwards(Temp_Range,2,LH,LH,fwfmi);
								if(MAXHITS==Hits) return;
								Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
								memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
							}
							else continue;
						}
						else
						{
							BMHStack_Top++;//Push range
							Temp_Range.Level++;
							BMHStack[BMHStack_Top]=Temp_Range;
						}
					}
					else
					{
						Temp_Range.Level++; 
						Possible_20[Possible_20_Pointer]=Temp_Range;
						Possible_20_Pointer++;
					}
				} 
			}
		}
	}
	return;
}

void Search_Backwards_X10_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{

	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= FWDInverseSA0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start-Tag.Level] && Now != Current_Tag[Start-Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start-Tag.Level] != Now)//only one mismatch allowed here...
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)//a tag of the form ?|1|0 , remove zero mismatch
				{
					Tag.End=Tag.Start;
					Tag.Level=1;
					Search_Backwards(Tag,2,LH,LH,fwfmi);
					return;
				}
				else return;
			}
			else { Tag.Level++;continue; }
		} 
		else
		{
			Tag.End=Tag.Start;
			if (Tag.Level!=StringLength) Tag.Level++; 
			Possible_20[Possible_20_Pointer]=Tag;
			Possible_20_Pointer++;
			return;
		}
	}
}
//}-----------------------------  BACKWARD SEARCH ROUTINE -------------------------------------------------/

//{-----------------------------  FORWARD SEARCH ROUTINES  -------------------------------------------------/
void Search_Forwards_0X(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{

	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offsets...
	for(;;)	
	{
		Get_SARange_Fast(Current_Tag[Start+Tag.Level],Tag,fmi);
		if (Tag.Start!=0)
		{
			if(Tag.Level== StringLength)
			{
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			return;
		}
	}
}

void Search_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	int BMStack_Top=0;
	BMStack_X11[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMStack_X11[BMStack_Top];
		BMStack_Top--;	//Pop the range
		Branch_Detect_Backwards(Range,fmi,Start);

		for(int Branch=0;Branch<4;Branch++)
		{
			if (Branch_Characters[Branch])//This character actually branches
			{
				Temp_Range=Range;//adjust
				Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
				Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

				if (Current_Tag[Start-Temp_Range.Level] != Branch)
				{
					Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
					Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
					Temp_Range.Mismatches++;
				}

				if (Temp_Range.Mismatches<=1)//we are guaranteed a valid SA range, check only for mismatches
				{
					if(Temp_Range.Level== StringLength)
					{
						if(Temp_Range.Mismatches)
						{
							Temp_Range.Level++;
							Temp_BC2[0]=Branch_Characters[0];Temp_BC2[1]=Branch_Characters[1];Temp_BC2[2]=Branch_Characters[2];Temp_BC2[3]=Branch_Characters[3];
							memcpy(Temp_Branch_Ranges2,Branch_Ranges,4*sizeof(SARange));//X|8
							Search_Half_Tag_X11(Temp_Range,2,STRINGLENGTH,RH,fwfmi);
							if(MAXHITS==Hits) return;
							memcpy(Branch_Ranges,Temp_Branch_Ranges2,4*sizeof(SARange));
							Branch_Characters[0]=Temp_BC2[0];Branch_Characters[1]=Temp_BC2[1];Branch_Characters[2]=Temp_BC2[2];Branch_Characters[3]=Temp_BC2[3];

						}
						else continue;
					}
					else
					{
						BMStack_Top++;//Push range
						Temp_Range.Level++;
						BMStack_X11[BMStack_Top]=Temp_Range;
					}
				}
			} 
		}
	}
	return;
}

void Search_Half_Tag_X11_OneSA(struct SARange & Tag,char* Current_Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if (!Do_Branch[Start-Tag.Level] && Current_Tag[Start-Tag.Level]!=Now) return;  
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;

		if (Current_Tag[Start-Tag.Level] != Now)
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength && Tag.Mismatches==2)
			{
				Tag.End=Tag.Start;
				Tag.Level=1;
				Search_Backwards(Tag,5,LH,LH,fwfmi);//LH,fwfmi);
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else return;
	}
}

void Search_Half_Tag_X11(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	int BMStack_Top=0;
	BMStack_X11H[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMStack_X11H[BMStack_Top];
		BMStack_Top--;	//Pop the range
		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Half_Tag_X11_OneSA(Range,Current_Tag,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=2)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength && Temp_Range.Mismatches == Count)
						{
							Temp_Range.Level=1;
							Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
							memcpy(Temp_Branch_Ranges2,Branch_Ranges,4*sizeof(SARange));//X|16
							Search_Backwards(Temp_Range,5,LH,LH,fwfmi);//LH,fwfmi);
							if(MAXHITS==Hits) return;
							memcpy(Branch_Ranges,Temp_Branch_Ranges2,4*sizeof(SARange));
							Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
						}
						else
						{
							BMStack_Top++;//Push range
							Temp_Range.Level++;
							BMStack_X11H[BMStack_Top]=Temp_Range;
						}
					}
				} 
			}
		}
	}
	return;
}

void Search_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)////////////////
{

	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStackX0X[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStackX0X[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		Branch_Detect(Range,revfmi,Start);
		for(int Branch=0;Branch<4;Branch++)
		{
			if (Branch_Characters[Branch])//This character actually branches
			{
				Temp_Range=Range;
				Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
				Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

				if (Current_Tag[Temp_Range.Level+Start]!=Branch)
				{
					Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
					Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
					Temp_Range.Mismatches++;
				}

				if (Temp_Range.Mismatches<=1)//we are guaranteed a valid SA range, check only for mismatches
				{
					if(Temp_Range.Level== StringLength)
					{
						Temp_Range.Level=1;
						Temp_BC2[0]=Branch_Characters[0];Temp_BC2[1]=Branch_Characters[1];Temp_BC2[2]=Branch_Characters[2];Temp_BC2[3]=Branch_Characters[3];
						memcpy(Temp_Branch_Ranges2,Branch_Ranges,4*sizeof(SARange));
						Search_Half_Tag_11X(Temp_Range,2,LHQL +1,LHQR,revfmi);
						if(MAXHITS==Hits) return;
						memcpy(Branch_Ranges,Temp_Branch_Ranges2,4*sizeof(SARange));
						Branch_Characters[0]=Temp_BC2[0];Branch_Characters[1]=Temp_BC2[1];Branch_Characters[2]=Temp_BC2[2];Branch_Characters[3]=Temp_BC2[3];
					}
					else
					{
						FSHStack_Top++;//Push range
						Temp_Range.Level++;
						FSHStackX0X[FSHStack_Top]=Temp_Range;
					}
				}
			} 
		}

	}
	return;
}

void Search_Half_Tag_11X_OneSA(struct SARange & Tag,char* Current_Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start+Tag.Level] && Now != Current_Tag[Start+Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start+Tag.Level] != Now) 
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}
		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength && Tag.Mismatches==Count)
			{
				Tag.End=Tag.Start;
				Tag.Level=1;
				Search_Forwards(Tag,4,LH+1,RH,revfmi);
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			Tag.End=Tag.Start;
			Tag.Level=LHQR+Tag.Level+1;
			Left_Mishits[Left_Mishits_Pointer]=Tag;
			Left_Mishits_Pointer++;
			return;
		}
	}
}

void Search_Half_Tag_11X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStack[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Half_Tag_11X_OneSA(Range,Current_Tag,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;
					Temp_Range.End = Branch_Ranges[Branch].End;

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength && Temp_Range.Mismatches==Count)
						{
							Temp_Range.Level=1;
							Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
							memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
							Search_Forwards(Temp_Range,4,LH+1,RH,revfmi);
							if(MAXHITS==Hits) return;
							memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
							Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStack[FSHStack_Top]=Temp_Range;
						}
					}
					else //store mismatches for later use...
					{
						Temp_Range.Level=LHQR+Temp_Range.Level+1;
						Left_Mishits[Left_Mishits_Pointer]=Temp_Range;
						Left_Mishits_Pointer++;
					}
				} 
			}

		}
	}
	return;
}

void Search_01X(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStack[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_01X_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;
					Temp_Range.End = Branch_Ranges[Branch].End;

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)
							{
								Temp_Range.Level=1;
								Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
								memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
								Search_Forwards(Temp_Range,3,LH+1,RH,revfmi);
								memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
								if(MAXHITS==Hits) return;
								Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
							}
							else continue;
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStack[FSHStack_Top]=Temp_Range;
						}
					}
				} 
			}

		}
	}
	return;
}

void Search_01X_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start+Tag.Level] && Now != Current_Tag[Start+Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start+Tag.Level] != Now) 
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}
		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)
				{
					Tag.End=Tag.Start;
					Tag.Level=1;
					Search_Forwards(Tag,3,LH+1,RH,revfmi);
					return;
				}
				else return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			return;
		}
	}
}

void Search_X01(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offset difference
	int FSHStack_Top=0;
	FSHStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSHStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSHStack[FSHStack_Top];
		FSHStack_Top--;		//Pop the range

		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_X01_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches)
							{
								Reverse(Temp_Range,STRINGLENGTH,RH);
								if(Temp_Range.Start)
								{
									Temp_Range.Level=1;
									Temp_BC1[0]=Branch_Characters[0];Temp_BC1[1]=Branch_Characters[1];Temp_BC1[2]=Branch_Characters[2];Temp_BC1[3]=Branch_Characters[3];
									Search_Backwards(Temp_Range,2,LH,LH,fwfmi);
									if(MAXHITS==Hits) return;
									Branch_Characters[0]=Temp_BC1[0];Branch_Characters[1]=Temp_BC1[1];Branch_Characters[2]=Temp_BC1[2];Branch_Characters[3]=Temp_BC1[3];
								}
							}
							else continue;
						}
						else
						{
							FSHStack_Top++;//Push range
							Temp_Range.Level++;
							FSHStack[FSHStack_Top]=Temp_Range;
						}
					}
					else
					{
						if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
						Possible_02[Possible_02_Pointer]=Temp_Range;
						Possible_02_Pointer++;
					}
				} 
			}

		}
	}
	return;
}

void Search_X01_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if ( !Do_Branch[Start+Tag.Level] && Now != Current_Tag[Start+Tag.Level]) return; //do not bend these nuces...
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Start+Tag.Level] != Now) 
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		}
		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches)
				{
					Tag.End=Tag.Start;
					Reverse(Tag,STRINGLENGTH,RH);
					if(Tag.Start)
					{
						Tag.Level=1;
						Search_Backwards(Tag,2,LH,LH,fwfmi);
					}
					return;
				}
				else return;
			}
			else {Tag.Level++;continue;}
		} 
		else
		{
			Tag.End=Tag.Start;
			if (Tag.Level!=StringLength) Tag.Level++; 
			Possible_02[Possible_02_Pointer]=Tag;
			Possible_02_Pointer++;
			return;
		}
	}
}

void Backwards(struct SARange & Tag,int Start,int StringLength)
{	

	char Temp=0;
	char Mismatch_Count=Tag.Mismatches;
	int Temp_Pos=0;//New_Char;
	int pos;
	Start=Start-2;
	
	for( int i=0;i<Mismatch_Count;i++)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Temp=Temp | (Current_Tag[pos]<<i*2);
		Current_Tag[pos]=Tag.Mismatch_Char>>(2*i) & 3;
	}

	Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
	memcpy(Temp_Branch_Ranges,Branch_Ranges,4*sizeof(SARange));
	{
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
		}
		Tag.Start=Forward_Start_LookupX[c];Tag.End=Forward_End_LookupX[c];
		Tag.Level=LOOKUPSIZE + 1;
		Search_Exact(Tag,Start,StringLength,revfmi);
	}
	Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
	memcpy(Branch_Ranges,Temp_Branch_Ranges,4*sizeof(SARange));
	for( int i=0;i<Tag.Mismatches;i++)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Current_Tag[pos]=(Temp>>(2*i)) & 3;
	}
	return;
}

void Reverse(struct SARange & Tag,int Start,int StringLength)
{	
	char Temp=0;
	char New_Char;
	char Mismatch_Count=Tag.Mismatches;
	unsigned pos;
	for( int i=0;i<Mismatch_Count;i++)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Temp=Temp | (Current_Tag[pos]<<i*2);
		Current_Tag[pos]=Tag.Mismatch_Char>>(2*i) & 3;
	}
	Temp_BC[0]=Branch_Characters[0];Temp_BC[1]=Branch_Characters[1];Temp_BC[2]=Branch_Characters[2];Temp_BC[3]=Branch_Characters[3];
	{
		if(LOOKUPSIZE==3)
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4);// | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		else
		{
			c=Current_Tag[STRINGLENGTH-1-0] | (Current_Tag[STRINGLENGTH-1-1]<<2) | (Current_Tag[STRINGLENGTH-1-2]<<4) | (Current_Tag[STRINGLENGTH-1-3]<<6) | Current_Tag[STRINGLENGTH-1-4]<<8 | (Current_Tag[STRINGLENGTH-1-5]<<10);//Use lookup table...
		}
		Tag.Start=Backward_Start_LookupX[c];Tag.End=Backward_End_LookupX[c];
		Tag.Level=LOOKUPSIZE + 1;
		Search_Backwards_Exact( Tag,STRINGLENGTH,RH,fwfmi);//Backward scan for ?|0
	}
	Branch_Characters[0]=Temp_BC[0];Branch_Characters[1]=Temp_BC[1];Branch_Characters[2]=Temp_BC[2];Branch_Characters[3]=Temp_BC[3];
	for( int i=0;i<Tag.Mismatches;i++)
	{
		pos=63 & (Tag.Mismatch_Pos>>(6*i));
		Current_Tag[pos]=(Temp>>(2*i)) & 3;
	}
	return;
}

void Search_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	int Level;
	unsigned long Index,Now,First,Last;
	if (!Tag.Start) return;

	for(;;)	
	{
		if(Tag.End==Tag.Start)//Only one branch?
		{
			Level=Tag.Level;
			for(;;)
			{
				Index=Tag.Start;
				if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
				Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
				if (Current_Tag[Start+Level] == Now)
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End=Tag.Start;
					if(Level== StringLength)
					{
						return;	
					}
					else {Level++;continue;}
				} 
				else//mismatch...
				{
					Tag.Start=0;//Tag.End=0;
					return;	
				}
			}
		}
		else//SA range has sevaral possible hits... 
		{
			if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
			{
				Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

				if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
				for (unsigned long Pos=First;Pos<=Last;Pos++)
				{
					Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
					Branch_Characters[Now]++;	
				}

				Now=Current_Tag[Tag.Level+Start];
				if (Branch_Characters[Now])//we have a match... 
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End = Tag.Start + Branch_Characters[Now]-1;// Calculate SAranges
				}
				else//mismatch..
				{
					Tag.Start=0;
				}
			} 
			else
			{
				Get_SARange_Fast(Current_Tag[Start+Tag.Level],Tag,fmi);
			}

			if (Tag.Start!=0)
			{
				if(Tag.Level== StringLength)
				{
					return;
				}
				else {Tag.Level++;continue;}
			} 
			else//Mismatch
			{
				return;
			}

		}
	}
}

void Search_Forwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	int Level;
	Exact_Match_Forward[Start+LH].Start=0;
	unsigned long Index,Now,First,Last;
	for(;;)	
	{
		if(Tag.End==Tag.Start)//Only one branch?
		{
			//Level=Tag.Level;
			for(;;)
			{
				Index=Tag.Start;
				if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
				Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
				if (Current_Tag[Start+Tag.Level] == Now)
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End=Tag.Start;
					Exact_Match_Forward[Start+Tag.Level]=Tag;
					if(Tag.Level== StringLength)
					{
						Print_LocationX(Tag);
						return;	
					}
					else {Tag.Level++;continue;}
				} 
				else//mismatch...
				{
					Exact_Match_Forward[0]=Tag;//save old location for heuristics...
					Tag.Start=0;//Tag.End=0;
					Exact_Match_Forward[Start+Tag.Level]=Tag;
					return;	
				}
			}
		}
		else//SA range has sevaral possible hits... 
		{
			if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
			{
				Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

				if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
				for (unsigned long Pos=First;Pos<=Last;Pos++)
				{
					Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
					Branch_Characters[Now]++;	
				}

				Now=Current_Tag[Tag.Level+Start];
				if (Branch_Characters[Now])//we have a match... 
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End = Tag.Start + Branch_Characters[Now]-1;// Calculate SAranges
				}
				else//mismatch..
				{
					Exact_Match_Forward[0]=Tag;//save old location for heuristics...
					Tag.Start=0;
				}
			} 
			else
			{
				Exact_Match_Forward[0]=Tag;//save old location for heuristics...
				Get_SARange_Fast(Current_Tag[Start+Tag.Level],Tag,fmi);
			}

			Exact_Match_Forward[Tag.Level+Start]=Tag;
			if (Tag.Start!=0)
			{
				if(Tag.Level== StringLength)
				{
					Print_LocationX(Tag);
					return;
				}
				else {Tag.Level++;continue;}
			} 
			else//Mismatch
			{
				Exact_Match_Forward[Start+Tag.Level]=Tag;
				return;
			}

		}
	}
}

void Search_Forwards(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offset difference
	int FSStack_Top=0;
	FSSStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSSStack[FSStack_Top];
		FSStack_Top--;		//Pop the range
		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Forwards_OneSA(Range,Count,Start,StringLength,revfmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect(Range,revfmi,Start);//One_Branch(Range,revfmi);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Temp_Range.Level+Start]!=Branch)
					{

						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;

					}


					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if (Temp_Range.Mismatches == Count) //dont print exact matches
							{
								Print_LocationX(Temp_Range);
								if (MAXHITS==Hits) return;
							}
							else continue;
						}
						else 
						{

							FSStack_Top++;//Push range
							Temp_Range.Level++;
							FSSStack[FSStack_Top]=Temp_Range;
						}
					}
					else
					{
						if(MAX_MISMATCHES !=Count)//store only for one mismatch... and last node will not branch
						{
							if (Temp_Range.Level!=StringLength) Temp_Range.Level++; 
							else //2 mismatches with the last at the end...
							{
								Two_Mismatches_At_End_Forward[Two_Mismatches_At_End_Forward_Pointer]=Temp_Range;
								Two_Mismatches_At_End_Forward_Pointer++;
								continue;
							}
							Mismatches_Forward[Mismatches_Forward_Pointer]=Temp_Range;
							Mismatches_Forward_Pointer++;
						}
						continue;
					}
				} 
			}
		}
	}
	return;
}

void Search_Forwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);
		if (!Do_Branch[Tag.Level+Start] && Current_Tag[Tag.Level+Start]!=Now) return;  
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
		if (Current_Tag[Tag.Level+Start]!=Now)
		{

			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start+Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
			
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches==Count)//avoid printing exact matches...
				{
					Tag.End=Tag.Start;
					Print_LocationX(Tag);
				}
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else//log 2 mismatches 
		{
			if(MAX_MISMATCHES !=Count)//store only for one mismatch... later report these on a seperate stack..
			{
				Tag.End=Tag.Start;//possibly two mismatch exists..
				if (Tag.Level != StringLength) Tag.Level++; 
				else //2 mismatches occuring in last position...
				{
					Two_Mismatches_At_End_Forward[Two_Mismatches_At_End_Forward_Pointer]=Tag;
					Two_Mismatches_At_End_Forward_Pointer++;
					return;
				}

				Mismatches_Forward[Mismatches_Forward_Pointer]=Tag;
				Mismatches_Forward_Pointer++;
			}
			return;
		}
	}
}

void Search_Forwards_Indel(const struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	Start=Start-2;//Adjust for offset difference
	SARange TRange;
	int FSStack_Top=0;
	FSSStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(FSStack_Top!=-1)//While Stack non-empty....
	{
		Range=FSSStack[FSStack_Top];
		FSStack_Top--;		//Pop the range

		if((Range.Mismatch_Pos>>6 & 63) !=63)//No indels?
		{
			TRange=Range;
			TRange.Mismatch_Pos=TRange.Mismatch_Pos | (63<<6);//indicate indel
			TRange.Mismatch_Pos=TRange.Mismatch_Pos | ((Start+TRange.Level)<<2*6);//indicate position
			FSStack_Top++;
			TRange.Level=TRange.Level+1;
			FSSStack[FSStack_Top]=TRange;
		}

		Branch_Detect(Range,revfmi,Start);//One_Branch(Range,revfmi);

		for(int Branch=0;Branch<4;Branch++)
		{
			if (Branch_Characters[Branch])//This character actually branches
			{
				Temp_Range=Range;
				Temp_Range.Start = Branch_Ranges[Branch].Start;
				Temp_Range.End = Branch_Ranges[Branch].End;
				if((Temp_Range.Mismatch_Pos>>6 & 63) !=63)//No indels?
				{
					TRange=Temp_Range;
					TRange.Mismatch_Pos=TRange.Mismatch_Pos | (63<<6);//indicate indel
					TRange.Mismatch_Pos=TRange.Mismatch_Pos | (63<<2*6);//indicate insertl
					TRange.Mismatch_Pos=TRange.Mismatch_Pos | ((Start+TRange.Level)<<3*6);//indicate position
					TRange.Mismatch_Char=TRange.Mismatch_Char | (Branch<<2*2);
					FSStack_Top++;
					FSSStack[FSStack_Top]=TRange;
				}
				if (Current_Tag[Temp_Range.Level+Start]!=Branch)
				{

					Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
					Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start+Temp_Range.Level)<<Temp_Range.Mismatches*6);
					Temp_Range.Mismatches++;

				}


				if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
				{
					if(Temp_Range.Level== StringLength && Temp_Range.Mismatch_Pos)
					{
						Print_LocationX(Temp_Range);
						if (MAXHITS==Hits) return;
					}
					else 
					{
						

						FSStack_Top++;//Push range
						Temp_Range.Level++;
						FSSStack[FSStack_Top]=Temp_Range;
					}
				}
			} 
		}
	}
	return;
}


void One_Branch(struct SARange Tag,BWT *fmi)
{
	unsigned Last, First;
	char Now;
	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

	if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
	for (unsigned long Pos=First;Pos<=Last;Pos++)
	{
		Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
		Branch_Characters[Now]++;	
	}

}


void Branch_Detect (const struct SARange Tag,BWT *fmi,int Start)
{

	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;
	if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
	{
		unsigned Last, First;
		char Now;

		if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 

		for (unsigned long Pos=First;Pos<=Last;Pos++)
		{
			Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
			Branch_Characters[Now]++;	
		}

		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Tag.Level+Start] && Branch != Current_Tag[Start+Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else if (Branch_Characters[Branch])
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = Branch_Ranges[Branch].Start + Branch_Characters[Branch]-1;// Calculate SAranges
			}
		}
	}
	else
	{
		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Tag.Level+Start] && Branch != Current_Tag[Start+Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.End+1, Branch);
				if(!(Branch_Ranges[Branch].End<Branch_Ranges[Branch].Start)) Branch_Characters[Branch]=1;
			}
		}

	}
}

void Branch_Detect_Backwards (const struct SARange Tag,BWT *fmi,int Start)
{

	Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;
	if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
	{
		unsigned Last, First;
		char Now;

		if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 

		for (unsigned long Pos=First;Pos<=Last;Pos++)
		{
			Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
			Branch_Characters[Now]++;	
		}

		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Start-Tag.Level] && Branch != Current_Tag[Start-Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else if (Branch_Characters[Branch])
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = Branch_Ranges[Branch].Start + Branch_Characters[Branch]-1;// Calculate SAranges
			}
		}
	}
	else
	{
		for (int Branch=0;Branch<4;Branch++)
		{
			if ( !Do_Branch[Start-Tag.Level] && Branch != Current_Tag[Start-Tag.Level]) 
			{
				Branch_Characters[Branch]=0; //do not bend these nuces...
			}
			else
			{
				Branch_Ranges[Branch].Start = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.Start, Branch) + 1;
				Branch_Ranges[Branch].End = fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Tag.End+1, Branch);
				if(!(Branch_Ranges[Branch].End<Branch_Ranges[Branch].Start)) Branch_Characters[Branch]=1;
			}
		}

	}
}
//}-----------------------------  FORWARD SEARCH ROUTINE  -------------------------------------------------/

//{-----------------------------  BACKWARD SEARCH ROUTINE  -------------------------------------------------/

void Search_Backwards_Exact(struct SARange & Tag,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	int Level;
	unsigned long Index,Now,First,Last;
	for(;;)	
	{
		if(Tag.End==Tag.Start)//Only one branch?
		{
			Level=Tag.Level;
			for(;;)
			{
				Index=Tag.Start;
				if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
				Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
				if (Current_Tag[Start-Level] == Now)
				{
					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End=Tag.Start;
					Exact_Match_Backward[Level]=Tag;
					if(Level== StringLength)//no need to print as we are still halfway..
					{
						return;	
					}
					else {Level++;continue;}
				} 
				else
				{
					Tag.Start=0;//Tag.End=0;
					return;	
				}
			}
		}
		else 
		{
			Exact_Match_Backward[Tag.Level]=Tag;
			if(Tag.End-Tag.Start<BRANCHTHRESHOLD)//only small number of branches
			{
				Branch_Characters[0]=0;Branch_Characters[1]=0;Branch_Characters[2]=0;Branch_Characters[3]=0;

				if (Tag.Start+1 >= fmi->inverseSa0) {First=Tag.Start;Last=Tag.End;} else {First=Tag.Start+1;Last=Tag.End+1;} 
				for (unsigned long Pos=First;Pos<=Last;Pos++)
				{
					Now=fmi->bwtCode[(Pos-1) / 16] << (((Pos-1) % 16) * 2)>> (BITS_IN_WORD - 2);
					Branch_Characters[Now]++;	
				}

				Now=Current_Tag[Start-Tag.Level];
				if (Branch_Characters[Now])//we have a match... 
				{

					Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;
					Tag.End = Tag.Start + Branch_Characters[Now]-1;// Calculate SAranges
				
				}
				else
				{
					Tag.Start=0;//Tag.End=0;
				}
			}
			else Get_SARange_Fast(Current_Tag[Start-Tag.Level],Tag,fmi);

			if (Tag.Start!=0)
			{
				if(Tag.Level== StringLength)
				{
					return;
				}
				else {Tag.Level++;continue;}
			} 
			else
			{
				return;
			}

		}
	}
}

//}-----------------------------  BACKWARD SEARCH ROUTINE  -------------------------------------------------/

void Search_Backwards(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	if (!Tag.Start) return;
	int BMStack_Top=0;
	BMStack[0]=Tag;
	struct SARange Range,Temp_Range;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{
		Range=BMStack[BMStack_Top];
		BMStack_Top--;	//Pop the range
		if (Range.End==Range.Start)//does this SArange have only one branch?
		{
			Search_Backwards_OneSA(Range,Count,Start,StringLength,fmi);
			if(MAXHITS==Hits) return;
		}
		else
		{
			Branch_Detect_Backwards(Range,fmi,Start);
			for(int Branch=0;Branch<4;Branch++)
			{
				if (Branch_Characters[Branch])//This character actually branches
				{
					Temp_Range=Range;//adjust
					Temp_Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Temp_Range.Start, Branch) + 1;
					Temp_Range.End = Branch_Ranges[Branch].End;//Temp_Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

					if (Current_Tag[Start-Temp_Range.Level] != Branch)
					{
						Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
						Temp_Range.Mismatch_Pos=Temp_Range.Mismatch_Pos | ((Start-Temp_Range.Level)<<Temp_Range.Mismatches*6);
						Temp_Range.Mismatches++;
					}

					if (Temp_Range.Mismatches<=Count)//we are guaranteed a valid SA range, check only for mismatches
					{
						if(Temp_Range.Level== StringLength)
						{
							if(Temp_Range.Mismatches==Count)
							{
								Print_LocationX(Temp_Range);
								if(MAXHITS==Hits) return;
							}
							else continue;
						}
						else
						{
							BMStack_Top++;//Push range
							Temp_Range.Level++;
							BMStack[BMStack_Top]=Temp_Range;
						}
					}
					else 
					{
						if(MAX_MISMATCHES !=Count)// 2 mismatches...
						{
							if(Temp_Range.Level != StringLength) Temp_Range.Level++;
							//if((Start-Temp_Range.Level) != 0) Temp_Range.Level++;
							else // 2 mismatches with the last at the end?
							{
								Two_Mismatches_At_End[Two_Mismatches_At_End_Pointer]=Temp_Range;
								Two_Mismatches_At_End_Pointer++;
								continue;
							}
							Mismatches_Backward[Mismatches_Backward_Pointer]=Temp_Range;
							Mismatches_Backward_Pointer++;
						}
						continue;
					}
				} 
			}
		}
	}
	return;
}

void Search_Backwards_Indel(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{

	if (!Tag.Start) return;
	int BMStack_Top=0;
	int Branch;
	BMStack[0]=Tag;
	struct SARange Range,Temp_Range,TRange;
	while(BMStack_Top!=-1)//While Stack non-empty....
	{

		Range=BMStack[BMStack_Top];
		BMStack_Top--;	//Pop the range

		if((Range.Mismatch_Pos>>6 & 63) !=63)//No indels?
		{
			//Temp_Range.Mismatch_Char=Temp_Range.Mismatch_Char | (Branch<<Temp_Range.Mismatches*2);
			TRange=Range;
			TRange.Mismatch_Pos=TRange.Mismatch_Pos | (63<<6);//indicate indel
			TRange.Mismatch_Pos=TRange.Mismatch_Pos | ((Start-TRange.Level)<<2*6);//indicate position
			BMStack_Top++;
			TRange.Level=TRange.Level+1;
			BMStack[BMStack_Top]=TRange;
		}

		Branch_Detect_Backwards(Range,fmi,Start);
		{
			for (Branch=0;Branch<4;Branch++)
			{
				if(Branch_Characters[Branch] && (Range.Mismatch_Pos>>6 & 63) !=63)//No indels?
				{
					TRange=Range;
					TRange.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Range.Start, Branch) + 1;
					TRange.End = Branch_Ranges[Branch].End;//Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges
					TRange.Mismatch_Pos=TRange.Mismatch_Pos | (63<<6);//indicate indel
					TRange.Mismatch_Pos=TRange.Mismatch_Pos | (63<<2*6);//indicate insertl
					TRange.Mismatch_Pos=TRange.Mismatch_Pos | ((Start-TRange.Level+1)<<3*6);//indicate position
					TRange.Mismatch_Char=TRange.Mismatch_Char | (Branch<<2*2);
					BMStack_Top++;
					BMStack[BMStack_Top]=TRange;
				}
			}

			Branch=Current_Tag[Start-Range.Level];
			if (Branch_Characters[Branch])//This character actually branches
			{
				
				Range.Start = Branch_Ranges[Branch].Start;//fmi->cumulativeFreq[Branch] + BWTOccValue(fmi, Range.Start, Branch) + 1;
				Range.End = Branch_Ranges[Branch].End;//Range.Start + Branch_Characters[Branch]-1;// Calculate SAranges

				

				if(Range.Level== StringLength && Range.Mismatch_Pos)
				{
					Print_LocationX(Range);
					if(MAXHITS==Hits) return;
				}
				else
				{
					BMStack_Top++;//Push range
					Range.Level++;
					BMStack[BMStack_Top]=Range;
				}
			} 
		}
	}
	return;

}

void Search_Backwards_OneSA(struct SARange & Tag,int Count,int Start,int StringLength,BWT *fmi)
{
	unsigned long Index,Now;
	if (Tag.Start==0) return;
	for(;;)
	{
		Index=Tag.Start;
		if (Index >= fmi->inverseSa0) Index--;//adjust for missing $
		Now=fmi->bwtCode[(Index) / 16] << (((Index) % 16) * 2)>> (BITS_IN_WORD - 2);//FMIBwtValue(fmi,Index);
		if (!Do_Branch[Start-Tag.Level] && Current_Tag[Start-Tag.Level]!=Now) return;  
		Tag.Start = fmi->cumulativeFreq[Now] + BWTOccValue(fmi, Tag.Start, Now) + 1;

		if (Current_Tag[Start-Tag.Level] != Now)
		{
			Tag.Mismatch_Char=Tag.Mismatch_Char | (Now<<Tag.Mismatches*2);
			Tag.Mismatch_Pos=Tag.Mismatch_Pos | ((Start-Tag.Level)<<Tag.Mismatches*6);
			Tag.Mismatches++;
		
		}

		if (Tag.Mismatches<=Count)
		{
			if(Tag.Level== StringLength)
			{
				if(Tag.Mismatches==Count)
				{
					Tag.End=Tag.Start;
					Print_LocationX(Tag);
				}
				return;
			}
			else {Tag.Level++;continue;}
		} 
		else 
		{
			if(MAX_MISMATCHES >= Tag.Mismatches && MAX_MISMATCHES != Count)// 2 mismatches
			{
				Tag.End=Tag.Start;//possibly two mismatch exists..
				if (Tag.Level != StringLength) Tag.Level++; 
				else//two mismatches with the last at the end ... 
				{
					Two_Mismatches_At_End[Two_Mismatches_At_End_Pointer]=Tag;
					Two_Mismatches_At_End_Pointer++;
					return;
				}
				Mismatches_Backward[Mismatches_Backward_Pointer]=Tag;
				Mismatches_Backward_Pointer++;
			}
			return;
		} 
	}
}


//{----------------------------------- FILE HANDLING ---------------------------------------------------------

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  File_Open
 *  Description:  Open a file:
 *  Mode - "w" create/Write text from top    - "wb" Write/Binary  -"w+" open text read/write            -"w+b" same as prev/Binary
 *         "r" Read text from top            - "rb" Read/Binary   -"r+" open text read/write/nocreate   -"r+b" same as prev/binary
 *       - "a" text append/write                                  -"a+" text open file read/append      -"a+b" open binary read/append
 *
 * =====================================================================================
 */
FILE* File_Open(const char* File_Name,const char* Mode)
{
	FILE* Handle;
	Handle=fopen(File_Name,Mode);
	if (Handle==NULL)
	{
		printf("File %s Cannot be opened ....",File_Name);
		exit(1);
	}
	else return Handle;
}

//}----------------------------------- FILE HANDLING ---------------------------------------------------------

//{----------------------------------- FM INDEX ROUTINES ---------------------------------------------------------
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  initFMI
 *  Description:  Opens FM index fmiFile
 * =====================================================================================
 */
BWT* initFMI(const char* BWTCodeFileName,const char* BWTOccValueFileName ) 

{
	BWT *fmi;
        int PoolSize = 524288;
	MMMasterInitialize(3, 0, FALSE, NULL);
	mmPool = MMPoolCreate(PoolSize);

	fmi = BWTLoad(mmPool, BWTCodeFileName, BWTOccValueFileName, NULL, NULL, NULL, NULL);//Load FM index
	return fmi;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Get_SARange
 *  Description:  gets the SA range of strings having prefix [New_Char][Range]
 * =====================================================================================
 */
SARange Get_SARange( long New_Char,struct SARange Range,BWT *fmi)
{

	Range.Start = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.Start, New_Char) + 1;
	Range.End = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.End+1, New_Char);
	if (Range.End<Range.Start) 
	{
		Range.Start=0;
	}
	return Range;

}

void Get_SARange_Fast( long New_Char,struct SARange & Range,BWT *fmi)
{

	Range.Start = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.Start, New_Char) + 1;
	Range.End = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Range.End+1, New_Char);
	if (Range.End<Range.Start) 
	{
		//Range.End=0;
		Range.Start=0;
	}
}

void Get_SARange_Fast_2( long New_Char,struct SARange & Start_Range, struct SARange & Dest_Range,BWT *fmi)
{

	Dest_Range.Start = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Start_Range.Start, New_Char) + 1;
	Dest_Range.End = fmi->cumulativeFreq[New_Char] + BWTOccValue(fmi, Start_Range.End+1, New_Char);
	if (Dest_Range.End<Dest_Range.Start) 
	{
		//Range.End=0;
		Dest_Range.Start=0;
	}
}
//}----------------------------------- FM INDEX ROUTINES ---------------------------------------------------------

//{-----------------------------------PRINT ROUTINE---------------------------------------------------------
void Convert_To_Reverse(SARange &Tag)
{
	char New_Char,Temp_One,Temp_Two;
	unsigned pos;

	if(Tag.Mismatches)
	{
		for( int i=0;i<Tag.Mismatches;i++)
		{
			pos=63 & (Tag.Mismatch_Pos>>(6*i));
			Temp_Char_Array[i]=Current_Tag[pos];
			Current_Tag[pos]=Tag.Mismatch_Char>>(2*i) & 3;
		}

	}
	if(LOOKUPSIZE == 3)
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4);// | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}
	else
	{
		c=Current_Tag[0] | (Current_Tag[1]<<2) | (Current_Tag[2]<<4) | (Current_Tag[3]<<6) | Current_Tag[4]<<8 | (Current_Tag[5]<<10);//Use lookup table...
	}
	Tag.Start=Forward_Start_LookupX[c];Tag.Level=LOOKUPSIZE+1;
	while (Tag.Level <= STRINGLENGTH)
	{
		New_Char=Current_Tag[1-2+Tag.Level];
		Tag.Start = revfmi->cumulativeFreq[New_Char] + BWTOccValue(revfmi, Tag.Start, New_Char) + 1;
		Tag.Level++;
	}

	if(Tag.Mismatches)
	{
		for( int i=0;i<Tag.Mismatches;i++)
		{
			Current_Tag[63 & (Tag.Mismatch_Pos>>(6*i))]=Temp_Char_Array[i];
		}
	} 
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Print_Location
 *  Description:  Prints the hex location of start and end of Range
 * =====================================================================================
 */
void Print_LocationX (struct SARange & Tag)
{
	unsigned Gap;
	
	switch(HITMODE)
	{
		case(DEFAULT):// lowest mismatch output of "best hit"
			{
				//Total_Hits++;
				Total_Tags++;
				Gap=Tag.End-Tag.Start;
				Record.Tag=Tag.Tag;
				if(ONEFMINDEX && FORWARD == FMIndex) 
				{
					Convert_To_Reverse(Tag);
					Record.Index=REVERSE;
				} 
				else 
				{
					Record.Index=FMIndex;
				}
				Record.Start=Tag.Start;
				//Record.Index=FMIndex;
				if (Current_Tag[STRINGLENGTH]=='-') Tag.Mismatches=Tag.Mismatches+100;//code - strand....
				if(63==(63 & (Tag.Mismatch_Pos>>(6)))) 
				{
					if(63==(63 & (Tag.Mismatch_Pos>>(6*2))))
					{
						Tag.Mismatches = Tag.Mismatches+75;//insert...
					}
					else
					{
						Tag.Mismatches = Tag.Mismatches+50;//del...
					}
				}
				Record.Mismatches=Tag.Mismatches;
				Record.Gap=Gap;Gap++;
				fwrite(&Record,sizeof(Record),1,Output_File);
				if (COUNT_ALLHITS)
				{
					if(Hits + Gap > MAXHITS ) Hits = MAXHITS; else Hits=Hits+Gap;//COUNT_ALLHITS =1 => count all the hits, COUNT_ALLHITS =0 => count saranges... i.e. unique hits...
				}
				else
				{
					Hits++;
				}
			//	return;
			}
			return;
		case(DEEP)://output first MAXHITS
			if (Ignore_Print) 
			{
				Hits=Hits+Tag.End-Tag.Start+1;
				return;
			}
			else
			{
				Gap=Tag.End-Tag.Start;
				if ( ONEFMINDEX && FORWARD==FMIndex )// forward search index...
				{
					Convert_To_Reverse(Tag);
				}
				if(UNIQUEHITS && !Gap)//a unique hit...
				{
					Record.Start=Tag.Start;
					Record.Tag=Tag.Tag;
					if(ONEFMINDEX) Record.Index=REVERSE; else Record.Index=FMIndex;
					Record.Mismatches=Tag.Mismatches;
					fwrite(&Record,sizeof(Record),1,Unique_File);
					//if (! ALLHITS) {Hits++;Total_Hits++;Total_Tags++;return;}//write only the unique hits...
					if (! ALLHITS) {Hits++;Total_Tags++;return;}//write only the unique hits...
				}
				if(ALLHITS)//do we need to report all hits?
				{
					if(!Hits || Print_Header) //first hit...
					{
						New_Record='@';
						fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global
						
						if(PRINT_DESC) fprintf(Output_File,"%s",Description);
						for( int i=0;i<STRINGLENGTH; i++) Translated_String[i]=Code_To_Char[Current_Tag[i]];
						Translated_String[STRINGLENGTH]=Current_Tag[STRINGLENGTH];
						fwrite(Translated_String,1,STRINGLENGTH+1,Output_File);//write tag...
						if(!COUNT_ALLHITS) Hits++;//count distinct saranges
						Total_Tags++;
						Print_Header=FALSE;
						One_Added_To_Hits=FALSE;
						Tag_Printed=TRUE;
					}
					//else
					{
						New_Record='%';
						fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global
					}
					Mismatches.Mismatch_Pos=Tag.Mismatch_Pos;
					Mismatches.Mismatch_Char=Tag.Mismatch_Char;

					Mismatches.Gap=Gap;Gap++;

					fwrite(&Mismatches,sizeof(Mismatches),1,Output_File);
					if (COUNT_ALLHITS)
					{
						if(!STATMODE && (Hits + Gap > MAXHITS)) Hits = MAXHITS; else Hits=Hits+Gap;//COUNT_ALLHITS =1 => count all the hits, COUNT_ALLHITS =0 => count saranges... i.e. unique hits...
					}
					Record.Start=Tag.Start;
					Record.Tag=Tag.Tag;
					if (ONEFMINDEX) Record.Index=REVERSE; else Record.Index=FMIndex;
					Record.Mismatches=Tag.Mismatches;
					fwrite(&Record,sizeof(Record),1,Output_File);
					//Total_Hits++;
				}
			}
			if(STATMODE) 
			{
				if(Hits>=MAXHITS) 
				{
					Ignore_Print=TRUE;
					if (Hits==MAXHITS){Hits++;One_Added_To_Hits=TRUE;}
				}
			}
			return;
		case(PAIREND)://output first MAXHITS
			if (Ignore_Print) 
			{
				Hits=Hits+Tag.End-Tag.Start+1;
				return;
			}
			else
			{
				Gap=Tag.End-Tag.Start;
				if ( ONEFMINDEX && FORWARD==FMIndex )// forward search index...
				{
					Convert_To_Reverse(Tag);
				}

				if(!Hits) //first hit...
				{
					if(!First_Pass_Printed)//first pass gets switched ....
					{
						New_Record='@';
						fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global

						if(PRINT_DESC) fprintf(Output_File,"%s",Description);
						fprintf(Output_File,"%s",Tag_Copy);
						First_Pass_Printed=TRUE;
					}
					if(!COUNT_ALLHITS) Hits++;//count distinct saranges
					Total_Tags++;
					One_Added_To_Hits=FALSE;
					Tag_Printed=TRUE;
				}
				//else
				{
					New_Record='%';
					fwrite(&New_Record,1,1,Output_File);//write new record marker... later make gap global
					if(!First_Pass) Translated_String[0]='H'; else Translated_String[0]='T';
					Translated_String[1]=Current_Tag[STRINGLENGTH];//Translated_String[2]=0;
					fwrite(&Translated_String,2,1,Output_File);//write head/tail and orientation...
				}
				Mismatches.Mismatch_Pos=Tag.Mismatch_Pos;
				Mismatches.Mismatch_Char=Tag.Mismatch_Char;
				Mismatches.Gap=Gap;
				Gap++;

				fwrite(&Mismatches,sizeof(Mismatches),1,Output_File);
				if (COUNT_ALLHITS)
				{
					if(!STATMODE && (Hits + Gap > MAXHITS) ) Hits = MAXHITS; else Hits=Hits+Gap;//COUNT_ALLHITS =1 => count all the hits, COUNT_ALLHITS =0 => count saranges... i.e. unique hits...
				}
				Record.Start=Tag.Start;
				Record.Tag=Tag.Tag;
				if (ONEFMINDEX) Record.Index=REVERSE; else Record.Index=FMIndex;
				Record.Mismatches=Tag.Mismatches;
				fwrite(&Record,sizeof(Record),1,Output_File);
				//Total_Hits++;
			}
			if(STATMODE) 
			{
				if(Hits>=MAXHITS) 
				{
					Ignore_Print=TRUE;
					if (Hits==MAXHITS){Hits++;One_Added_To_Hits=TRUE;}
				}
			}
			return;
	}
}
//}-----------------------------------PRINT ROUTINE---------------------------------------------------------

//{-----------------------------  Parse Command Line  -------------------------------------------------
void Parse_Command_line(int argc, char* argv[])
{
	int Current_Option=0;
	char* Short_Options ="F:A::hq:o:b:m:f:g:u:sx::n:pa:cH::iI:rBMS";//allowed options....
	char* This_Program = argv[0];//Current program name....
	char* Help_String=
"Parameters:\n"
" --help | -h\t\t\t\t Print help\n"
" --query | -q <filename>\t\t Query file(File of Tags)\n"
" --output | -o <filename>\t\t Name of output file\n"
" --usequality | -p \t\t use quality score..\n"
" --uniquefile | -u <filename>\t\t Name of output file for unique hits\n"
" --genome | -g <filename>\t\t Name of the reference genome\n"
" --buffersize | -b <integer> \t\t Size of disk buffers\n"
" --maxhits | -m <integer> \t\t Maximum number of hits to output...\n"
" --singleindex | -s <integer> \t\t Convert output to be read by reverse index only...\n"
" --mishits | -x <file name> \t\t Print unprocessed hits to a file..\n"
" --maxmismatches | -n <number> \t\t maximum mismatches allowed..\n"
" --scanboth | M \t\t scan both directions of a tag, even if default yields hit..\n"
" --nocomplement | -c <filename> \t\t Do not scan for the reverse complement of tag..\n"
" --filterambiguous | -A <filename> \t\t Write tags whose orientatin is ambiguous to a file ..\n"
" --mismatchstat | -S <filename> \t\t Write upto --maxhits mismatches and gather statistics ..\n"
" --accuracy | -a <number> \t\t accuracy level... (0=normal,1=strict)\n"
" --noindel | i \t\t Donot scan for indels ...\n"
" --ignorehead | I <number> \t\t Ignore specified number of bases from start...\n"
" --forcelength | F <number> \t\t Force the string lengths to be that specified by number...\n"
" --heuristics | -H <options=value> \t\t Use heuristics, where \n"
"			   \t\t length = <number> Smallest length of exact match to apply heuristics...\n"
"			   \t\t mm = <number> Maximum mismatches to allow for a string with exact match specified above...\n"
" --rollover | -r \t\t when MAXHITS hits are not found, scan the reverse complement..\n"
" --printblanks | -B \t\t write an empty record for unprocessed tags..\n"
" --format | -f <level>  \t\t Format of the output where Level is\n"
"                          \t\t 0 - outputs without detailed information\n"
"                          \t\t 1 - outputs only unique hits\n"//the uniqu hits are written to uniquhits.txt in filtering modes..
"                          \t\t 2 - Write unique hits to a seperate file in addition to the detailed output file\n"
"                          \t\t 3 - Detailed output file\n"
"                          \t\t 4 - Minimum format needed to run verification\n"
;

	Source=(char*)malloc(sizeof(char)*5000);//create space for file names...
	char *options, *value; 
	char* Name;int Last_Dash;char* Genome_Name;

	PATTERNFILE=PATTERNFILE_DEFAULT;HITSFILE=HITSFILE_DEFAULT;UNIQUEFILE=UNIQUEFILE_DEFAULT;GENOMEFILE=GENOMEFILE_DEFAULT;
	MISHITFILE=MISHITFILE_DEFAULT;AMBIGUOUSFILE=AMBIGUOUSFILE_DEFAULT;
	BWTFILE = BWTFILE_DEFAULT; 
	OCCFILE = OCCFILE_DEFAULT;
	REVBWTINDEX=REVBWTINDEX_DEFAULT;
	REVOCCFILE=REVOCCFILE_DEFAULT;

	Translated_String[STRINGLENGTH]='\0';//temporary buffer for translation of string...

	for(;;)	
	{
		Current_Option=getopt_long(argc, argv, Short_Options, Long_Options, NULL);
		if (Current_Option == -1 ) break;
		switch(Current_Option)
		{
			case 'h':
				printf("%s \n",Help_String);exit(0);
			case 'i':
				INDELSCAN=FALSE;
				break;
			case 'S':
				STATMODE=TRUE;
				break;
			case 'r':
				ROLLOVER=TRUE;
				break;
			case 'B':
				PRINTBLANKS=TRUE;
				break;
			case 'M':
				SCANBOTH=TRUE;
				ROLLOVER=TRUE;
				break;
			case 'I':
				IGNOREHEAD=atoi(optarg);
				break;
			case 'F':
				FORCELENGTH=atoi(optarg);
				break;
			case 'H':
				HEURISTIC=TRUE;
				options = optarg; 
				if (!options) break;
				while (*options != '\0')
				{ 
					switch(getsubopt(&options, myopts, &value))
					{
						case HEU_LENGTH:
							if (value == NULL)
							{
								printf("Error - need value for length..\n");	
								exit(1);
							}
							MAX_HIGH_QUALITY_LENGTH=atoi(value);
							break;
						case HEU_MISMATCHES:
							if (value == NULL)
							{
								printf("Error - need value for length..\n");	
								exit(1);
							}
							MAX_MISMATCHES_H=atoi(value);
							break;
						default:
							/* process unknown token */
							printf("Unknown parameter...\n");exit(1);
							break;
					} 
				}
				break;
			case 'A':
				FILTER_AMBIGUOUS=TRUE;
				if(optarg) AMBIGUOUSFILE=optarg;
				break;
			case 'a':
				if(atoi(optarg)==1) SUPERACCURATE=TRUE;
				break;
			case 'c':
				DIRECTIONS=1;
				TAG_GUESS=FALSE;
				break;

			case 'p':
				USEQUALITY=TRUE;
				break;
			case 'n':
				MAX_MISMATCHES = atoi(optarg);
				if (MAX_MISMATCHES <0 or MAX_MISMATCHES >5) MAX_MISMATCHES=5;
				break;
			case 'x':
				PRINT_MISHITS=TRUE;
				if(optarg) MISHITFILE=optarg;
				//Mishit_File=File_Open("mishits.fq","w");
				break;
			case 's':
				ONEFMINDEX =TRUE;
				break;
			case 'q':
				PATTERNFILE=optarg;
				break;
			case 'o':
				HITSFILE=optarg;
				break;
			case 'u':
				UNIQUEFILE=optarg;
				break;
			case 'b':
				DISKBUFFERSIZE=atol(optarg);
				break;
			case 'm':
				if(!(MAXHITS=atoi(optarg))) {printf("Maximum hits defaulted to 1\n");MAXHITS=1;};
				break;
			case 'l':
				STRINGLENGTH=atoi(optarg);
				break;
			case 'f':
				HITMODE=atoi(optarg);
				if(HITMODE ==0) { UNIQUEHITS =FALSE;ALLHITS = FALSE; }
				else if(HITMODE ==1) { HITMODE=DEEP;UNIQUEHITS =TRUE;ALLHITS = FALSE; }
				else if(HITMODE ==2) { HITMODE=DEEP;UNIQUEHITS =TRUE;ALLHITS = TRUE; }
				else if(HITMODE ==3) { HITMODE=DEEP;UNIQUEHITS =FALSE;ALLHITS = TRUE;}
				else if(HITMODE ==4) { HITMODE=DEEP;UNIQUEHITS =FALSE;ALLHITS = TRUE;PRINT_DESC=FALSE;}
				break;
			case 'g':
				Name=optarg;Last_Dash=0;Genome_Name=optarg;
				for(;Name[0]!=0;Name++)
				{
					if (Name[0]=='/') 
					{
						Last_Dash++;Genome_Name=Name;
					}
				}

				REVBWTINDEX = (char*)Source;
				if(Last_Dash) Last_Dash=Genome_Name-optarg+1; else Genome_Name--;
				strncpy(REVBWTINDEX,optarg,Last_Dash);
				REVBWTINDEX[Last_Dash+0]='r';REVBWTINDEX[Last_Dash+1]='e';REVBWTINDEX[Last_Dash+2]='v';
				strcpy(REVBWTINDEX+Last_Dash+3,Genome_Name+1);
				strcat(REVBWTINDEX+Last_Dash+3,".bwt"); 

				BWTFILE=REVBWTINDEX+600;
				strncpy(BWTFILE,optarg,Last_Dash);
				strcpy(BWTFILE+Last_Dash,Genome_Name+1);
				strcat(BWTFILE+Last_Dash,".bwt"); 


				REVOCCFILE = BWTFILE+600;
				strncpy(REVOCCFILE,optarg,Last_Dash);
				REVOCCFILE[Last_Dash+0]='r';REVOCCFILE[Last_Dash+1]='e';REVOCCFILE[Last_Dash+2]='v';
				strcpy(REVOCCFILE+Last_Dash+3,Genome_Name+1);
				strcat(REVOCCFILE+Last_Dash+3,".fmv"); 


				OCCFILE=REVOCCFILE+600;			
				strncpy(OCCFILE,optarg,Last_Dash);
				strcpy(OCCFILE+Last_Dash,Genome_Name+1);
				strcat(OCCFILE+Last_Dash,".fmv"); 


				break;
			default:
				printf("%s \n",Help_String);
				exit(0);
		}
	}	
}

//}-----------------------------  Parse Command Line  -------------------------------------------------
void Build_Tables()
{
	Range LRange;
	for( int i=0;i<4;i++)//fill lookup tables for first character...
	{
		Forward_Start_Lookup[i]=revfmi->cumulativeFreq[i] + 1;
		Forward_End_Lookup[i]=revfmi->cumulativeFreq[i + 1];
		Backward_Start_Lookup[i]=fwfmi->cumulativeFreq[i] + 1;
		Backward_End_Lookup[i]=fwfmi->cumulativeFreq[i + 1];//
		LRange.Start=revfmi->cumulativeFreq[i] + 1;
		LRange.End=revfmi->cumulativeFreq[i + 1];
		LRange.Label=i;
		for(int j=0;j<4;j++) Build_Preindex_Forward(LRange, 2, j);
		LRange.Start=fwfmi->cumulativeFreq[i] + 1;
		LRange.End=fwfmi->cumulativeFreq[i + 1];
		LRange.Label=i;
		for(int j=0;j<4;j++) Build_Preindex_Backward(LRange, 2, j);

	}
}

void Allocate_Memory()
{
	int STRINGLENGTH =36;
	int Max_Allocate=1;
	int Max_Limit=5;
	for (int i=0;i<Max_Limit-1;i++) Max_Allocate=Max_Allocate*STRINGLENGTH;
	Forward_Start_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	Forward_End_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	Backward_Start_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	Backward_End_LookupX=(unsigned*)malloc(sizeof(unsigned)*(2<<2*LOOKUPSIZE));
	BMHStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	FSHStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	FSHStackX0X=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	FSSStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	BMStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	BMStack_X11=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	BMStack_X11H=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	PSBStack=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Exact_Match_ForwardF=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Exact_Match_ForwardC=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Exact_Match_Backward=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH);	
	Left_Mishits=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);	
	Right_Mishits=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);	
	Mismatches_Backward=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);//STRINGLENGTH*STRINGLENGTH);//*STRINGLENGTH);	
	Mismatches_Forward=(SARange*)malloc(sizeof(SARange)*4*Max_Allocate);//STRINGLENGTH*STRINGLENGTH*STRINGLENGTH);	
	Two_Mismatches_At_End_Forward=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	Two_Mismatches_At_End=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	Possible_20=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	Possible_02=(SARange*)malloc(sizeof(SARange)*2*STRINGLENGTH*STRINGLENGTH);	
	if (NULL==Mismatches_Backward||NULL==Two_Mismatches_At_End_Forward||NULL==Two_Mismatches_At_End||NULL==BMHStack||NULL==FSHStackX0X||NULL==FSHStack||NULL==FSSStack||NULL==BMStack_X11H||NULL==BMStack_X11||NULL==BMStack||NULL==Exact_Match_Backward||NULL==Exact_Match_ForwardF||NULL==Exact_Match_ForwardC||NULL==Mismatches_Forward||NULL==PSBStack||NULL==Forward_End_LookupX||NULL==Forward_Start_LookupX||NULL==Source)
	{
		printf("out of memory");
		exit(1);
	}

	Exact_Match_Forward=Exact_Match_ForwardF;

}

void Open_Files()
{

	Input_File=File_Open(PATTERNFILE,"r");//Load tags
	Output_File=File_Open(HITSFILE,"w");//Open output file...
	Unique_File=File_Open(UNIQUEFILE,"w");//Open output file...
	if (STATMODE) Stat_File=File_Open("statistics","w");
	if (FILTER_AMBIGUOUS) Ambiguous_File=File_Open(AMBIGUOUSFILE,"w");
	if (PRINT_MISHITS) Mishit_File=File_Open(MISHITFILE,"w"); 
	if(setvbuf(Unique_File,NULL,_IOFBF,DISKBUFFERSIZE*sizeof(long))||setvbuf(Output_File,NULL,_IOFBF,DISKBUFFERSIZE*sizeof(long)))
	{
		printf("Allocating disk buffers failed...\n");
		exit(1);
	}

}

void Init_Variables()
{

	if (fgets(Description,MAXDES,Input_File)!=0)//Measure tag length
	{
		fgets(Current_Tag,MAXTAG,Input_File);
		for(STRINGLENGTH=0;Current_Tag[STRINGLENGTH]!='\n' && Current_Tag[STRINGLENGTH]!='\r' && Current_Tag[STRINGLENGTH]!=0 && Current_Tag[STRINGLENGTH]!=PAIR_END_SEPERATOR;STRINGLENGTH++);
		if(Current_Tag[STRINGLENGTH]==PAIR_END_SEPERATOR) 
		{
			NORMAL_TAGS=FALSE;//we have pair ended tags..
			PAIR_LENGTH_LEFT=STRINGLENGTH;
			if (PAIR_LENGTH_LEFT < 28) PLOOKUPSIZE=3;
			PLH=PAIR_LENGTH_LEFT/2;//calculate tag portions...
			PLHQL=PLH/2;
			if ((PAIR_LENGTH_LEFT % 2)) {PLH++;PLHQL++;}	
			PLHQR=PLH-PLHQL;
			PRH=PAIR_LENGTH_LEFT-PLH;
			PRHQL=PRH/2;PRHQR=PRH-PRHQL;

			for(PAIR_LENGTH_RIGHT=0;Current_Tag[STRINGLENGTH+1+PAIR_LENGTH_RIGHT]!='\n' && Current_Tag[STRINGLENGTH+1+PAIR_LENGTH_RIGHT]!='\r' && Current_Tag[STRINGLENGTH+1+PAIR_LENGTH_RIGHT]!=0;PAIR_LENGTH_RIGHT++);
			if (PAIR_LENGTH_RIGHT < 28) PRLOOKUPSIZE=3;
			PRLH=PAIR_LENGTH_RIGHT/2;//calculate tag portions...
			PRLHQL=PRLH/2;
			if ((PAIR_LENGTH_RIGHT % 2)) {PRLH++;PRLHQL++;}	
			PRLHQR=PRLH-PRLHQL;
			PRRH=PAIR_LENGTH_RIGHT-PRLH;
			PRRHQL=PRRH/2;PRRHQR=PRRH-PRRHQL;
			
			if (PAIR_LENGTH_RIGHT> PAIR_LENGTH_LEFT) STRINGLENGTH=PAIR_LENGTH_RIGHT;

		}
		else
		{
			fgets(Quality,MAXTAG,Input_File);//plus
			fgets(Quality,MAXTAG,Input_File);//phred

			if (FORCELENGTH) STRINGLENGTH=FORCELENGTH;

		}

		if ((PAIR_LENGTH_RIGHT) && (PAIR_LENGTH_RIGHT < 28 || PAIR_LENGTH_LEFT < 28))LOOKUPSIZE=3;
		LH=STRINGLENGTH/2;//calculate tag portions...
		LHQL=LH/2;
		if ((STRINGLENGTH % 2)) {LH++;LHQL++;}	
		LHQR=LH-LHQL;
		RH=STRINGLENGTH-LH;
		RHQL=RH/2;RHQR=RH-RHQL;
	}
	else {printf("Tag file error : cannot get string length\n");exit(1);}

	for(int i=0;i<4;i++)//sample 4 reads for length...
	{
		if (fgets(Description,MAXDES,Input_File)!=0)//des .... read a tag
		{
			fgets(Current_Tag,MAXDES,Input_File);//tag
			if (NORMAL_TAGS)
			{
				fgets(Quality,MAXTAG,Input_File);//plus
				fgets(Quality,MAXTAG,Input_File);//phred
			}
		}
	}

	Average_Length=ftello(Input_File)/5;
	fseek(Input_File, 0L, SEEK_END);
	File_Size = ftello(Input_File);
	Number_of_Tags=(File_Size/Average_Length)/20;

	fseek(Input_File,0,SEEK_SET);//go top
	
	if (IGNOREHEAD) STRINGLENGTH=STRINGLENGTH-IGNOREHEAD;	

	Low_Quality=(char*)malloc(STRINGLENGTH+1);
	Do_All=(char*)malloc(STRINGLENGTH+1);
	

	Char_To_Code['N']=0;Char_To_Code['n']=0;Char_To_Code['A']=0;Char_To_Code['C']=1;Char_To_Code['G']=2;Char_To_Code['T']=3;Char_To_Code['a']=0;Char_To_Code['c']=1;Char_To_Code['g']=2;Char_To_Code['t']=3;Char_To_Code['+']='+';Char_To_Code['-']='-';//we are using character count to store the fmicode for acgt
	Char_To_CodeC['N']=3;Char_To_CodeC['n']=3;Char_To_CodeC[0]=3;Char_To_CodeC[1]=2;Char_To_CodeC[2]=1;Char_To_CodeC[3]=0;Char_To_CodeC['a']=3;Char_To_CodeC['c']=2;Char_To_CodeC['g']=1;Char_To_CodeC['t']=0;Char_To_CodeC['-']='-';Char_To_CodeC['+']='+';//we are using character count to store the fmicode for acgt

	if(!MAX_MISMATCHES_H) MAX_MISMATCHES_H=MAX_MISMATCHES;
	if (!MAX_HIGH_QUALITY_LENGTH) MAX_HIGH_QUALITY_LENGTH=STRINGLENGTH-MAX_MISMATCHES_H;


}

void Load_Indexes()
{
	fwfmi=initFMI(BWTFILE,OCCFILE);//Load FM indexes
	revfmi=initFMI(REVBWTINDEX,REVOCCFILE);
	SOURCELENGTH = fwfmi->textLength;
	if (SOURCELENGTH!=revfmi->textLength)
	{ 
		printf("FM index load error \n"); 
		exit(1);
	}
	FWDInverseSA0=fwfmi->inverseSa0;
}

void Verbose()
{
	printf("\n-= BATMAN beta version PAIR_END_SEPERATOR =-\n");
	printf("Using the genome files\n %s\t %s\n %s\t %s\n", BWTFILE,OCCFILE,REVBWTINDEX,REVOCCFILE); 
	printf("Query File : %s \t\t Output file: %s\n",PATTERNFILE,HITSFILE);
	printf("Length of Tags: %d\t", STRINGLENGTH);
	printf("Mismatches allowed : %d\n",MAX_MISMATCHES);
	if (!NORMAL_TAGS) printf("Pair end data ... Left %d  Right %d\n",PAIR_LENGTH_LEFT,PAIR_LENGTH_RIGHT); 
	if(USEQUALITY) printf("Using Quality info ...\n"); else printf("Not using Quality info...\n");
	if (DIRECTIONS ==2 ) printf("Scanning complement...\n"); else printf("Not scanning complement...\n");
	if(PRINT_MISHITS) printf("Mishits file: %s\n",MISHITFILE);
	if(FILTER_AMBIGUOUS) printf("Ambiguous file: %s\n",AMBIGUOUSFILE);
	if(INDELSCAN && MAX_MISMATCHES >=3 ) printf("Scanning indels...\n"); else printf("Not scanning indels...\n");
	if (IGNOREHEAD) printf ("Ignoring %d bases fromm start ..\n", IGNOREHEAD);
	if(HEURISTIC)
	{
		printf ("Heuristics on with length = %d, mismatches %d\n",MAX_HIGH_QUALITY_LENGTH,MAX_MISMATCHES_H);
	}
	if(SCANBOTH) printf ("Scanning both directions of the tag...\n");
	if(STATMODE) printf ("writing statistics to file: statistics\n");
}


void Show_Progress(unsigned Percentage)
{
	if (Percentage >=97) return;
	printf("+%d%\b\b\b",Percentage);
	fflush(stdout);
}

void Read_INI()
{

	dictionary* Dictionary;
	Dictionary=iniparser_load("batman.ini",FALSE);
	if (Dictionary)
	{
		MAX_MISMATCHES = iniparser_getint(Dictionary,"settings:mismatches",4);
		INDELSCAN = iniparser_getint(Dictionary,"settings:indels",1);
		USEQUALITY = iniparser_getint(Dictionary,"settings:quality",0);
		ONEFMINDEX=iniparser_getint(Dictionary,"settings:singleindex",0);
		SUPERACCURATE=iniparser_getint(Dictionary,"settings:singleindex",0);
		HEURISTIC=iniparser_getint(Dictionary,"settings:heuristics",0);
		IGNOREHEAD=iniparser_getint(Dictionary,"settings:ignorehead",0);

		PRINTBLANKS=iniparser_getint(Dictionary,"scan:printblanks",0);
		ROLLOVER=iniparser_getint(Dictionary,"scan:rollover",0);
		SCANBOTH=iniparser_getint(Dictionary,"scan:scanboth",0);
		if(SCANBOTH) ROLLOVER=TRUE;
		HITMODE=iniparser_getint(Dictionary,"output:hitmode",0);
		if(HITMODE ==0) { UNIQUEHITS =FALSE;ALLHITS = FALSE; }
		else if(HITMODE ==1) { HITMODE=DEEP;UNIQUEHITS =TRUE;ALLHITS = FALSE; }
		else if(HITMODE ==2) { HITMODE=DEEP;UNIQUEHITS =TRUE;ALLHITS = TRUE; }
		else if(HITMODE ==3) { HITMODE=DEEP;UNIQUEHITS =FALSE;ALLHITS = TRUE;}
		else if(HITMODE ==4) { HITMODE=DEEP;UNIQUEHITS =FALSE;ALLHITS = TRUE;PRINT_DESC=FALSE;}


	}
	iniparser_freedict(Dictionary);
}
