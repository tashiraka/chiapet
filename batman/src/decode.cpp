//version checked upto pruning of one mismatch of last stage...
//Basic save added...
//pair end.. fix directory bug

//{-----------------------------  INCLUDE FILES  -------------------------------------------------/
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <ctype.h>
//#include "fmsearch.h"
extern "C"
{
#include <time.h>
#include "MemManager.h"
#include "MiscUtilities.h"
#include "TextConverter.h"
#include "iniparser.h"
#include "BWT.h"
}
//}-----------------------------  INCLUDE FILES  -------------------------------------------------/
//#pragma pack(push)
//#pragma pack(1)
struct Offset_Record
{
	char Genome[40];
	unsigned Offset;
};

struct Output_Record
{
	//char Name[36];
	unsigned Tag;
	unsigned  Start;
	char Index;
	char Mismatches;
	int Gap;
}__attribute__((__packed__));

struct Mismatches_Record
{

	/*int 	One_Mismatch_Pos;
	char 	One_Mismatch_Char;
	int 	Two_Mismatch_Pos;
	char 	Two_Mismatch_Char;*/
	int 	Gap;
	unsigned Mismatch_Pos;//6|6|...
	unsigned Mismatch_Char;//2|2|...

}__attribute__((__packed__));

struct Header
{
	char ID[3] ;
	unsigned MAXHITS;
	char HITMODE;
	char Index_Count;
	int Tag_Length;
	char Print_Desc;
}__attribute__((__packed__));

//#pragma pack(pop)
//{-----------------------------  DEFINES  -------------------------------------------------/

#define MAXDES 400
#define DEFAULT 0
#define DEEP 1
#define PAIREND 2

#define DEBUG 0
#define BUFFERSIZE 1000000

/* 	#define BWTFILE "chr11.bwt"// "genome.bwt"// 
 * 	#define OCCFILE "chr11.fmv"// "genome.fmv"//
 * 	#define SAFILE "chr11.sa"
 * 	#define REVBWTFILE "revchr11.bwt"//"revgenome.bwt"//
 * 	#define REVOCCFILE "revchr11.fmv"//"revgenome.fmv"//
 * 	#define REVSAFILE "revchr11.sa"
 */

#define MAXSTRINGLENGTH 180 //36//36//6+EXTRA//6+EXTRA//36
#define HALFSTRINGLENGTH 18
#define QUARTERSTRINGLENGTH 9


//}-----------------------------  DEFINES  -------------------------------------------------/

//{-----------------------------  GLOBALS  -------------------------------------------------/
void Print_Location (unsigned Range,BWT *fmi);
void Location_To_Genome( unsigned & Location);
void Process_Default();
void Process_Deep();
void Process_Pairend();
void Process_Pairend_Formatted();
void Process_Deep_Formatted();
void Read_INI();
FILE* File_Open(const char* File_Name,const char* Mode);
BWT* initFMI(const char* BWTCodeFileName,const char* BWTOccValueFileName, const char* SAfile);

Mismatches_Record Mismatches;
Output_Record Record;
Output_Record* Hits;//[BUFFERSIZE + 1];
MMPool *mmPool;
BWT *fmi,*revfmi;
Header Head;
Offset_Record Genome_Offsets[80];

int debug;
int Mismatch_Count;
int Offset=0;
int Genome_Count=0;
int Genome_Position;//position of offset info
int STRINGLENGTH=36;

unsigned CONVERSION_FACTOR;
unsigned MAXHITS;
unsigned Total_Hits=0;
unsigned Total_Tags=0;
unsigned Offsets[80];
unsigned DISKBUFFERSIZE=36*1000000;

char Translated_String_Original[MAXSTRINGLENGTH+1];
char Translated_String[MAXSTRINGLENGTH+1];
char RevString[MAXSTRINGLENGTH+1];
char Test_String[137];
char Tag_Type;//@ if new tag % if still in an old tag & if end...

FILE* Data_File;
FILE* Output_File;
FILE* Binary_File;
FILE* Location_File;
const char* Code_To_Char="acgt";

char* Command_Line_Buffer;
char* INPUTFILE;
char* OUTPUTFILE;
char* BWTFILE ; 
char* OCCFILE ;
char* REVBWTINDEX;
char* REVOCCFILE;
char* REVSAFILE;
char* SAFILE;
char* BINFILE;
char* LOCATIONFILE ; 

char Description[MAXDES];
char Char_To_CodeC[256];
char LOADREVERSEONLY=FALSE;
char PRINT_DESC;
char FORMAT=FALSE;
char BWTFILE_DEFAULT[] = "genome.bwt";//"genome.bwt";// 
char OCCFILE_DEFAULT[] ="genome.fmv";//"genome.fmv";//
char REVBWTINDEX_DEFAULT[] ="revgenome.bwt";//"revgenome.bwt";//
char REVOCCFILE_DEFAULT[] ="revgenome.fmv";//"revgenome.fmv";//
char REVSAFILE_DEFAULT[] ="revgenome.sa";
char SAFILE_DEFAULT[] ="genome.sa";
char BINFILE_DEFAULT[] = "genome.bin";//"genome.bwt";// 
char INPUTFILE_DEFAULT[]="hits.txt";
char OUTPUTFILE_DEFAULT[]="output.txt";
char LOCATIONFILE_DEFAULT[]="location";
char USELOCATION = FALSE;
char VERIFY=FALSE;//TRUE;//FALSE;
char PLUSSTRAND=FALSE;

//}-----------------------------  GLOBALS  -------------------------------------------------/
//{---------------------------- Command Line  -------------------------------------------------
option Long_Options[]=
{
{"help",0,NULL,'h'},
{"inputfile",1,NULL,'i'},
{"ganome",1,NULL,'g'},
{"outputfile",1,NULL,'o'},
{"buffersize",1,NULL,'b'},
{"reverseload",0,NULL,'r'},
{"verify",0,NULL,'v'},
{"offset",1,NULL,'O'},
{"plusstrand",0,NULL,'p'},
{"formatted",1,NULL,'f'},
{"location",optional_argument,NULL,'l'},
{0,0,0,0}
};
//}---------------------------- Command Line -------------------------------------------------
void Parse_Command_line(int argc, char* argv[]);
void Verify_Deep();
void Verify_Pairend();
//{-----------------------------  Main  -------------------------------------------------
int main(int argc, char* argv[])
{

	INPUTFILE=INPUTFILE_DEFAULT;
	OUTPUTFILE=OUTPUTFILE_DEFAULT;
	BWTFILE = BWTFILE_DEFAULT; 
	OCCFILE = OCCFILE_DEFAULT;
	REVBWTINDEX=REVBWTINDEX_DEFAULT;
	REVOCCFILE=REVOCCFILE_DEFAULT;
	REVSAFILE=REVSAFILE_DEFAULT;
	SAFILE=SAFILE_DEFAULT;
	BINFILE = BINFILE_DEFAULT;
	LOCATIONFILE=LOCATIONFILE_DEFAULT;
	Char_To_CodeC['A']=3;Char_To_CodeC['C']=2;Char_To_CodeC['G']=1;Char_To_CodeC['T']=0;Char_To_CodeC['a']=3;Char_To_CodeC['c']=2;Char_To_CodeC['g']=1;Char_To_CodeC['t']=0;Char_To_CodeC['n']=0;Char_To_CodeC['N']=0;//we are using character count to store the fmicode for acgt
	Read_INI();

	Command_Line_Buffer=(char*)malloc(5000);
	Parse_Command_line(argc,argv);	

	Data_File=File_Open(INPUTFILE,"rb");
	if (! VERIFY) 
	{
		Output_File=File_Open(OUTPUTFILE,"w");
		if(setvbuf(Output_File,NULL,_IOFBF,DISKBUFFERSIZE*sizeof(long))) printf("Buffer allocation failure... Disk access will be unbffered\n");
	}
	if(setvbuf(Data_File,NULL,_IOFBF,DISKBUFFERSIZE*sizeof(long))) printf("Buffer allocation failure... Disk access will be unbffered\n");
	
	if (USELOCATION)
	{
		Location_File=File_Open(LOCATIONFILE,"r");
		while (fgets(Genome_Offsets[Genome_Count].Genome,39,Location_File)!=0 && Genome_Count<80)
		{
			Genome_Offsets[Genome_Count].Offset=atoi(Genome_Offsets[Genome_Count].Genome);
			fgets(Genome_Offsets[Genome_Count].Genome,39,Location_File);
			for(int i=0;i<40;i++) 
			{
				if (Genome_Offsets[Genome_Count].Genome[i] == '\n' ||Genome_Offsets[Genome_Count].Genome[i] == '\r')
				{ 
					Genome_Offsets[Genome_Count].Genome[i]=0;
					break;
				} 
			}
			Genome_Count++;	
		}
		for ( int i=1;i<Genome_Count;i++)
		{
			Offsets[i]=Offsets[i-1]+Genome_Offsets[i].Offset;
		}
	}

	fread(&Head,1,sizeof(Header),Data_File);
	if(!(Head.ID[0]=='B'&&Head.ID[1]=='A'&&Head.ID[2]=='T')) {printf("Not a BAT file\n");exit(0);};
	MAXHITS=Head.MAXHITS;
	STRINGLENGTH=Head.Tag_Length;
	PRINT_DESC=Head.Print_Desc;
	if(!Head.Index_Count)//!LOADREVERSEONLY)
	{
		fmi=initFMI(BWTFILE,OCCFILE,SAFILE);//Load FM indexes
		printf("Forward index loaded ...\n");
	}

	revfmi=initFMI(REVBWTINDEX,REVOCCFILE,REVSAFILE);//Load FM indexes
	printf("Reverse index loaded ...\n");
	CONVERSION_FACTOR=revfmi->textLength-STRINGLENGTH;//+1;

	printf("Using the genome files\n %s\t %s\t %s\n %s\t %s\t %s\n", BWTFILE,OCCFILE,SAFILE,REVBWTINDEX,REVOCCFILE,REVSAFILE); 
	printf("Input File : %s\t  Output File : %s \n",INPUTFILE, OUTPUTFILE);
	printf("Length of a tag : %d\n", STRINGLENGTH);
	if (USELOCATION) printf("Using location file : %s\n", LOCATIONFILE);
	if (Offset) printf("Offset %d \n",Offset);

	switch (Head.HITMODE)
	{
		case DEFAULT:
			if (VERIFY) {printf("File format insufficient for verification...\n");exit(0);}
			Process_Default();
			break;
		
		case DEEP:
			if(VERIFY) Verify_Deep();
			else if(FORMAT) Process_Deep_Formatted(); else Process_Deep();
			break;
		case PAIREND:
			if(VERIFY) Verify_Pairend();
			else if(FORMAT) Process_Pairend_Formatted(); else Process_Pairend();
			break;
	}
	
	printf("Total hits/Tags %d / %d\n",Total_Hits,Total_Tags);

}

//}-----------------------------  Main  -------------------------------------------------

//{-----------------------------  Verify Deep  -------------------------------------------------
void Verify_Deep()
{
	unsigned i, Bitsread;
	unsigned Start,End;
	unsigned Previous_Tag,Hits;
	unsigned Location;
	unsigned pos;
	int Conversion_Factor,StringLength;

	printf ("Verifying using %s\n", BINFILE);
	Binary_File=fopen(BINFILE,"rb");
	Test_String[STRINGLENGTH]=0;
	for(;;)
	{
		i=0;
		fread(&Tag_Type,1,1,Data_File);
		if('&' == Tag_Type) break;
		if('@' == Tag_Type)//start of new tag
		{
			if(PRINT_DESC) fgets(Description,1000,Data_File);
			fread(Translated_String_Original,STRINGLENGTH+1,1,Data_File);
			Hits=0;Total_Tags++;
			continue;
		}		
		else
		{

			memcpy(Translated_String,Translated_String_Original,MAXSTRINGLENGTH);
			Conversion_Factor=CONVERSION_FACTOR;
			StringLength=STRINGLENGTH;
			fread(&Mismatches,sizeof(Mismatches_Record),1,Data_File);
			fread(&Record,sizeof(Output_Record),1,Data_File);

			Mismatch_Count = Record.Mismatches;

			if (Mismatch_Count)//mismatch positions..
			{
				for( int i=0;i<Mismatch_Count;i++)
				{
					Translated_String[63 & (Mismatches.Mismatch_Pos>>(6*i))]=Code_To_Char[Mismatches.Mismatch_Char>>(2*i) & 3];
				}
			}

			if((63 & (Mismatches.Mismatch_Pos>>6))==63)//Indel...
			{
				pos=63 & (Mismatches.Mismatch_Pos>>(6*2));
				if (pos==63)//insertion
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*3));
					Conversion_Factor--;StringLength++;
					for(int i=STRINGLENGTH+1;i>pos;i--) 
					{
						Translated_String[i]=Translated_String[i-1];
					}
					Translated_String[pos]=Code_To_Char[Mismatches.Mismatch_Char >> 2*2];
				}
				else//deletion
				{
					Conversion_Factor++;StringLength--;
					for(int i=pos;i<STRINGLENGTH;i++) 
					{
						Translated_String[i]=Translated_String[i+1];
					}
				}
			}		

			for (int j=0;j<=Mismatches.Gap && Hits< MAXHITS;j++)
			{
				if( Record.Index)//print record...
				{
					Location = Conversion_Factor-BWTSaValue(revfmi,Record.Start);
				}
				else
				{
					Location = BWTSaValue(fmi,Record.Start);
				}
				//Location_To_Genome(Location);
				fseeko(Binary_File,Location,SEEK_SET);
				fread(&Test_String,StringLength,1,Binary_File);
				if (strncmp(Test_String,Translated_String,StringLength))
				{
					for (unsigned i=0;i<=StringLength-1;i++)
					{
						Translated_String[i]=Code_To_Char[Char_To_CodeC[Translated_String[i]]];
					}

					if (strncmp(Test_String,Translated_String,StringLength))
					{
						printf("Error in Tag %d\t%s \n: Location %u is\t%s\t%s\n",Record.Tag, Translated_String,Location,Test_String,Description);
					}
				}
				Hits++;Record.Start++;Total_Hits++;
			}

		}

	}
}
//}-----------------------------  Verify Deep  -------------------------------------------------

//{-----------------------------  Process pairend  -------------------------------------------------
void Verify_Pairend()
{
	unsigned i, Bitsread;
	unsigned Start,End;
	unsigned Location;
	int Desc_End;
	unsigned Previous_Tag;
	unsigned Hits=0;
	char letter;
	unsigned pos;
	int Conversion_Factor,StringLength;
	int HEAD_LENGTH,TAIL_LENGTH;
	char Layout[2];//head/tail, orientation...
	int Genome_Length=revfmi->textLength;	
	char Pass,PM;
	char* Next_String;
	char* String;

	printf ("Verifying using %s\n", BINFILE);
	Binary_File=fopen(BINFILE,"rb");
	fread(&HEAD_LENGTH,sizeof(int),1,Data_File);
	fread(&TAIL_LENGTH,sizeof(int),1,Data_File);
	for(;;)
	{
		i=0;
		fread(&Tag_Type,1,1,Data_File);
		if('&' == Tag_Type) break;
		if('@' == Tag_Type)//start of new tag
		{
			if(PRINT_DESC) 
			{
				fgets(Description,1000,Data_File);
				for(Desc_End=0;Description[Desc_End]!='\n' && Description[Desc_End]!='\r' && Description[Desc_End]!=0;Desc_End++);
				Description[Desc_End]=0;
			};
			//Hits=0;
			Total_Tags++;
			
			fgets(Translated_String_Original,MAXSTRINGLENGTH,Data_File);
			for(Next_String=Translated_String_Original;Next_String[0]!='\n' && Next_String[0]!='\r' && Next_String[0]!=0;Next_String++) Next_String[0]=tolower(Next_String[0]);
			Pass=1;PM='+';
			continue;
		}		
		else
		{
			memcpy(Translated_String,Translated_String_Original,MAXSTRINGLENGTH);
			fread(&Layout,2,1,Data_File);
			if(Layout[0]=='H')
			{
				String=Translated_String;STRINGLENGTH=HEAD_LENGTH;
			} 
			else
			{
				String=Translated_String+TAIL_LENGTH+1;STRINGLENGTH=TAIL_LENGTH;

				if (Pass==1) 
				{
					Hits=0;Pass=0;
				}
			}
			if (Layout[1] == '-')
			{
				for (unsigned i=0;i<=STRINGLENGTH-1;i++)
				{
					RevString[STRINGLENGTH-1-i]=Code_To_Char[Char_To_CodeC[String[i]]];
				}
				String=RevString;
				if (PM == '+')
				{
					Hits=0;PM='-';
				}
			}
			else
			{
				if (PM == '-')
				{
					Hits=0;PM='+';
				}
			}


			Conversion_Factor=Genome_Length-STRINGLENGTH;
			StringLength=STRINGLENGTH;
			fread(&Mismatches,sizeof(Mismatches_Record),1,Data_File);
			fread(&Record,sizeof(Output_Record),1,Data_File);
			Mismatch_Count = Record.Mismatches;

			if (Mismatch_Count)//mismatch positions..
			{
				for( int i=0;i<Mismatch_Count;i++)
				{
					String[63 & (Mismatches.Mismatch_Pos>>(6*i))]=Code_To_Char[Mismatches.Mismatch_Char>>(2*i) & 3];
				}
			}

			if((63 & (Mismatches.Mismatch_Pos>>6))==63)//Indel...
			{
				pos=63 & (Mismatches.Mismatch_Pos>>(6*2));
				if (pos==63)//insertion
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*3));
					Conversion_Factor--;StringLength++;
					for(int i=STRINGLENGTH+1;i>pos;i--) 
					{
						String[i]=String[i-1];
					}
					String[pos]=Code_To_Char[Mismatches.Mismatch_Char >> 2*2];
				}
				else//deletion
				{
					Conversion_Factor++;StringLength--;
					for(int i=pos;i<STRINGLENGTH;i++) 
					{
						String[i]=String[i+1];
					}
				}
			}		

			for (int j=0;j<=Mismatches.Gap && Hits< MAXHITS;j++)
			{
				if( Record.Index)//print record...
				{
					Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
				}
				else
				{
					Location=BWTSaValue(fmi,Record.Start)-Offset;
				}
				fseeko(Binary_File,Location,SEEK_SET);
				fread(&Test_String,StringLength,1,Binary_File);
				if (strncmp(Test_String,String,StringLength))
				{
					for (unsigned i=0;i<=StringLength-1;i++)
					{
						String[i]=Code_To_Char[Char_To_CodeC[String[i]]];
					}

					if (strncmp(Test_String,RevString,StringLength))
					{
						printf("Error in Tag %d\t%s \n: Location %u is\t%s\t%s\n",Record.Tag, String,Location,Test_String,Description);
					}
				}
				Hits++;Record.Start++;Total_Hits++;
			}

		}
	}
}
//}-----------------------------  Process pairend  -------------------------------------------------

//{-----------------------------  Process Deep  -------------------------------------------------
void Process_Deep()
{
	unsigned i, Bitsread;
	unsigned Start,End;
	unsigned Location;
	int Desc_End;
	unsigned Previous_Tag,Hits;
	char letter;
	unsigned pos;
	int Conversion_Factor,StringLength;

	for(;;)
	{
		i=0;
		fread(&Tag_Type,1,1,Data_File);
		if('&' == Tag_Type) break;
		if('@' == Tag_Type)//start of new tag
		{
			fprintf(Output_File,"@\n");
			if(PRINT_DESC) 
			{
				fgets(Description,1000,Data_File);
				for(Desc_End=0;Description[Desc_End]!='\n' && Description[Desc_End]!='\r' && Description[Desc_End]!=0;Desc_End++);
				Description[Desc_End]=0;
				fprintf(Output_File,"%s ",Description);
			};
			fread(Translated_String,STRINGLENGTH+1,1,Data_File);
			fprintf(Output_File,"%s",Translated_String);Hits=0;Total_Tags++;
			continue;
		}		
		else
		{
			//modify out...	
			Conversion_Factor=CONVERSION_FACTOR;
			StringLength=STRINGLENGTH;
			//Offset=0;
			fread(&Mismatches,sizeof(Mismatches_Record),1,Data_File);
			fread(&Record,sizeof(Output_Record),1,Data_File);
			Mismatch_Count = Record.Mismatches;

			fprintf(Output_File,"\t%u %d ",Record.Tag,Mismatch_Count);

			if((63 & (Mismatches.Mismatch_Pos>>6))==63)//Indel...
			{
				pos=63 & (Mismatches.Mismatch_Pos>>(6*2));
				if (pos==63)//insertion
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*3));
					Conversion_Factor--;StringLength++;
					fprintf(Output_File," %d<%c ",pos,Code_To_Char[Mismatches.Mismatch_Char >> 2*2]-('a'-'A'));
				}
				else
				{
					Conversion_Factor++;StringLength--;
					fprintf(Output_File," %d>D ",pos);
				}

			}

			if (Mismatch_Count)//mismatch positions..
			{
				for( int i=0;i<Mismatch_Count;i++)
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*i));
					letter=Mismatches.Mismatch_Char>>(2*i) & 3;
					//Translated_String[pos]=Code_To_Char[letter];
					fprintf(Output_File," %d>%c ",pos,Code_To_Char[letter]);
				}
			}

			fprintf(Output_File," "); //seperator...
			for (int j=0;j<=Mismatches.Gap && Hits< MAXHITS;j++)
			{
				if(USELOCATION)
				{
					if( Record.Index)//print record...
					{
						Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
						Location_To_Genome(Location);
					}
					else
					{
						Location=BWTSaValue(fmi,Record.Start)-Offset;
						Location_To_Genome(Location);
					}
					if (!PLUSSTRAND && Translated_String[STRINGLENGTH]=='-') Location=Location+STRINGLENGTH;
					fprintf(Output_File,"%u;[%s]",Location,Genome_Offsets[Genome_Position].Genome);
				}
				else
				{
					if( Record.Index)//print record...
					{
						Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
					}
					else
					{
						Location=BWTSaValue(fmi,Record.Start)-Offset;
					}
					if (!PLUSSTRAND && Translated_String[STRINGLENGTH]=='-') Location=Location+STRINGLENGTH;
					fprintf(Output_File,"%u;",Location);
				}
				Hits++;Record.Start++;Total_Hits++;
			}
			fprintf(Output_File,"\n");

		}

	}
}
//}-----------------------------  Process Deep  -------------------------------------------------

//{-----------------------------  Process Deep Formatted  -------------------------------------------------
void Process_Deep_Formatted()
{
	unsigned i, Bitsread;
	unsigned Start,End;
	unsigned Location;
	int Desc_End;
	unsigned Previous_Tag,Hits;
	char letter;
	unsigned pos;
	char Init=TRUE;
	int Conversion_Factor,StringLength;

	Total_Hits=0;
	for(;;)
	{
		i=0;
		fread(&Tag_Type,1,1,Data_File);
		if('&' == Tag_Type) break;
		if('@' == Tag_Type)//start of new tag
		{
			fprintf(Output_File,"@\n");
			if(!Init){fprintf(Output_File,"@\n");} else {fprintf(Output_File,"@\t%d\n",STRINGLENGTH);Init=FALSE;}
			if(PRINT_DESC) 
			{
				fgets(Description,1000,Data_File);
				for(Desc_End=0;Description[Desc_End]!='\n' && Description[Desc_End]!='\r' && Description[Desc_End]!=0;Desc_End++);
				Description[Desc_End]=0;
				fprintf(Output_File,"%s",Description);
			};
			fread(Translated_String,STRINGLENGTH+1,1,Data_File);
			fprintf(Output_File,"\t%s\n",Translated_String);Hits=0;Total_Tags++;
			continue;
		}		
		else
		{
		//modify out...	
			Conversion_Factor=CONVERSION_FACTOR;
			StringLength=STRINGLENGTH;
			//Offset=0;
			fread(&Mismatches,sizeof(Mismatches_Record),1,Data_File);
			fread(&Record,sizeof(Output_Record),1,Data_File);
			Mismatch_Count = Record.Mismatches;
			
				fprintf(Output_File,"%u\t%d\t",Record.Tag,Mismatch_Count);

				if((63 & (Mismatches.Mismatch_Pos>>6))==63)//Indel...
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*2));
					if (pos==63)//insertion
					{
						pos=63 & (Mismatches.Mismatch_Pos>>(6*3));
						Conversion_Factor--;StringLength++;
						fprintf(Output_File,"%d<%c\t",pos,Code_To_Char[Mismatches.Mismatch_Char >> 2*2]-('a'-'A'));
					}
					else
					{
						Conversion_Factor++;StringLength--;
						fprintf(Output_File,"%d>D\t",pos);
					}

				}

				if (Mismatch_Count)//mismatch positions..
				{
					for( int i=0;i<Mismatch_Count;i++)
					{
						pos=63 & (Mismatches.Mismatch_Pos>>(6*i));
						letter=Mismatches.Mismatch_Char>>(2*i) & 3;
						//Translated_String[pos]=Code_To_Char[letter];
						fprintf(Output_File,"%d>%c\t",pos,Code_To_Char[letter]);
					}
				}
				
				fprintf(Output_File,"|%d\t",Mismatches.Gap+1); //seperator...
				for (int j=0;j<=Mismatches.Gap && Hits< MAXHITS;j++)
				{
					if(USELOCATION)
					{
						if( Record.Index)//print record...
						{
							Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
							Location_To_Genome(Location);
						}
						else
						{
							Location=BWTSaValue(fmi,Record.Start)-Offset;
							Location_To_Genome(Location);
						}
						if (!PLUSSTRAND && Translated_String[STRINGLENGTH]=='-') Location=Location+STRINGLENGTH;
						fprintf(Output_File,"%s:%u;",Genome_Offsets[Genome_Position].Genome,Location);
					}
					else
					{
						if( Record.Index)//print record...
						{
							Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
						}
						else
						{
							Location=BWTSaValue(fmi,Record.Start)-Offset;
						}
						if (!PLUSSTRAND && Translated_String[STRINGLENGTH]=='-') Location=Location+STRINGLENGTH;
						fprintf(Output_File,"%u;",Location);
					}
					Hits++;Record.Start++;Total_Hits++;
				}
				fprintf(Output_File,"\n");

		}

	}
}
//}-----------------------------  Process Deep  -------------------------------------------------

//{-----------------------------  Process pairend  -------------------------------------------------
void Process_Pairend()
{
	unsigned i, Bitsread;
	unsigned Start,End;
	unsigned Location;
	int Desc_End;
	unsigned Previous_Tag,Hits;
	char letter;
	unsigned pos;
	int Conversion_Factor,StringLength;
	int HEAD_LENGTH,TAIL_LENGTH;
	char Layout[2];//head/tail, orientation...
	int Genome_Length=revfmi->textLength;	
	char Pass;
	char PM;

	//if (MAXHITS==1) MAXHITS=2;
	//MAXHITS=2*MAXHITS;
	fread(&HEAD_LENGTH,sizeof(int),1,Data_File);
	fread(&TAIL_LENGTH,sizeof(int),1,Data_File);
	for(;;)
	{
		i=0;
		fread(&Tag_Type,1,1,Data_File);
		if('&' == Tag_Type) break;
		if('@' == Tag_Type)//start of new tag
		{
			fprintf(Output_File,"@\n");
			if(PRINT_DESC) 
			{
				fgets(Description,1000,Data_File);
				for(Desc_End=0;Description[Desc_End]!='\n' && Description[Desc_End]!='\r' && Description[Desc_End]!=0;Desc_End++);
				Description[Desc_End]=0;
				fprintf(Output_File,"%s\t",Description);
			};
			fgets(Translated_String,MAXSTRINGLENGTH,Data_File);
			fprintf(Output_File,"%s",Translated_String);Hits=0;Total_Tags++;
			Pass=1;PM='+';
			continue;
		}		
		else
		{
		//modify out...	
			fread(&Layout,2,1,Data_File);

			if(Layout[0]=='H') 
			{
				STRINGLENGTH=HEAD_LENGTH;
			}
			else 
			{
				STRINGLENGTH=TAIL_LENGTH;
				if (Pass==1) 
				{
					Hits=0;
					Pass=0;
				}
			}

			if(Layout[1]=='-')
			{
				if (PM == '+')
				{
					Hits=0;PM='-';
				}
			}
			else
			{
				if (PM == '-')
				{
					Hits=0;PM='+';
				}
			}

			Conversion_Factor=Genome_Length-STRINGLENGTH;
			//printf("H %d ", STRINGLENGTH);
			StringLength=STRINGLENGTH;
			//Offset=0;
			fread(&Mismatches,sizeof(Mismatches_Record),1,Data_File);
			fread(&Record,sizeof(Output_Record),1,Data_File);
			Mismatch_Count = Record.Mismatches;
			
				//fprintf(Output_File,"\t%u %d ",Record.Tag,Mismatch_Count);
				fprintf(Output_File,"%c\t%c\t",Layout[0],Layout[1]);//,Record.Tag,Mismatch_Count);
/*
				if((63 & (Mismatches.Mismatch_Pos>>6))==63)//Indel...
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*2));
					if (pos==63)//insertion
					{
						pos=63 & (Mismatches.Mismatch_Pos>>(6*3));
						Conversion_Factor--;StringLength++;
						fprintf(Output_File," %d<%c ",pos,Code_To_Char[Mismatches.Mismatch_Char >> 2*2]-('a'-'A'));
					}
					else
					{
						Conversion_Factor++;StringLength--;
						fprintf(Output_File," %d>D ",pos);
					}

				}

				if (Mismatch_Count)//mismatch positions..
				{
					for( int i=0;i<Mismatch_Count;i++)
					{
						pos=63 & (Mismatches.Mismatch_Pos>>(6*i));
						letter=Mismatches.Mismatch_Char>>(2*i) & 3;
						//Translated_String[pos]=Code_To_Char[letter];
						fprintf(Output_File," %d>%c ",pos,Code_To_Char[letter]);
					}
				}*/
				
				fprintf(Output_File," "); //seperator...
				for (int j=0;j<=Mismatches.Gap && Hits< MAXHITS;j++)
				{
					if(USELOCATION)
					{
						if( Record.Index)//print record...
						{
							Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
							Location_To_Genome(Location);
						}
						else
						{
							Location=BWTSaValue(fmi,Record.Start)-Offset;
							Location_To_Genome(Location);
						}
						if (!PLUSSTRAND && Layout[1]=='-') Location=Location+STRINGLENGTH;
						fprintf(Output_File,"%s:%u;",Genome_Offsets[Genome_Position].Genome,Location);
					}
					else
					{
						if( Record.Index)//print record...
						{
							Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
						}
						else
						{
							Location=BWTSaValue(fmi,Record.Start)-Offset;
						}
						if (!PLUSSTRAND && Layout[1]=='-') Location=Location+STRINGLENGTH;
						fprintf(Output_File,"%u;",Location);
					}
					Hits++;Record.Start++;Total_Hits++;
				}
				fprintf(Output_File,"\t%d\t%d\n",Mismatch_Count,STRINGLENGTH);

		}

	}
}
//}-----------------------------  Process pairend  -------------------------------------------------
//formatted
//{-----------------------------  Process pairend Formatted  -------------------------------------------------
void Process_Pairend_Formatted()
{
	unsigned i, Bitsread;
	unsigned Start,End;
	unsigned Location;
	int Desc_End;
	unsigned Previous_Tag,Hits;
	char letter;
	unsigned pos;
	int Conversion_Factor,StringLength;
	int HEAD_LENGTH,TAIL_LENGTH;
	char Layout[2];//head/tail, orientation...
	char Pass,PM;
	char Init=TRUE;
	int Genome_Length=revfmi->textLength;	

	//if (MAXHITS==1) MAXHITS=2;
	//MAXHITS=2*MAXHITS;
	fread(&HEAD_LENGTH,sizeof(int),1,Data_File);
	fread(&TAIL_LENGTH,sizeof(int),1,Data_File);
	for(;;)
	{
		i=0;
		fread(&Tag_Type,1,1,Data_File);
		if('&' == Tag_Type) break;
		if('@' == Tag_Type)//start of new tag
		{
			if(!Init){fprintf(Output_File,"@\n");} else {fprintf(Output_File,"@\t%d\t%d\n",HEAD_LENGTH,TAIL_LENGTH);Init=FALSE;}
			if(PRINT_DESC) 
			{
				fgets(Description,1000,Data_File);
				for(Desc_End=0;Description[Desc_End]!='\n' && Description[Desc_End]!='\r' && Description[Desc_End]!=0;Desc_End++);
				Description[Desc_End]=0;
				fprintf(Output_File,"%s",Description);
			};
			fgets(Translated_String,MAXSTRINGLENGTH,Data_File);
			fprintf(Output_File,"\t%s\n",Translated_String);Hits=0;Total_Tags++;
			Pass=1;PM='+';
			continue;
		}		
		else
		{
		//modify out...	
			fread(&Layout,2,1,Data_File);
			if(Layout[0]=='H') 
			{
				STRINGLENGTH=HEAD_LENGTH;
			}
			else 
			{
				STRINGLENGTH=TAIL_LENGTH;
				if (Pass==1) 
				{
					Hits=0;
					Pass=0;
				}
			}

			if(Layout[1]=='-')
			{
				if (PM == '+')
				{
					Hits=0;PM='-';
				}
			}
			else
			{
				if (PM == '-')
				{
					Hits=0;PM='+';
				}
			}
			//if(Layout[0]=='H') STRINGLENGTH=HEAD_LENGTH; else {STRINGLENGTH=TAIL_LENGTH;if (Pass==1) {Hits=0;Pass=0;}}
			Conversion_Factor=Genome_Length-STRINGLENGTH;
			//printf("H %d ", STRINGLENGTH);
			StringLength=STRINGLENGTH;
			//Offset=0;
			fread(&Mismatches,sizeof(Mismatches_Record),1,Data_File);
			fread(&Record,sizeof(Output_Record),1,Data_File);
			Mismatch_Count = Record.Mismatches;
			
				fprintf(Output_File,"%u\t%d\t",Record.Tag,Mismatch_Count);
				fprintf(Output_File,"%c\t%c\t",Layout[0],Layout[1]);//,Record.Tag,Mismatch_Count);

				if((63 & (Mismatches.Mismatch_Pos>>6))==63)//Indel...
				{
					pos=63 & (Mismatches.Mismatch_Pos>>(6*2));
					if (pos==63)//insertion
					{
						pos=63 & (Mismatches.Mismatch_Pos>>(6*3));
						Conversion_Factor--;StringLength++;
						fprintf(Output_File,"%d<%c\t",pos,Code_To_Char[Mismatches.Mismatch_Char >> 2*2]-('a'-'A'));
					}
					else
					{
						Conversion_Factor++;StringLength--;
						fprintf(Output_File,"%d>D\t",pos);
					}

				}

				if (Mismatch_Count)//mismatch positions..
				{
					for( int i=0;i<Mismatch_Count;i++)
					{
						pos=63 & (Mismatches.Mismatch_Pos>>(6*i));
						letter=Mismatches.Mismatch_Char>>(2*i) & 3;
						//Translated_String[pos]=Code_To_Char[letter];
						fprintf(Output_File,"%d>%c\t",pos,Code_To_Char[letter]);
					}
				}
				
				//fprintf(Output_File," "); //seperator...
				fprintf(Output_File,"|%d\t",Mismatches.Gap+1); //seperator...
				for (int j=0;j<=Mismatches.Gap && Hits< MAXHITS;j++)
				{
					if(USELOCATION)
					{
						if( Record.Index)//print record...
						{
							Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
							Location_To_Genome(Location);
						}
						else
						{
							Location=BWTSaValue(fmi,Record.Start)-Offset;
							Location_To_Genome(Location);
						}
						if (!PLUSSTRAND && Layout[1]=='-') Location=Location+STRINGLENGTH;
						fprintf(Output_File,"%s:%u;",Genome_Offsets[Genome_Position].Genome,Location);
					}
					else
					{
						if( Record.Index)//print record...
						{
							Location=Conversion_Factor-BWTSaValue(revfmi,Record.Start)-Offset;
						}
						else
						{
							Location=BWTSaValue(fmi,Record.Start)-Offset;
						}
						if (!PLUSSTRAND && Layout[1]=='-') Location=Location+STRINGLENGTH;
						fprintf(Output_File,"%u;",Location);
					}
					Hits++;Record.Start++;Total_Hits++;
				}
				//fprintf(Output_File,"\t%d\t%d\n",Mismatch_Count,STRINGLENGTH);
				fprintf(Output_File,"\n");

		}

	}
}
//}-----------------------------  Process pairend  -------------------------------------------------


//{-----------------------------  Process Default  -------------------------------------------------
void Process_Default()
{
	unsigned i, Bitsread;
	unsigned Start,End,Location;
	unsigned Hitcount,Last_Tag;
	char PlusMinus;
	char Indel=0;
	int Conversion_Factor;

	Hits=(Output_Record*)malloc(sizeof(Output_Record)*BUFFERSIZE+1);
	Hits[BUFFERSIZE].Start=0;
	Hitcount=0;
	for(;;)
	{
		i=0;
		Bitsread=fread(Hits,sizeof(Output_Record),BUFFERSIZE,Data_File);
		while(Hits[i].Start)//while not sentinel...
		{
			if (Last_Tag != Hits[i].Tag) Hitcount=0;//new tag, new count...
			Last_Tag=Hits[i].Tag;
			for (int j=0;j<=Hits[i].Gap && Hitcount< MAXHITS;j++)
			{
				Indel='M';
				Conversion_Factor=CONVERSION_FACTOR;
				if (Hits[i].Mismatches>=100) {Location=Location+STRINGLENGTH;Hits[i].Mismatches=Hits[i].Mismatches-100;PlusMinus='-';} else PlusMinus='+';
				if (Hits[i].Mismatches>=75) {Conversion_Factor--;Hits[i].Mismatches=Hits[i].Mismatches-75;Indel='I';} 
				else if (Hits[i].Mismatches>=50) {Conversion_Factor++;Hits[i].Mismatches=Hits[i].Mismatches-50;Indel='D';}

				if( Hits[i].Index)//print record...
				{
					Location=Conversion_Factor-BWTSaValue(revfmi,Hits[i].Start)-Offset;
				}
				else
				{
					Location=BWTSaValue(fmi,Hits[i].Start)-Offset;
				}
				if (USELOCATION) Location_To_Genome(Location);

				fprintf(Output_File,"%u \t %u \t %c%d \t %c[%s] \n ",Hits[i].Tag,Location,Indel,Hits[i].Mismatches,PlusMinus,Genome_Offsets[Genome_Position].Genome);
				Total_Hits++;

				Hitcount++;Hits[i].Start++;
			}
			i++;
		}
		if(Bitsread<BUFFERSIZE) break;
	}
}
//}-----------------------------  Process Default  -------------------------------------------------

void Location_To_Genome(unsigned & Location)
{
	Genome_Position=0;
	while ( Genome_Position< Genome_Count )
	{
		if (Location < Offsets[Genome_Position]) break;
		Genome_Position++;
	}
	Genome_Position--;
	Location=Location-Offsets[Genome_Position];
	
}

//{-----------------------------  Parse Command Line  -------------------------------------------------
void Parse_Command_line(int argc, char* argv[])
{
	int Current_Option=0;
	const char* Short_Options ="rhvi:b:l::o:g:O:pf";//allowed options....
	char* This_Program = argv[0];//Current program name....
	const char* Help_String=
"Parameters:\n"
" --help | -h\t\t\t\t Print help\n"
" --inputfile | -i <filename>\t\t Name of input file\n"
" --genome | -g <filename>\t\t Name of the genome mapped against\n"
" --outputfile | -o <filename>\t\t Name of output file\n"
" --buffersize | -b <integer> \t\t Size of disk buffers\n"
" --verify | -v \t\t Verify hits\n"
" --location | -l <filename> \t\t use this file to filter locations by region ...\n"
" --offset | -O <integer> \t\t subtract <integer> from hit location...\n"
" --formatted | f \t\t convert output to be used by fmt..\n"
" --plusstrand | -p \t\t always output + strand coordinate....\n"
;
	char* Name;int Last_Dash;char* Genome_Name;

	for(;;)	
	{
		Current_Option=getopt_long(argc, argv, Short_Options, Long_Options, NULL);
		if (Current_Option == -1 ) break;
		switch(Current_Option)
		{
			case 'h':
				printf("%s \n",Help_String);exit(0);
			case 'i':
				INPUTFILE=optarg;
				break;
			case 'f':
				FORMAT=TRUE;
				break;
			case 'p':
				PLUSSTRAND=TRUE;
				break;
			case 'o':
				OUTPUTFILE=optarg;
				break;
			case 'O':
				Offset=atol(optarg);
				break;
			case 'v':
				VERIFY=TRUE;
				break;

			case 'r':
				LOADREVERSEONLY = TRUE;
				break;
			case 'b':
				DISKBUFFERSIZE=atol(optarg);
				break;
			case 'l':
				if (optarg) LOCATIONFILE=optarg;	
				USELOCATION=TRUE;
				break;
			case 'g':
				Name=optarg;Last_Dash=0;Genome_Name=optarg;
				for(;Name[0]!=0;Name++)
				{
					if (Name[0]=='/') 
					{
						Last_Dash++;Genome_Name=Name;
					}
				}

				REVBWTINDEX = (char*)Command_Line_Buffer;
				if(Last_Dash) Last_Dash=Genome_Name-optarg+1; else Genome_Name--;
				strncpy(REVBWTINDEX,optarg,Last_Dash);
				REVBWTINDEX[Last_Dash+0]='r';REVBWTINDEX[Last_Dash+1]='e';REVBWTINDEX[Last_Dash+2]='v';
				strcpy(REVBWTINDEX+Last_Dash+3,Genome_Name+1);
				strcat(REVBWTINDEX+Last_Dash+3,".bwt"); 

				BWTFILE=REVBWTINDEX+500;
				strncpy(BWTFILE,optarg,Last_Dash);
				strcpy(BWTFILE+Last_Dash,Genome_Name+1);
				strcat(BWTFILE+Last_Dash,".bwt"); 


				REVOCCFILE = BWTFILE+500;
				strncpy(REVOCCFILE,optarg,Last_Dash);
				REVOCCFILE[Last_Dash+0]='r';REVOCCFILE[Last_Dash+1]='e';REVOCCFILE[Last_Dash+2]='v';
				strcpy(REVOCCFILE+Last_Dash+3,Genome_Name+1);
				strcat(REVOCCFILE+Last_Dash+3,".fmv"); 


				OCCFILE=REVOCCFILE+500;			
				strncpy(OCCFILE,optarg,Last_Dash);
				strcpy(OCCFILE+Last_Dash,Genome_Name+1);
				strcat(OCCFILE+Last_Dash,".fmv"); 

				SAFILE=OCCFILE+500;			
				strncpy(SAFILE,optarg,Last_Dash);
				strcpy(SAFILE+Last_Dash,Genome_Name+1);
				strcat(SAFILE+Last_Dash,".sa");

				REVSAFILE = SAFILE+500;
				strncpy(REVSAFILE,optarg,Last_Dash);
				REVSAFILE[Last_Dash+0]='r';REVSAFILE[Last_Dash+1]='e';REVSAFILE[Last_Dash+2]='v';
				strcpy(REVSAFILE+Last_Dash+3,Genome_Name+1);
				strcat(REVSAFILE+Last_Dash+3,".sa"); 

				BINFILE=REVSAFILE+500;			
				strncpy(BINFILE,optarg,Last_Dash);
				strcpy(BINFILE+Last_Dash,Genome_Name+1);
				strcat(BINFILE+Last_Dash,".bin");





/*
				REVBWTINDEX = Command_Line_Buffer;
				REVBWTINDEX[0]='r';REVBWTINDEX[1]='e';REVBWTINDEX[2]='v';
				strcpy(REVBWTINDEX+3,optarg);
				strcat(REVBWTINDEX+3,".bwt"); 
				BWTFILE=REVBWTINDEX+3;
				REVOCCFILE = BWTFILE+500;
				
				REVOCCFILE[0]='r';REVOCCFILE[1]='e';REVOCCFILE[2]='v';
				strcpy(REVOCCFILE+3,optarg);	
				strcpy(REVOCCFILE+500,REVOCCFILE);
				REVSAFILE=REVOCCFILE+500;
				strcat(REVSAFILE,".sa");
				SAFILE=REVSAFILE+3;
				strcat(REVOCCFILE+3,".fmv"); 
				OCCFILE=REVOCCFILE+3;			

				BINFILE=REVSAFILE +500;
				strcpy(BINFILE,optarg);
				strcat(BINFILE,".bin");*/
				break;
			default:
				printf("%s \n",Help_String);
				exit(0);
		}
	}	
}

//}-----------------------------  Parse Command Line  -------------------------------------------------


//{----------------------------------- FILE HANDLING ---------------------------------------------------------

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  File_Open
 *  Description:  Open a file:
 *  Mode - "w" create/Write text from top    - "wb" Write/Binary  -"w+" open text read/write            -"w+b" same as prev/Binary
 *         "r" Read text from top            - "rb" Read/Binary   -"r+" open text read/write/nocreate   -"r+b" same as prev/binary
 *       - "a" text append/write                                  -"a+" text open file read/append      -"a+b" open binary read/append
 *
 * =====================================================================================
 */
FILE* File_Open(const char* File_Name,const char* Mode)
{
	FILE* Handle;
	Handle=fopen(File_Name,Mode);
	if (Handle==NULL)
	{
		printf("File %s Cannot be opened ....",File_Name);
		exit(1);
	}
	else return Handle;
}

//}----------------------------------- FILE HANDLING ---------------------------------------------------------

//{----------------------------------- FM INDEX ROUTINES ---------------------------------------------------------
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  initFMI
 *  Description:  Opens FM index fmiFile
 * =====================================================================================
 */
BWT* initFMI(const char* BWTCodeFileName,const char* BWTOccValueFileName, const char* SAFile) 

{
	BWT *fmi;
        int PoolSize = 524288;
	MMMasterInitialize(3, 0, FALSE, NULL);
	mmPool = MMPoolCreate(PoolSize);

	fmi = BWTLoad(mmPool, BWTCodeFileName, BWTOccValueFileName, SAFile, NULL, NULL, NULL);//Load FM index

	return fmi;
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Get_SARange
 *  Description:  gets the SA range of strings having prefix [New_Char][Range]
 * =====================================================================================
 */

//}----------------------------------- FM INDEX ROUTINES ---------------------------------------------------------

//{-----------------------------------DEBUG ROUTINE---------------------------------------------------------

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Print_Location
 *  Description:  Prints the hex location of start and end of Range
 * =====================================================================================

void Print_Location (unsigned Range,BWT *fmi)
{
		printf("%x :",BWTSaValue(fmi,Range));//<<":";//%d;%d>",coordinate,textPosition[Pattern_Number]);
		//printf("%x :",BWTSaValue(fmi,Range.End));//<<":";//%d;%d>",coordinate,textPosition[Pattern_Number]);
}*/
//}-----------------------------------DEBUG ROUTINE---------------------------------------------------------

void Read_INI()
{

	dictionary* Dictionary;
	Dictionary=iniparser_load("batman.ini",FALSE);
	if (Dictionary)
	{
		PLUSSTRAND = iniparser_getint(Dictionary,"decode:plusstrand",0);
		FORMAT = iniparser_getint(Dictionary,"decode:formatted",1);
		
	}
	iniparser_freedict(Dictionary);
}


