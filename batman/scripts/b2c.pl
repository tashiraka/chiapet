#!/usr/bin/perl 
#sieves tags into three files
# .des for description, .tag for tags, .phred for score...
# tr -d '\r' <rikky.fq.flat to remove lf..
print "Base space 2 Color space...\n";
unless($ARGV[0])
{
	print "tagformat file count.. \n";
	exit;
}

$FileName=$ARGV[0];
unless(open(FILE,$FileName))
{
	print "File does not Exist \n";
	exit;
}

%Conversion_Table=();
$Conversion_Table{'AA'}='A';
$Conversion_Table{'CC'}='A';
$Conversion_Table{'GG'}='A';
$Conversion_Table{'TT'}='A';

$Conversion_Table{'AC'}='C';
$Conversion_Table{'CA'}='C';
$Conversion_Table{'GT'}='C';
$Conversion_Table{'TG'}='C';

$Conversion_Table{'AG'}='G';
$Conversion_Table{'GA'}='G';
$Conversion_Table{'CT'}='G';
$Conversion_Table{'TC'}='G';

$Conversion_Table{'AT'}='T';
$Conversion_Table{'TA'}='T';
$Conversion_Table{'GC'}='T';
$Conversion_Table{'CG'}='T';

open (TAGFILE,'>'.$FileName.".cfq") or die;
$Line_Count = 0;
$count = 0;
$Last='';
while ($TagLine = <FILE> )
{
	if (substr($TagLine,0,1) eq '>')
	{
		print TAGFILE $TagLine;
	}
	else
	{
		$TagLine=$Last.$TagLine;
		$TagLine=~ tr/nNacgt/AAACGT/;
		$count=$count+1;
		$New_Tag='';
		$Len= (length $TagLine);
		for ($i=0;$i<= $Len ; $i++)
		{
			$New_Tag=$New_Tag.$Conversion_Table{substr($TagLine,$i,2)};
		}
		#$New_Tag=$New_Tag.$Conversion_Table{substr($TagLine,$_,2)} for 0 .. length $TagLine;
		$Last=substr($TagLine,(length $TagLine)-2,1);
		print TAGFILE $New_Tag."\n";
	}
}
print $count." lines Coded... \n";
close FILE;
